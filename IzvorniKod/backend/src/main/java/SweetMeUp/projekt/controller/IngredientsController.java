package SweetMeUp.projekt.controller;

import SweetMeUp.projekt.DTO.CakeDTO;
import SweetMeUp.projekt.DTO.IngredientDTO;
import SweetMeUp.projekt.entity.Ingredient;
import SweetMeUp.projekt.entity.Metric;
import SweetMeUp.projekt.requests.InsertBodyIngredient;
import SweetMeUp.projekt.requests.InsertIngredient;
import SweetMeUp.projekt.security.CurrentUser;
import SweetMeUp.projekt.security.UserPrincipal;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("ingredients")
public class IngredientsController {

    private IngredientDTO ingredientDAO;
    private CakeDTO cakeDao;

    public IngredientsController(IngredientDTO ingredientDAO, CakeDTO cakeDao) {
        this.ingredientDAO = ingredientDAO;
        this.cakeDao = cakeDao;
    }

    @GetMapping
    List<Ingredient> allIng() {
        return ingredientDAO.getIngredients();
    }
    
    @GetMapping("metrics")
    List<Metric> allMetrics(){
    	return ingredientDAO.getMetrics();
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("addNewIngredient")
    public ResponseEntity<String> insertIngdient(@RequestBody InsertBodyIngredient insertBodyIngredient) {
        if (ingredientDAO.insertIngredients(insertBodyIngredient.getName(), insertBodyIngredient.getAllergen())) {
            return new ResponseEntity<String>("Ingredient successfully added", HttpStatus.OK);
        }
        return new ResponseEntity<String>("Ingredient addition was unsuccessful", HttpStatus.BAD_REQUEST);
    }
    
    @PreAuthorize("hasRole('SELLER')")
    @PostMapping("addIngredientToCake")
    public ResponseEntity<String> insertIngdientToCake(@CurrentUser UserPrincipal user, @RequestBody InsertIngredient insertIngredient) {
    	
    	if(!cakeDao.checkUserCake(user.getId(),insertIngredient.getCakeId()))
    		return new ResponseEntity<String>("That is not your cake", HttpStatus.UNAUTHORIZED);
    	    	
        if (ingredientDAO.insertIngredientToCake(insertIngredient.getIngredientId(), insertIngredient.getCakeId(), insertIngredient.getAmount(), insertIngredient.getMetric())) {
            return new ResponseEntity<String>("Ingredient successfully added to Cake", HttpStatus.OK);
        }
        return new ResponseEntity<String>("Ingredient addition was unsuccessful", HttpStatus.BAD_REQUEST);
    }
    
    @PreAuthorize("hasRole('SELLER')")
    @PutMapping("editIngredientOnCake")
    public ResponseEntity<String> editIngdientOnCake(@CurrentUser UserPrincipal user, @RequestBody InsertIngredient insertIngredient) {
    	
    	if(!cakeDao.checkUserCake(user.getId(),insertIngredient.getCakeId()))
    		return new ResponseEntity<String>("That is not your cake", HttpStatus.UNAUTHORIZED);
    	    	
        if (ingredientDAO.updateIngredientToCake(insertIngredient.getIngredientId(), insertIngredient.getCakeId(), insertIngredient.getAmount(), insertIngredient.getMetric())) {
            return new ResponseEntity<String>("Ingredient successfully updated on Cake", HttpStatus.OK);
        }
        return new ResponseEntity<String>("Ingredient update was unsuccessful", HttpStatus.BAD_REQUEST);
    }

    @PreAuthorize("hasRole('SELLER')")
    @DeleteMapping("deleteFromCake")
    public ResponseEntity<String> deleteIngdient(@CurrentUser UserPrincipal user, @RequestParam(value = "ingredientId") Long ingredientId, @RequestParam(value = "cakeId") Long cakeId) {
    	
    	if(!cakeDao.checkUserCake(user.getId(),cakeId))
    		return new ResponseEntity<String>("That is not your cake", HttpStatus.UNAUTHORIZED);
    	
        if (ingredientDAO.deleteIngredientFromCake(ingredientId, cakeId)) {
            return new ResponseEntity<>("Ingredient successfully deleted", HttpStatus.OK);
        }
        return new ResponseEntity<String>("Ingredient deletion was unsuccessful", HttpStatus.BAD_REQUEST);
    }
    
    

}
