package SweetMeUp.projekt.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import SweetMeUp.projekt.DTO.OrderDTO;
import SweetMeUp.projekt.entity.CustomCakeNotification;
import SweetMeUp.projekt.entity.Order;
import SweetMeUp.projekt.requests.CustomizeInsert;
import SweetMeUp.projekt.requests.InsertBodyOrder;
import SweetMeUp.projekt.requests.SeenBody;
import SweetMeUp.projekt.requests.customBuyerOrderInsert;
import SweetMeUp.projekt.requests.customOrderInsert;
import SweetMeUp.projekt.security.CurrentUser;
import SweetMeUp.projekt.security.UserPrincipal;

@RestController
@CrossOrigin
@RequestMapping("order")
public class OrderController {
	
	private OrderDTO orderDao;
	
	public OrderController(OrderDTO orderDao) {
		this.orderDao = orderDao;
	}
	
	@PreAuthorize("hasRole('BUYER')")
	@PostMapping("sendCustomCakeRequest")
	public ResponseEntity<String> customCake(@CurrentUser UserPrincipal user, @RequestBody CustomizeInsert custom){
		
		if(orderDao.addCustomOrder(user.getId(), custom.getSellerId(), custom.getCustomImageUrl(), custom.getDescription(), custom.getIngredients(), custom.getDeliveryAt()))
			return new ResponseEntity<String>("Custom cake post was succesfull", HttpStatus.OK);
		
		return new ResponseEntity<String>("Custom cake post was unsuccesfull", HttpStatus.BAD_REQUEST);
	}
	
	@PreAuthorize("hasRole('SELLER')")
	@GetMapping("customOrderNotifications")
	public List<CustomCakeNotification> getcustomOrderNotificationsInPending(@CurrentUser UserPrincipal user){
		return orderDao.getNotifications(user.getId());
	}
	
	@PreAuthorize("hasRole('BUYER')")
	@GetMapping("customOrderNotificationsBuyer")
	public List<CustomCakeNotification> getcustomOrderNotificationsForBuyer(@CurrentUser UserPrincipal user){
		return orderDao.getCustomBuyerNotifications(user.getId());
	}
	
	@PreAuthorize("hasRole('BUYER')")
	@PostMapping("acceptOrRejectCustomBuyerNotification")
	public ResponseEntity<String> editCustomBuyerNotification(
			@CurrentUser UserPrincipal user, 
			@RequestBody customBuyerOrderInsert custom){
				
		if(custom.getStatus() != 2 && custom.getStatus() != -2)
			return new ResponseEntity<String>("Editing notification status was unsuccesfull because of wrong status",HttpStatus.BAD_REQUEST);
						
		if(orderDao.checkIfOrderIsBuyers(user.getId(), custom.getOrderId()))
			if(orderDao.checkIfOrderIsBuyerPending(custom.getOrderId()))
				if(orderDao.editCustomBuyerOrder(custom.getOrderId(), custom.getStatus()))
					return new ResponseEntity<String>("Editing notification status was succesfull",HttpStatus.OK);
		return new ResponseEntity<String>("Editing notification status was unsuccesfull",HttpStatus.BAD_REQUEST);
	}
	
	@PreAuthorize("hasRole('SELLER')")
	@PostMapping("acceptOrRejectCustomNotification")
	public ResponseEntity<String> editCustomNotification(
			@CurrentUser UserPrincipal user, 
			@RequestBody customOrderInsert custom){
				
		if(custom.getStatus() != 1 && custom.getStatus() != -1)
			return new ResponseEntity<String>("Editing notification status was unsuccesfull because of wrong status",HttpStatus.BAD_REQUEST);
				
		if(orderDao.checkIfOrderIsUsers(user.getId(), custom.getOrderId()))
			if(orderDao.checkIfOrderIsPending(custom.getOrderId()))
				if(orderDao.editCustomOrder(custom.getOrderId(), custom.getStatus(), custom.getPrice(), custom.getCurrencyId(), custom.getRejectReason()))
					return new ResponseEntity<String>("Editing notification status was succesfull",HttpStatus.OK);
		return new ResponseEntity<String>("Editing notification status was unsuccesfull",HttpStatus.BAD_REQUEST);
	}
	
	@PreAuthorize("hasRole('SELLER')")
	@GetMapping("getNormalNotifications")
	public List<Order> getNormalNotifications(@CurrentUser UserPrincipal user){
		return orderDao.getNormalNotifications(user.getId());
	}
	
	@PutMapping("seen")
	public ResponseEntity<String> setSeen(@CurrentUser UserPrincipal user, @RequestBody SeenBody seen){
		if(orderDao.checkIfOrderIsUsers(user.getId(), seen.getOrderId()) || orderDao.checkIfOrderIsBuyers(user.getId(), seen.getOrderId()))
			if(orderDao.setSeen(seen.getOrderId(), seen.isSeen()))
				return new ResponseEntity<String>("Editing seen status was succesfull",HttpStatus.OK);
		return new ResponseEntity<String>("Editing seen status was unsuccesfull",HttpStatus.BAD_REQUEST);
	}
	
	@PostMapping("checkout")
    public ResponseEntity<String> insertOrder(@CurrentUser UserPrincipal user, @RequestBody InsertBodyOrder insertBodyOrder) {
    	
        if (orderDao.insertOrder(user.getId(), insertBodyOrder.getPrice(), insertBodyOrder.getCurrencyId(), insertBodyOrder.getCakeList())) {
            return new ResponseEntity<String>("Order successfully added", HttpStatus.OK);
        }else {
            return new ResponseEntity<String>("Order placement was unsuccessful", HttpStatus.BAD_REQUEST);
        }
        
    }

}
