package SweetMeUp.projekt.controller;

import SweetMeUp.projekt.DTO.CakeDTO;
import SweetMeUp.projekt.DTO.OrderDTO;
import SweetMeUp.projekt.entity.AveragePrice;
import SweetMeUp.projekt.entity.Cake;
import SweetMeUp.projekt.entity.Order;
import SweetMeUp.projekt.entity.Seller;
import SweetMeUp.projekt.security.CurrentUser;
import SweetMeUp.projekt.security.UserPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "profile")
public class ProfileController {

    private OrderDTO orderDAO;
    private CakeDTO cakeDTO;

    public ProfileController(OrderDTO orderDAO, CakeDTO cakeDTO) {
        this.orderDAO = orderDAO;
        this.cakeDTO = cakeDTO;
    }

    @GetMapping("orderHistory")
    public List<Order> allOrders(@CurrentUser UserPrincipal user) {
    	
        return orderDAO.getOrders(user.getId());
        
    }
    
    @GetMapping("seller")
    public List<Cake> allCakes(@CurrentUser UserPrincipal user) {
    	
        return orderDAO.getSellerCakes(user.getId());
        
    }
    
    @GetMapping("sellers")
    public List<Seller> allSellers() {
    	
        return orderDAO.getSellers();
        
    }
    
    @GetMapping("seller/averageCakePrice")
    public List<AveragePrice> averagePrice(@RequestParam("sellerId") Long sellerId) {
    	return cakeDTO.getAveragePriceOfSeller(sellerId);
    }   

}
