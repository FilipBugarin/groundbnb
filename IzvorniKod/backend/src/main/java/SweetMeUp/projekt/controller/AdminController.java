package SweetMeUp.projekt.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import SweetMeUp.projekt.DTO.CakeDTO;
import SweetMeUp.projekt.DTO.IngredientDTO;
import SweetMeUp.projekt.requests.InsertBodyString;
import SweetMeUp.projekt.requests.InsertIngredientAdmin;

@RestController
@CrossOrigin
@PreAuthorize("hasRole('ADMIN')")
@RequestMapping("admin")
public class AdminController {
	
	private IngredientDTO ingredientDAO;

    public AdminController(IngredientDTO ingredientDAO, CakeDTO cakeDao) {
        this.ingredientDAO = ingredientDAO;
    }
	
    @PostMapping("addIngredientToList")
    public ResponseEntity<String> addIngredient(@RequestBody InsertIngredientAdmin insertIngredient) {
    	  	
        if (ingredientDAO.insertIngrediente(insertIngredient.getName(), insertIngredient.getAlergen())>0) {
            return new ResponseEntity<String>("Ingredient successfully added to IngredientList", HttpStatus.OK);
        }
        return new ResponseEntity<String>("Ingredient addition was unsuccessful", HttpStatus.BAD_REQUEST);
    }
    
    @DeleteMapping("deleteIngredient")
    public ResponseEntity<String> deleteIngdient(@RequestParam(value = "ingredientId") Long ingredientId) {
    	
    	int status = ingredientDAO.deleteIngredient(ingredientId);
    	
        if (status>0) {
            return new ResponseEntity<>("Ingredient successfully deleted", HttpStatus.OK);
        }else if(status==-1)
        	return new ResponseEntity<>("Ingredient used, cannot delete", HttpStatus.CONFLICT);
        return new ResponseEntity<String>("Ingredient deletion was unsuccessful", HttpStatus.BAD_REQUEST);
    }
    
    @PostMapping("addMetric")
    public ResponseEntity<String> addMetric(@RequestBody InsertBodyString metric) {
    	  	
        if (ingredientDAO.insertMetric(metric.getString())>0) {
            return new ResponseEntity<String>("Metric successfully added to Metrics", HttpStatus.OK);
        }
        return new ResponseEntity<String>("Metric addition was unsuccessful", HttpStatus.BAD_REQUEST);
    }
    
    @DeleteMapping("deleteMetric")
    public ResponseEntity<String> deleteMetric(@RequestParam(value = "metricId") Long metricId) {
    	
    	int status = ingredientDAO.deletemetric(metricId);
    	
        if (status>0) {
            return new ResponseEntity<>("Metric successfully deleted", HttpStatus.OK);
        }else if(status==-1)
        	return new ResponseEntity<>("Metric used, cannot delete", HttpStatus.CONFLICT);
        return new ResponseEntity<String>("Metric deletion was unsuccessful", HttpStatus.BAD_REQUEST);
    }

}
