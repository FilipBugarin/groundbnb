package SweetMeUp.projekt.controller;

import SweetMeUp.projekt.DTO.CakeDTO;
import SweetMeUp.projekt.entity.Cake;
import SweetMeUp.projekt.entity.Currency;
import SweetMeUp.projekt.requests.InsertBodyCake;
import SweetMeUp.projekt.requests.editBodyCake;
import SweetMeUp.projekt.security.CurrentUser;
import SweetMeUp.projekt.security.UserPrincipal;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("cakes")
public class CakeController {

    private final CakeDTO cakeDao;

    public CakeController(CakeDTO cakeDao) {
        this.cakeDao = cakeDao;
    }

    @GetMapping
    List<Cake> all () {
        return cakeDao.getCakes();
    }
    
    @GetMapping("currencies")
    List<Currency> currencies(){
    	return cakeDao.getCurrencies();
    }

    @PreAuthorize("hasRole('SELLER')")
    @PostMapping("add")
    public ResponseEntity<String> insertCake(@CurrentUser UserPrincipal user, @RequestBody InsertBodyCake insertBodyCake) {
    	
        if (cakeDao.insertCake(insertBodyCake.getName(), insertBodyCake.getDescription(), insertBodyCake.getImageurl(), insertBodyCake.getPrice(), insertBodyCake.getCurrencyId(), user.getId())) {
            return new ResponseEntity<String>("Cake successfully added", HttpStatus.OK);
        }
        return new ResponseEntity<String>("Cake addition was unsuccessful", HttpStatus.BAD_REQUEST);
    }
    
    
    @PreAuthorize("hasRole('SELLER')")
    @PutMapping("edit")
    public ResponseEntity<String> editCake(@CurrentUser UserPrincipal user, @RequestBody editBodyCake editBodyCake){
    	
    	if(!cakeDao.checkUserCake(user.getId(),editBodyCake.getCakeId()))
    		return new ResponseEntity<String>("That is not your cake", HttpStatus.UNAUTHORIZED);
    	
    	if(cakeDao.editCake(editBodyCake.getCakeId(),editBodyCake.getName(), editBodyCake.getDescription(), editBodyCake.getImageurl(), editBodyCake.getPrice(), editBodyCake.getCurrencyId()))
    		return new ResponseEntity<String>("Cake successfully added", HttpStatus.OK);
    	
    	
    	return new ResponseEntity<String>("Cake addition was unsuccessful", HttpStatus.BAD_REQUEST);
    }

    @PreAuthorize("hasRole('SELLER')")
    @DeleteMapping("delete")
    public ResponseEntity<String> deleteCake(@CurrentUser UserPrincipal user, @RequestParam(value = "cakeId") Long id) {
    	
    	if(!cakeDao.checkUserCake(user.getId(),id))
    		return new ResponseEntity<String>("That is not your cake", HttpStatus.UNAUTHORIZED);
    	
        if (cakeDao.deleteCake(id)) {
            return new ResponseEntity<>("Cake successfully deleted", HttpStatus.OK);
        }
        return new ResponseEntity<String>("Cake deletion was unsuccessful", HttpStatus.BAD_REQUEST);
    }

}
