package SweetMeUp.projekt.DTO;

import SweetMeUp.projekt.DAO.IngredientDAO;
import SweetMeUp.projekt.entity.Ingredient;
import SweetMeUp.projekt.entity.Metric;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IngredientDTO {

    private IngredientDAO ingredientDAO;

    public IngredientDTO(IngredientDAO ingredientDTO) {
        this.ingredientDAO = ingredientDTO;
    }

    public List<Ingredient> getIngredients() {
    	
    	List<Ingredient> ingredients = ingredientDAO.getAllIngrediants();
    	
    	ingredients.forEach(i -> {
    		i.setMetrics(ingredientDAO.getMetricForIngredient(i.getId()));
    	});
    	
        return ingredients;
    }

    public boolean insertIngredients(String name, String allergen) {
    	    	
        return ingredientDAO.insertIngredients(name, allergen)>0;
        
    }

	public boolean deleteIngredientFromCake(Long ingredientId, Long cakeId) {
		
		boolean deleted = ingredientDAO.delteIngredientFromCake(ingredientId, cakeId) > 0;
				
		return deleted;
	}

	public boolean insertIngredientToCake(Long ingredientId, Long cakeId, Float amount, Long metric) {
		if(ingredientDAO.checkIfIngredientOnCakeAlready(cakeId,ingredientId)==1)
			return updateIngredientToCake(ingredientId, cakeId, amount, metric);
		return ingredientDAO.addIngredientToCake(ingredientId, cakeId, amount, metric)>0;
	}
	
	public boolean updateIngredientToCake(Long ingredientId, Long cakeId, Float amount, Long metric) {
		return ingredientDAO.updateIngredient(ingredientId, cakeId, amount, metric)>0;
	}

	public List<Metric> getMetrics() {
		return ingredientDAO.getMetrics();		
	}

	public Integer insertIngrediente(String name, String alergen) {
		return ingredientDAO.insertIngredients(name, alergen);
	}

	public Integer deleteIngredient(Long ingredientId) {
		
		if(ingredientDAO.checkIfIngredientIsUsed(ingredientId))
			return -1;
		
		return ingredientDAO.deleteIngredient(ingredientId);
	}

	public int insertMetric(String metric) {
		return ingredientDAO.insertMetric(metric);
	}

	public int deletemetric(Long metricId) {
		
		if(ingredientDAO.checkIfMetricIsUsed(metricId))
			return -1;
		
		return ingredientDAO.deleteMetric(metricId);
	}
}
