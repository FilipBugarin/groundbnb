package SweetMeUp.projekt.DTO;

import SweetMeUp.projekt.DAO.CakeDAO;
import SweetMeUp.projekt.DAO.IngredientDAO;
import SweetMeUp.projekt.entity.AveragePrice;
import SweetMeUp.projekt.entity.Cake;
import SweetMeUp.projekt.entity.Currency;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CakeDTO {

    private CakeDAO cakeDAO;
    private IngredientDAO ingredientDAO;
    private OrderDTO orderDTO;

    public CakeDTO(CakeDAO cakeDTO, IngredientDAO ingredientDTO, OrderDTO orderDTO) {
        this.cakeDAO = cakeDTO;
        this.ingredientDAO = ingredientDTO;
        this.orderDTO = orderDTO;
    }

    public List<Cake> getCakes() {

        List<Cake> result = cakeDAO.getAllCakes();
        result.forEach(c -> {
        	c.setIngredients(ingredientDAO.getAllIngrediantsForCake(c.getId()));
        	c.setCurrency(cakeDAO.getCurrencyById(c.getCurrency().getCurrencyId()));
        });
        
        return result;
    }

    public boolean insertCake(String name, String description, String imageurl, Float price, Long currencyId, Long sellerId) {
    	return cakeDAO.insertCake(name, description, imageurl, price, currencyId,sellerId) > 0;
    }
    
    public boolean editCake(Long cakeId, String name, String description, String imageurl, Float price, Long currencyId) {
		return cakeDAO.editCake(cakeId, name, description, imageurl, price, currencyId) > 0;
	}

    public boolean deleteCake(Long id) {
    	return cakeDAO.deleteCake(id) > 0;
    }

	public boolean checkUserCake(Long userId, Long cakeId) {
		return cakeDAO.checkUserCake(userId, cakeId);
	}

	public List<Currency> getCurrencies() {
		return cakeDAO.getCurrencies();
	}

	public List<AveragePrice> getAveragePriceOfSeller(Long sellerId) {
		
		List<Cake> cakes = orderDTO.getSellerCakes(sellerId);
		List<Currency> currencies = cakeDAO.getCurrencies();
	
		Double avg = 0.;
		Integer counter=0;
		
		for(Cake c:cakes) {
			for(Currency cur:currencies) {				
				if(c.getCurrency().getCurrencyName().equals(cur.getCurrencyName())) {
					counter++;
					avg+=c.getPrice()/cur.getRatioToEuro();
					break;
				}
			}
		}
		
		List<AveragePrice> averagePrices = new ArrayList<>();
		
		for(Currency cur:currencies) {				
			Double avgInCur = (double) (Math.round(avg*100/counter)/(double)100);
			if(!cur.getCurrencyName().equals("Euro"))
				avgInCur=(double) (Math.round(avgInCur*100*cur.getRatioToEuro())/(double)100);
			averagePrices.add(new AveragePrice(avgInCur, cur));
		}
				
		return averagePrices;
	}

	
}
