package SweetMeUp.projekt.DTO;

import SweetMeUp.projekt.DAO.CakeDAO;
import SweetMeUp.projekt.DAO.IngredientDAO;
import SweetMeUp.projekt.DAO.OrderDAO;
import SweetMeUp.projekt.DAO.UserDAO;
import SweetMeUp.projekt.entity.AveragePrice;
import SweetMeUp.projekt.entity.Cake;
import SweetMeUp.projekt.entity.CakeList;
import SweetMeUp.projekt.entity.Currency;
import SweetMeUp.projekt.entity.CustomCakeNotification;
import SweetMeUp.projekt.entity.Order;
import SweetMeUp.projekt.entity.Seller;
import SweetMeUp.projekt.requests.CakeListInsert;
import SweetMeUp.projekt.requests.CustomIngredient;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class OrderDTO {
    private OrderDAO orderDAO;
    private IngredientDAO ingredientDAO;
    private CakeDAO cakeDAO;
    private UserDAO userDAO;

    public OrderDTO(OrderDAO orderDTO, IngredientDAO ingredientDTO, CakeDAO cakeDTO, UserDAO userDTO) {

        this.orderDAO = orderDTO;
        this.ingredientDAO = ingredientDTO;
        this.cakeDAO = cakeDTO;
        this.userDAO = userDTO;
    }

    public List<Order> getOrders(Long userId) {
        List<Order> result = orderDAO.getAllOrders(userId);

        result.forEach(c -> {
        	c.setCakeList(cakeDAO.getCakesByUser(userId,c.getOrderId()));
        	c.getCakeList().forEach(cl -> {
        		cl.getCake().setIngredients(ingredientDAO.getAllIngrediantsForCake(cl.getCake().getId()));
        		cl.getCake().setCurrency(cakeDAO.getCurrencyById(cl.getCake().getCurrency().getCurrencyId()));
        	});
        });
        return result;
    }

    public List<Cake> getSellerCakes(Long userId) {
        List<Cake> result = orderDAO.GetSellerCakes(userId);
        result.forEach(c -> {
        	c.setIngredients(ingredientDAO.getAllIngrediantsForCake(c.getId()));
        	c.setCurrency(cakeDAO.getCurrencyById(c.getCurrency().getCurrencyId()));
        });
        return result;
    }

    public boolean insertOrder(Long id, Double price, Long currencyId, List<CakeListInsert> list) {
        return (orderDAO.insertOrder(id, price, currencyId, list) > 0);
    }

	public List<Seller> getSellers() {
		
		List<Seller> sellers = userDAO.findAllSellers();
		
		for(Seller s:sellers) {
			List<Cake> cakes = orderDAO.GetSellerCakes(s.getId());
			List<Currency> currencies = cakeDAO.getCurrencies();
		
			Double avg = 0.;
			Integer counter=0;
			
			for(Cake c:cakes) {
				c.setCurrency(cakeDAO.getCurrencyById(c.getCurrency().getCurrencyId()));
				for(Currency cur:currencies) {				
					if(c.getCurrency().getCurrencyName().equals(cur.getCurrencyName())) {
						counter++;
						avg+=c.getPrice()/cur.getRatioToEuro();
						break;
					}
				}
			}
			
			List<AveragePrice> averagePrices = new ArrayList<>();
			
			for(Currency cur:currencies) {				
				Double avgInCur = (double) (Math.round(avg*100/counter)/(double)100);
				if(!cur.getCurrencyName().equals("Euro"))
					avgInCur=(double) (Math.round(avgInCur*100*cur.getRatioToEuro())/(double)100);
				averagePrices.add(new AveragePrice(avgInCur, cur));
			}
			
			s.setAveragePrices(averagePrices);
		}
		
		
		return sellers;
	}

	public boolean addCustomOrder(Long buyerId, Long sellerId, String customImageUrl, String description, List<CustomIngredient> ingredients, Date deliveryAt) {
		Integer orderId = orderDAO.insertCustomOrder(buyerId, sellerId, customImageUrl, description, deliveryAt);
		if(orderId>0) {
			ingredients.forEach(i -> {
				orderDAO.addToCustomOrderIngredients(orderId, i.getIngredientId(), i.getAmount(), i.getMetricId());
			});
			return true;
		}
		
		return false;
	}

	public List<CustomCakeNotification> getNotifications(Long userId) {
		
		List<CustomCakeNotification> notifications = orderDAO.getNotifications(userId);
		
		notifications.forEach(n -> {
			n.setIngredients(ingredientDAO.getIngredientsForOrder(n.getOrderId()));
			n.setCurrency(cakeDAO.getCurrencyById(n.getCurrency().getCurrencyId()));
		});
		
		return notifications;
	}

	public boolean checkIfOrderIsUsers(Long id, Long orderId) {
		return orderDAO.checkIfOrderIsUsers(id,orderId);
	}
	
	public boolean checkIfOrderIsBuyers(Long id, Long orderId) {
		return orderDAO.checkIfOrderIsBuyers(id,orderId);
	}
	
	public boolean editCustomOrder(Long orderId, Integer status, Double price, Integer currencyId, String rejectReason) {
		return orderDAO.editCustomOrder(orderId,status, price, currencyId, rejectReason);
	}

	public boolean editCustomBuyerOrder(Long orderId, Integer status) {
		return orderDAO.editCustomOrder(orderId,status);
	}
	
	public boolean checkIfOrderIsPending(Long orderId) {
		return orderDAO.checkIfOrderIsPending(orderId);
	}
	
	public boolean checkIfOrderIsBuyerPending(Long orderId) {
		return orderDAO.checkIfOrderIsBuyerPending(orderId);
	}

	public List<Order> getNormalNotifications(Long sellerId) {
		
		List<Order> orders = orderDAO.getNormalNotifications(sellerId);
		List<Currency> currencies = cakeDAO.getCurrencies();
		
		orders.forEach(c -> {
        	c.setCakeList(cakeDAO.getCakesByOrder(c.getOrderId()));
        	c.getCakeList().forEach(cl -> {
        		cl.getCake().setIngredients(ingredientDAO.getAllIngrediantsForCake(cl.getCake().getId()));
        		cl.getCake().setCurrency(cakeDAO.getCurrencyById(cl.getCake().getCurrency().getCurrencyId()));
        	});
        });
		
	
		
		for(Order o:orders) {
			Double price = 0.;
			for(CakeList c:o.getCakeList()) {
				
				for(Currency cur:currencies) {				
					if(c.getCake().getCurrency().getCurrencyName().equals(cur.getCurrencyName())) {
						price=price + (c.getCake().getPrice()/cur.getRatioToEuro())*c.getQuantity();
						break;
					}
				}
			}
			
			o.setPrice(o.getPrice()!=null?o.getPrice():0. + price);
			o.setCurrency(currencies.get(0));
		}
		
		return orders;
	}

	public List<CustomCakeNotification> getCustomBuyerNotifications(Long userId) {
		
		List<CustomCakeNotification> notifications = orderDAO.getBuyerCustomNotifications(userId);
		
		notifications.forEach(n -> {
			n.setIngredients(ingredientDAO.getIngredientsForOrder(n.getOrderId()));
			n.setCurrency(cakeDAO.getCurrencyById(n.getCurrency().getCurrencyId()));
		});
		
		return notifications;
	}

	public boolean setSeen(Long orderId, boolean seen) {
		return orderDAO.setSeen(orderId, seen);
	}

}
