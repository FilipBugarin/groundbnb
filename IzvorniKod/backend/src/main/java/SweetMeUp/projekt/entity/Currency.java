package SweetMeUp.projekt.entity;

public class Currency {

	private Long currencyId;
	private String currencyName;
	private Double ratioToEuro;
	private String symbol;
	
	public Currency() {}
	
	public Currency(Long currencyId) {
		this.currencyId = currencyId;
	}

	public Long getCurrencyId() {
		return currencyId;
	}
	public void setCurrencyId(Long currencyId) {
		this.currencyId = currencyId;
	}
	public String getCurrencyName() {
		return currencyName;
	}
	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}
	public Double getRatioToEuro() {
		return ratioToEuro;
	}
	public void setRatioToEuro(Double ratioToEuro) {
		this.ratioToEuro = ratioToEuro;
	}
	public String getSymbol() {
		return symbol;
	}
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	
	
	
}
