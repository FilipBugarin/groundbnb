package SweetMeUp.projekt.entity;

import java.util.List;

public class Seller {

	private Long id;
    private String name;
    private String profileImageUrl;
    private List<AveragePrice> averagePrices;
    
	public Seller() {}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getProfileImageUrl() {
		return profileImageUrl;
	}
	public void setProfileImageUrl(String profileImageUrl) {
		this.profileImageUrl = profileImageUrl;
	}
	public List<AveragePrice> getAveragePrices() {
		return averagePrices;
	}
	public void setAveragePrices(List<AveragePrice> averagePrices) {
		this.averagePrices = averagePrices;
	}
	
	
}
