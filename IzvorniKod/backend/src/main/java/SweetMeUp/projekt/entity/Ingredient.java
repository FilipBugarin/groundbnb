package SweetMeUp.projekt.entity;

import java.util.List;

public class Ingredient {
	private Long id;
	private String name;
	private String allergen;
	private List<Metric> metrics;
	
	public Ingredient() {}

	public Ingredient(String name, String allergen) {
		this.name = name;
		this.allergen = allergen;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAllergen() {
		return allergen;
	}

	public void setAllergen(String allergen) {
		this.allergen = allergen;
	}
	public List<Metric> getMetrics() {
		return metrics;
	}
	public void setMetrics(List<Metric> metrics) {
		this.metrics = metrics;
	}
	

}
