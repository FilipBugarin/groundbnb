package SweetMeUp.projekt.entity;

public class AveragePrice {

	private Double avg;
	private Currency currency;
	
	public AveragePrice(Double avg, Currency currency) {
		this.avg = avg;
		this.currency = currency;
	}
	public Double getAvg() {
		return avg;
	}
	public void setAvg(Double avg) {
		this.avg = avg;
	}
	public Currency getCurrency() {
		return currency;
	}
	public void setCurrency(Currency currency) {
		this.currency = currency;
	}
	
	
}
