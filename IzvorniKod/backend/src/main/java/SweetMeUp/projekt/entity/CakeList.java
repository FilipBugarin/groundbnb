package SweetMeUp.projekt.entity;

public class CakeList {

    private Cake cake;
    private Integer quantity;

    public CakeList() {}

	public Cake getCake() {
		return cake;
	}

	public void setCake(Cake cake) {
		this.cake = cake;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	};
    
    
}

