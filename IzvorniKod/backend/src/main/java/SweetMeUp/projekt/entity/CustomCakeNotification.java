package SweetMeUp.projekt.entity;

import java.util.Date;
import java.util.List;

import SweetMeUp.projekt.security.audit.DateAudit;

public class CustomCakeNotification extends DateAudit{

	private static final long serialVersionUID = 1L;
	private Long OrderId;
	private Long buyerId;
	private String buyerName;
	private Double price;
	private Currency currency;
	private String imageUrl;
	private String description;
	private Integer status;
	private List<CakesIngredient> ingredients;
	private Date deliveryAt;
	private String rejectReason;
	private Long sellerId;
	private String sellerName;
	private boolean seen;
	
	public CustomCakeNotification() {}
		
	public Long getOrderId() {
		return OrderId;
	}
	public void setOrderId(Long orderId) {
		OrderId = orderId;
	}
	public Long getBuyerId() {
		return buyerId;
	}
	public void setBuyerId(Long buyerId) {
		this.buyerId = buyerId;
	}
	public String getBuyerName() {
		return buyerName;
	}
	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public List<CakesIngredient> getIngredients() {
		return ingredients;
	}
	public void setIngredients(List<CakesIngredient> ingredients) {
		this.ingredients = ingredients;
	}
	
	public Date getDeliveryAt() {
		return deliveryAt;
	}

	public void setDeliveryAt(Date deliveryAt) {
		this.deliveryAt = deliveryAt;
	}

	public String getRejectReason() {
		return rejectReason;
	}
	
	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public Long getSellerId() {
		return sellerId;
	}

	public void setSellerId(Long sellerId) {
		this.sellerId = sellerId;
	}

	public String getSellerName() {
		return sellerName;
	}

	public void setSellerName(String sellerName) {
		this.sellerName = sellerName;
	}
	public boolean isSeen() {
		return seen;
	}
	public void setSeen(boolean seen) {
		this.seen = seen;
	}
	
	
	
	
}
