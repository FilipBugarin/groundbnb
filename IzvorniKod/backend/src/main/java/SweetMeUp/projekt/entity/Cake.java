package SweetMeUp.projekt.entity;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Cake {
	
	private Integer id;
	private String name;
	private String description;
	private String imageurl;
	private Float price;
	private List<CakesIngredient> ingredients;
	private Currency currency;
	@JsonIgnore
	private boolean deleted;
	
	public Cake(String name, String description, String imageurl, float price, List<CakesIngredient> ingredients, Currency currency) {
		this.name = name;
		this.description = description;
		this.imageurl = imageurl;
		this.price = price;
		this.ingredients = ingredients;
		this.currency = currency;
		this.deleted = false;
	}

	public Cake() {}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImageurl() {
		return imageurl;
	}

	public void setImageurl(String imageurl) {
		this.imageurl = imageurl;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public List<CakesIngredient> getIngredients() {
		return ingredients;
	}

	public void setIngredients(List<CakesIngredient> ingredients) {
		this.ingredients = ingredients;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
		
}
