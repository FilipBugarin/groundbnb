package SweetMeUp.projekt.entity;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

public class SellersOrderSetExtractor implements ResultSetExtractor<Order>{

	@Override
	public Order extractData(ResultSet rs) throws SQLException, DataAccessException {
		
		Order notification = new Order();
		
		notification.setOrderId(rs.getLong("id"));
		notification.setBuyerId(rs.getLong("buyerid"));
		notification.setBuyerName(rs.getString("name"));
		notification.setCakeList(null);
		notification.setCreated_at(rs.getString("created_at"));
		notification.setUpdated_at(rs.getString("updated_at"));
		notification.setSeen(rs.getBoolean("seen"));
		
		return notification;
	}

}
