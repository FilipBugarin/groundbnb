package SweetMeUp.projekt.entity;

public class Metric {

	private Long metricId;
	private String metricName;
	
	public Metric() {}
	
	public Long getMetricId() {
		return metricId;
	}
	public void setMetricId(Long metricId) {
		this.metricId = metricId;
	}
	public String getMetricName() {
		return metricName;
	}
	public void setMetricName(String metricName) {
		this.metricName = metricName;
	}
	
	
}
