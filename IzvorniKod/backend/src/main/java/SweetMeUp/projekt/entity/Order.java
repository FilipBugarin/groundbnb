package SweetMeUp.projekt.entity;

import java.util.List;

public class Order {
	
    private Long orderId;
    private Long buyerId;
    private String buyerName;
    private List<CakeList> cakeList;
    private Double price;
    private Currency currency;
    private String createdAt;
    private String updatedAt;
    private boolean seen;
    
	public Order() {}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public Long getBuyerId() {
		return buyerId;
	}

	public void setBuyerId(Long buyerId) {
		this.buyerId = buyerId;
	}

	public List<CakeList> getCakeList() {
		return cakeList;
	}

	public void setCakeList(List<CakeList> cakeList) {
		this.cakeList = cakeList;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrencyId(Currency currency) {
		this.currency = currency;
	}

	public String getCreated_at() {
		return createdAt;
	}

	public void setCreated_at(String created_at) {
		this.createdAt = created_at;
	}

	public String getUpdated_at() {
		return updatedAt;
	}

	public void setUpdated_at(String updated_at) {
		this.updatedAt = updated_at;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}
	public String getBuyerName() {
		return buyerName;
	}
	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}
	public boolean isSeen() {
		return seen;
	}
	public void setSeen(boolean seen) {
		this.seen = seen;
	}

	

   
}
