package SweetMeUp.projekt.entity;

public class CakesIngredient {
	
	private Integer id;
	private String name;
	private String allergen;
	private Long amount;
	private String metric;

	public CakesIngredient() {}

	public CakesIngredient(String name, String allergen, Long amount, String metric) {
		this.name = name;
		this.allergen = allergen;
		this.amount = amount;
		this.metric = metric;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAllergen() {
		return allergen;
	}

	public void setAllergen(String allergen) {
		this.allergen = allergen;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public String getMetric() {
		return metric;
	}

	public void setMetric(String metric) {
		this.metric = metric;
	}
		
}
