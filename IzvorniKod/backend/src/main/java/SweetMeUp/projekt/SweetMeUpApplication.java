package SweetMeUp.projekt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SweetMeUpApplication {

	public static void main(String[] args) {
		SpringApplication.run(SweetMeUpApplication.class, args);
		System.out.println("running");
	}

}
