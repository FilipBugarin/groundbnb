package SweetMeUp.projekt.DAO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import SweetMeUp.projekt.entity.auth.Role;
import SweetMeUp.projekt.mappers.RoleRowMapper;

@Repository
public class RoleDAO {
	
	@Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;
	
	public Role getRoleById(long id) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
    	parameters.addValue("id", id);
    	
    	return jdbcTemplate.queryForObject(
    			"select * from roles r where r.id = :id", parameters, new RoleRowMapper());
	}
}
