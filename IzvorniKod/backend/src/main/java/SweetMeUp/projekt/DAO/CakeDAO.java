package SweetMeUp.projekt.DAO;

import SweetMeUp.projekt.entity.Cake;
import SweetMeUp.projekt.entity.CakeList;
import SweetMeUp.projekt.entity.Currency;
import SweetMeUp.projekt.mappers.CakeListRowMapper;
import SweetMeUp.projekt.mappers.CakeRowMapper;
import SweetMeUp.projekt.mappers.CurrencyRowMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CakeDAO {

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    public List<Cake> getAllCakes() {

        return jdbcTemplate.query("select c.id, c.name, c.description,c.imageUrl, c.price, c.currencyId as currency from cake c where deleted!=true", new CakeRowMapper());

    }
    
    public List<Currency> getCurrencies() {
		return jdbcTemplate.query("select * from currency", new CurrencyRowMapper());
	}
    
    public Currency getCurrencyById(Long currencyId) {
    	
    	if(currencyId == 0)
    		return null;
    	
    	MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("currencyId", currencyId);
    	return jdbcTemplate.queryForObject("select * from currency where currencyId = :currencyId", parameters, new CurrencyRowMapper());	
	}

    public Integer insertCake(String name, String description, String imageurl, Float price, Long currencyId, Long sellerId) {
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("name", name);
        parameters.addValue("description", description);
        parameters.addValue("imageurl", imageurl);
        parameters.addValue("price", price);
        parameters.addValue("currencyId", currencyId);
        
        Integer cakeId=jdbcTemplate.queryForObject("insert into cake (name, description, imageurl, price, CurrencyId,deleted) values (:name, :description, :imageurl, :price, :currencyId, false) returning Id", parameters, Integer.class);
        
        parameters.addValue("sellerId", sellerId);
        parameters.addValue("cakeId", cakeId);
        
        return jdbcTemplate.update("insert into sellercakes (sellerid,cakeid) values (:sellerId,:cakeId)", parameters);
    }
    
    public int editCake(Long cakeId, String name, String description, String imageurl, Float price, Long currencyId) {
		
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("cakeId", cakeId);
        parameters.addValue("name", name);
        parameters.addValue("description", description);
        parameters.addValue("imageurl", imageurl);
        parameters.addValue("price", price);
        parameters.addValue("currencyId", currencyId);
		
		return jdbcTemplate.update("update cake set name = :name, description = :description, imageurl = :imageurl, price = :price, currencyId = :currencyId where id = :cakeId", parameters);
	}

    public Integer deleteCake(Long id) {
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("id", id);
        return jdbcTemplate.update("update cake set deleted = true where cake.id = :id", parameters);
    }

	public List<CakeList> getCakesByUser(Long buyerId, Long orderId) {
		
		MapSqlParameterSource parameters = new MapSqlParameterSource();
	    parameters.addValue("buyerId", buyerId);
	    parameters.addValue("orderId", orderId);
		
		return jdbcTemplate.query(
				"select c.id, c.name, c.description,c.imageUrl, c.price, c.currencyId as currency, "
				+ "cl.quantity "
				+ "from cake c "
				+ "left join cakelist cl ON c.id =cl.cakeid "
				+ "left join orders o on o.id = cl.orderId "
				+ "where o.buyerid = :buyerId and o.id = :orderId", parameters,new CakeListRowMapper());    
	}

	public boolean checkUserCake(Long sellerId, Long cakeId) {
		
		MapSqlParameterSource parameters = new MapSqlParameterSource();
	    parameters.addValue("sellerId", sellerId);
	    parameters.addValue("cakeId", cakeId);
		
		return jdbcTemplate.queryForObject("select COUNT(*) from sellercakes where sellerid=:sellerId and cakeid=:cakeId", parameters, Integer.class)==1;
	}

	public List<CakeList> getCakesByOrder(Long orderId) {
		
		MapSqlParameterSource parameters = new MapSqlParameterSource();
	    parameters.addValue("orderId", orderId);
		
	    return jdbcTemplate.query(
	    		"select c.id, c.name, c.description,c.imageUrl, c.price, c.currencyId as currency, "
				+ "cl.quantity "
				+ "from cake c "
				+ "left join cakelist cl ON c.id =cl.cakeid "
				+ "left join orders o on o.id = cl.orderId "
				+ "where o.id = :orderId", parameters,new CakeListRowMapper());
	}

	

	
}
