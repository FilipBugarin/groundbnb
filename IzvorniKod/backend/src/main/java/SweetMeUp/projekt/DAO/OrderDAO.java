package SweetMeUp.projekt.DAO;


import SweetMeUp.projekt.entity.Cake;
import SweetMeUp.projekt.entity.CustomCakeNotification;
import SweetMeUp.projekt.entity.Order;
import SweetMeUp.projekt.mappers.CakeRowMapper;
import SweetMeUp.projekt.mappers.CustomCakeBuyerNotificationRowMapper;
import SweetMeUp.projekt.mappers.CustomCakeNotificationRowMapper;
import SweetMeUp.projekt.mappers.OrderRowMapper;
import SweetMeUp.projekt.mappers.SellersOrderRowMapper;
import SweetMeUp.projekt.requests.CakeListInsert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Repository
public class OrderDAO {

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    public List<Order> getAllOrders(Long userId) {
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("userId", userId);
        return jdbcTemplate.query(
        		"SELECT * "
        		+ "FROM orders "
        		+ "WHERE orders.buyerid = :userId",parameters, new OrderRowMapper());
    }
    
    public List<Cake> GetSellerCakes(Long userId) {
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("userId", userId);
        return jdbcTemplate.query(
        		"SELECT "
        		+ "cake.id, "
        		+ "name, "
        		+ "cake.description, "
        		+ "imageurl, "
        		+ "price, "
        		+ "currencyid as currency "
        		+ "FROM sellercakes LEFT JOIN cake ON sellercakes.cakeid = cake.id "
        		+ "WHERE sellercakes.sellerid = :userId "
        		+ "and deleted!=true",parameters, new CakeRowMapper());
    }

    public Integer insertOrder(Long buyerId, Double price, Long currencyId, List<CakeListInsert> list) {

    	 //INSERTING ORDER
        MapSqlParameterSource parametersOrder = new MapSqlParameterSource();
        parametersOrder.addValue("buyerId", buyerId);
        parametersOrder.addValue("price", price);
        parametersOrder.addValue("currencyId", currencyId);
        Integer orderId = jdbcTemplate.queryForObject("INSERT INTO orders (buyerid, price, currencyId) VALUES (:buyerId, :price, :currencyId) returning id ", parametersOrder, Integer.class);
    	
        //INSERTING CAKELIST
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        for (CakeListInsert c: list) {
        	parameters.addValue("orderId", orderId);
            parameters.addValue("cakeId", c.getCakeId());
            parameters.addValue("quantity", c.getQuantity());
            
            jdbcTemplate.update("INSERT INTO cakelist (orderId, cakeId, quantity) VALUES (:orderId, :cakeId, :quantity)", parameters);
        }
        
        return jdbcTemplate.queryForObject("SELECT COUNT(*) from orders where orders.id = :orderId", parameters, Integer.class);



    }

	public Integer insertCustomOrder(Long buyerId, Long sellerId, String customImageUrl,String description, Date deliveryAt) {
		
		MapSqlParameterSource parametersOrder = new MapSqlParameterSource();
        parametersOrder.addValue("buyerId", buyerId);
        parametersOrder.addValue("deliveryAt", deliveryAt);
        Integer orderId = jdbcTemplate.queryForObject("INSERT INTO orders (buyerid) VALUES (:buyerId) returning id ", parametersOrder, Integer.class);
        
        parametersOrder = new MapSqlParameterSource();
        parametersOrder.addValue("orderId", orderId);
        parametersOrder.addValue("sellerId", sellerId);
        parametersOrder.addValue("customImageUrl", customImageUrl);
        parametersOrder.addValue("description", description);
        parametersOrder.addValue("deliveryAt", deliveryAt);

		return jdbcTemplate.queryForObject("INSERT INTO customorder "
				+ "(orderId, sellerId, imageUrl, description, status, deliveryAt) VALUES (:orderId, :sellerId, :customImageUrl, :description, 0, :deliveryAt) returning orderId",parametersOrder, Integer.class);
	}

	public void addToCustomOrderIngredients(Integer orderId, Long ingredientId, Float amount, Long metricId) {
		
		MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("orderId", orderId);
        parameters.addValue("ingredientId", ingredientId);
        parameters.addValue("amount", amount);
        parameters.addValue("metricId", metricId);
        
        jdbcTemplate.update("INSERT INTO customorderingredients "
				+ "(orderId, IngredientId, Amount, Metric_id) VALUES (:orderId, :ingredientId, :amount, :metricId) ", parameters);
		
	}

	public List<CustomCakeNotification> getNotifications(Long userId) {
		
		MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("userId", userId);
		
		return jdbcTemplate.query(
				"select o.id, u.id as userId , u.\"name\", o.price, o.currencyId , c.imageurl ,c.description, c.status, o.created_at, c.rejectReason, c.deliveryAt, o.updated_at, o.seen from orders o "
				+ "left join customorder c on o.id =c.orderid "
				+ "left join users u on o.buyerid = u.id "
				+ "where c.sellerid = :userId", parameters,new CustomCakeNotificationRowMapper());
	}

	public boolean checkIfOrderIsUsers(Long userId, Long orderId) {
		
		MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("userId", userId);
        parameters.addValue("orderId", orderId);
		
		return jdbcTemplate.queryForObject("select count(*) from orders o left join customorder c on o.id = c.orderid where o.id = :orderId and c.sellerid = :userId", parameters, Integer.class) == 1 ||
			   jdbcTemplate.queryForObject("select count(*) from orders o left join cakelist c on o.id = c.orderid "
			   		+ "left join sellercakes sc on c.cakeid = sc.cakeid where o.id = :orderId and sc.sellerid = :userId", parameters, Integer.class) >= 1;
	}

	public boolean checkIfOrderIsBuyers(Long userId, Long orderId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("userId", userId);
        parameters.addValue("orderId", orderId);
		
		return jdbcTemplate.queryForObject("select count(*) from orders o left join customorder c on o.id = c.orderid where o.id = :orderId and o.buyerId = :userId", parameters, Integer.class) == 1;
	}
	
	public boolean checkIfOrderIsPending(Long orderId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("orderId", orderId);
        
		return jdbcTemplate.queryForObject("select count(*) from customorder c where c.orderId = :orderId and status=0", parameters, Integer.class) == 1;
	}
	
	public boolean checkIfOrderIsBuyerPending(Long orderId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("orderId", orderId);
        
		return jdbcTemplate.queryForObject("select count(*) from customorder c where c.orderId = :orderId and status=1", parameters, Integer.class) == 1;
	}

	public List<Order> getNormalNotifications(Long sellerId) {
		
		MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("sellerId", sellerId);
		
		return jdbcTemplate.query(
				"select distinct(o.id),o.buyerid,u.\"name\", o.created_at,o.updated_at,o.seen from orders o "
				+ "left join cakelist c on o.id = c.orderid "
				+ "left join sellercakes s on c.cakeid = s.cakeid "
				+ "left join users u on o.buyerid = u.id "
				+ "where s.sellerid = :sellerId", parameters, new SellersOrderRowMapper());
	}

	public boolean editCustomOrder(Long orderId, Integer status, Double price, Integer currencyId, String rejectReason) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("orderId", orderId);
        parameters.addValue("status", status);
        parameters.addValue("price", price);
        parameters.addValue("currencyId", currencyId);
        parameters.addValue("rejectReason", rejectReason);
        parameters.addValue("now", new Timestamp(System.currentTimeMillis()));
        
                
        if(status==-1)
        	return jdbcTemplate.update("update customOrder set status = :status, rejectReason = :rejectReason where orderId=:orderId", parameters)==1 && 
     			   jdbcTemplate.update("update orders set updated_at = :now where id=:orderId", parameters)==1;
		
		return jdbcTemplate.update("update customOrder set status = :status where orderId=:orderId", parameters)==1 &&
			   jdbcTemplate.update("update Orders set price = :price,currencyId = :currencyId, updated_at = :now  where id=:orderId", parameters)==1;
		
			
	}
	
	public boolean editCustomOrder(Long orderId, Integer status) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("orderId", orderId);
        parameters.addValue("status", status);
        parameters.addValue("now", new Timestamp(System.currentTimeMillis()));
		
		return jdbcTemplate.update("update customOrder set status = :status where orderId=:orderId", parameters)==1 && 
			   jdbcTemplate.update("update orders set updated_at = :now where id=:orderId", parameters)==1;
	}

	public List<CustomCakeNotification> getBuyerCustomNotifications(Long userId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("userId", userId);
		
		return jdbcTemplate.query(
				"select o.id, u.id as userId , u.\"name\", o.price, o.currencyId , c.imageurl ,c.description, c.status, o.created_at, c.rejectReason, c.deliveryAt, c.rejectReason, o.updated_at, o.seen from orders o "
				+ "left join customorder c on o.id =c.orderid "
				+ "left join users u on c.sellerId = u.id "
				+ "where o.buyerid = :userId", parameters,new CustomCakeBuyerNotificationRowMapper());
	}

	public boolean setSeen(Long orderId, boolean seen) {
		
		MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("orderId", orderId);
        parameters.addValue("seen", seen);
        parameters.addValue("now", new Timestamp(System.currentTimeMillis()));
		
        return jdbcTemplate.update("update orders set seen = :seen, updated_at = :now where id=:orderId", parameters)==1;
	}

}
