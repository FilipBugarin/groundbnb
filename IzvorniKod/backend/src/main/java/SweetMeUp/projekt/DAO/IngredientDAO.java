package SweetMeUp.projekt.DAO;

import SweetMeUp.projekt.entity.CakesIngredient;
import SweetMeUp.projekt.entity.Ingredient;
import SweetMeUp.projekt.entity.Metric;
import SweetMeUp.projekt.mappers.CakesIngredientRowMapper;
import SweetMeUp.projekt.mappers.IngredientRowMapper;
import SweetMeUp.projekt.mappers.MetricRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class IngredientDAO {

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    public List<Ingredient> getAllIngrediants() {

        return jdbcTemplate.query("select i.id,i.\"name\",i.allergen from ingredient i", new IngredientRowMapper());

    }

    public List<CakesIngredient> getAllIngrediantsForCake(Integer cakeId) {

        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("cakeId", cakeId);

        return jdbcTemplate.query("select "
        		+ "i.id, "
        		+ "i.name, "
        		+ "i.allergen, "
        		+ "c.amount, "
        		+ "m.Metric_name "
        		+ "from ingredient i "
        		+ "left join cakesingredients c on i.id = c.ingredientid "
        		+ "left join Metrics m on c.Metric_id = m.Metric_id "
        		+ "where c.cakeid = :cakeId", parameters, new CakesIngredientRowMapper());

    }

    public Integer insertIngredients(String name, String allergen) {
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("name", name);
        parameters.addValue("allergen", allergen);

        return jdbcTemplate.queryForObject("insert into ingredient (name, allergen) values (:name, :allergen) returning id", parameters, Integer.class);
    }

    public Integer deleteIngredient(Long ingredientId) {
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("id", ingredientId);
        return jdbcTemplate.update("delete from ingredient where ingredient.id = :id", parameters);
    }

	public Integer delteIngredientFromCake(Long ingredientId, Long cakeId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("ingredientId", ingredientId);
        parameters.addValue("cakeId", cakeId);
        
        return jdbcTemplate.update("delete from cakesingredients where cakeid = :cakeId and ingredientid = :ingredientId", parameters);
	}

	public boolean checkIfIngredientIsUsed(Long ingredientId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("ingredientId", ingredientId);
        
		return jdbcTemplate.queryForObject("SELECT count(*) from cakesingredients where ingredientid = :ingredientId", parameters, Integer.class)>0;
	}

	public Integer addIngredientToCake(Long ingredientId, Long cakeId, Float amount, Long metric) {
		
		MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("ingredientId", ingredientId);
        parameters.addValue("cakeId", cakeId);
        parameters.addValue("amount", amount);
        parameters.addValue("metric", metric);
		
		return jdbcTemplate.update(
				"INSERT INTO CakesIngredients(CakeId, IngredientId, Amount, Metric_id) VALUES "
				+ "(:cakeId, :ingredientId, :amount, :metric)", parameters);
	}

	public int checkIfIngredientOnCakeAlready(Long cakeId, Long ingredientId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("ingredientId", ingredientId);
        parameters.addValue("cakeId", cakeId);
		
		return jdbcTemplate.queryForObject("select COUNT(*) from CakesIngredients where CakeId=:cakeId and IngredientId=:ingredientId", parameters, Integer.class);
	}

	public int updateIngredient(Long ingredientId, Long cakeId, Float amount, Long metric) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("ingredientId", ingredientId);
        parameters.addValue("cakeId", cakeId);
        parameters.addValue("amount", amount);
        parameters.addValue("metric", metric);
		
		return jdbcTemplate.update(
				"Update CakesIngredients set Amount = :amount, Metric_id = :metric where CakeId = :cakeId and IngredientId = :ingredientId", parameters);
	}

	public List<Metric> getMetrics() {
		return jdbcTemplate.query("select distinct m.Metric_id,m.Metric_name from Metrics m ", new MetricRowMapper());
	}
	
	public List<Metric> getMetricForIngredient(Long ingredientId){
		MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("ingredientId", ingredientId);
		return jdbcTemplate.query("select m.Metric_id,m.Metric_name from Metrics m "
				+ "left join availableMetricsOnIngredient a on a.MetricId = m.Metric_id "
				+ "where a.IngredientId = :ingredientId", parameters, new MetricRowMapper());
	}

	public int insertMetric(String metric) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("metric", metric);
        
        return jdbcTemplate.queryForObject(
				"INSERT INTO Metrics(Metric_name) VALUES "
				+ "(:metric) returning Metric_id", parameters, Integer.class);
	}

	public boolean checkIfMetricIsUsed(Long metricId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("metricId", metricId);
        
		return jdbcTemplate.queryForObject("SELECT count(*) from cakesingredients where Metric_id = :metricId", parameters, Integer.class)>0;
	}

	public int deleteMetric(Long metricId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("metricId", metricId);
        return jdbcTemplate.update("delete from Metrics m where m.Metric_id = :metricId", parameters);
	}

	public List<CakesIngredient> getIngredientsForOrder(Long orderId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("orderId", orderId);

        return jdbcTemplate.query("select "
        		+ "i.id, "
        		+ "i.name, "
        		+ "i.allergen, "
        		+ "c.amount, "
        		+ "m.Metric_name "
        		+ "from ingredient i "
        		+ "left join customorderingredients c on i.id = c.ingredientid "
        		+ "left join Metrics m on c.Metric_id = m.Metric_id "
        		+ "where c.orderId = :orderId", parameters, new CakesIngredientRowMapper());
	}
}
