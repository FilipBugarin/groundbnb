package SweetMeUp.projekt.requests;

public class SeenBody {
	
	private Long orderId;
	private boolean seen;
	
	public Long getOrderId() {
		return orderId;
	}
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	public boolean isSeen() {
		return seen;
	}
	public void setSeen(boolean seen) {
		this.seen = seen;
	}
	
	

}
