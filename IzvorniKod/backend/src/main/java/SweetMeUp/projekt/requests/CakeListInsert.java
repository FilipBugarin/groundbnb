package SweetMeUp.projekt.requests;

public class CakeListInsert {

	private Integer cakeId;
	private Integer quantity;

	public CakeListInsert() {}

	public Integer getCakeId() {
		return cakeId;
	}

	public void setCakeId(Integer cakeId) {
		this.cakeId = cakeId;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	
}
