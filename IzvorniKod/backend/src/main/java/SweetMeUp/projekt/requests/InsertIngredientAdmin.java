package SweetMeUp.projekt.requests;

public class InsertIngredientAdmin {

	private String name;
	private String alergen;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAlergen() {
		return alergen;
	}
	public void setAlergen(String alergen) {
		this.alergen = alergen;
	}
	
	
	
}
