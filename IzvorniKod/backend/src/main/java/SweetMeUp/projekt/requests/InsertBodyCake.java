package SweetMeUp.projekt.requests;

public class InsertBodyCake {
    private String name;
    private String description;
    private String imageurl;
    private Float price;
    private Long currencyId;

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getImageurl() {
        return imageurl;
    }

    public Float getPrice() {
        return price;
    }
    public Long getCurrencyId(){
    	return currencyId;
    }

}
