package SweetMeUp.projekt.requests;

import java.util.List;

public class InsertBodyOrder {
    private Double price;
    private Long currencyId;
    private List<CakeListInsert> cakeList;

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public List<CakeListInsert> getCakeList() {
        return cakeList;
    }

    public void setCakeList(List<CakeListInsert> cakeList) {
        this.cakeList = cakeList;
    }

	public Long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(Long currencyId) {
		this.currencyId = currencyId;
	}    
}