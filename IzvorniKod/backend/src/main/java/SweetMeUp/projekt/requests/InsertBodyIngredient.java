package SweetMeUp.projekt.requests;

public class InsertBodyIngredient {
    private String name;
    private String allergen;

    public String getName() {
        return name;
    }

    public String getAllergen() {
        return allergen;
    }
}
