package SweetMeUp.projekt.requests;

import java.util.Date;
import java.util.List;

public class CustomizeInsert {

	private Long sellerId;
	private String customImageUrl;
	private String description;
	private List<CustomIngredient> ingredients;
	private Date deliveryAt;
	
	public Long getSellerId() {
		return sellerId;
	}
	public void setSellerId(Long sellerId) {
		this.sellerId = sellerId;
	}
	public String getCustomImageUrl() {
		return customImageUrl;
	}
	public void setCustomImageUrl(String customImageUrl) {
		this.customImageUrl = customImageUrl;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<CustomIngredient> getIngredients() {
		return ingredients;
	}
	public void setIngredients(List<CustomIngredient> ingredients) {
		this.ingredients = ingredients;
	}
	public Date getDeliveryAt() {
		return deliveryAt;
	}
	public void setDeliveryAt(Date deliveryAt) {
		this.deliveryAt = deliveryAt;
	}
	
	
	
	
}
