package SweetMeUp.projekt.requests;

public class DeleteBodyIngredient {
    private String name;
    private String allergen;

    public String getName() {
        return name;
    }

    public String getAllergen() {
        return allergen;
    }

}
