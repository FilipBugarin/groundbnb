package SweetMeUp.projekt.requests;

public class DeleteBodyCake {
    private String name;
    private String description;
    private String imageurl;
    private Integer price;

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getImageurl() {
        return imageurl;
    }

    public Integer getPrice() {
        return price;
    }

}