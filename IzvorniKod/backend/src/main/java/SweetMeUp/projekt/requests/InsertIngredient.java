package SweetMeUp.projekt.requests;

public class InsertIngredient {

	private Long ingredientId;
	private Long cakeId;
	private Float amount;
	private Long metric;
	
	public Long getIngredientId() {
		return ingredientId;
	}
	public void setIngredientId(Long ingredientId) {
		this.ingredientId = ingredientId;
	}
	public Long getCakeId() {
		return cakeId;
	}
	public void setCakeId(Long cakeId) {
		this.cakeId = cakeId;
	}
	public Float getAmount() {
		return amount;
	}
	public void setAmount(Float amount) {
		this.amount = amount;
	}
	public Long getMetric() {
		return metric;
	}
	public void setMetric(Long metric) {
		this.metric = metric;
	}
	
	
	
}
