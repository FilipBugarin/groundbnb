package SweetMeUp.projekt.extractors;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import SweetMeUp.projekt.entity.auth.Role;

public class RoleSetExtractor implements ResultSetExtractor<Role>{

	@Override
	public Role extractData(ResultSet rs) throws SQLException, DataAccessException {

		Role role = new Role();
		
		role.setId(rs.getLong("id"));
		role.setName(rs.getString("name"));
		
		return role;
	}

}
