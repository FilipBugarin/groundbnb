package SweetMeUp.projekt.extractors;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import SweetMeUp.projekt.entity.Metric;

public class MetricResultSetExtractor implements ResultSetExtractor<Metric>{

	@Override
	public Metric extractData(ResultSet rs) throws SQLException, DataAccessException {
		Metric metric = new Metric();
			
		metric.setMetricId(rs.getLong("metric_id"));
		metric.setMetricName(rs.getString("Metric_name"));
				
		return metric;
	}

}
