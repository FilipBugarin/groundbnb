package SweetMeUp.projekt.extractors;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

public class IntegerSetExtractor implements ResultSetExtractor<Integer> {

    @Override
    public Integer extractData(ResultSet rs) throws SQLException, DataAccessException {

        return rs.getInt("id");
    }
}
