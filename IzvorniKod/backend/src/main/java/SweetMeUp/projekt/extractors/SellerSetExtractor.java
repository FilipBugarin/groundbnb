package SweetMeUp.projekt.extractors;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import SweetMeUp.projekt.entity.Seller;

public class SellerSetExtractor implements ResultSetExtractor<Seller>{

	@Override
	public Seller extractData(ResultSet rs) throws SQLException, DataAccessException {
		
		Seller seller = new Seller();
		
		seller.setId(rs.getLong("id"));
		seller.setName(rs.getString("name"));
		seller.setProfileImageUrl(rs.getString("profileImage"));
		
		return seller;
	}

}
