package SweetMeUp.projekt.extractors;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import SweetMeUp.projekt.entity.Currency;
import SweetMeUp.projekt.entity.CustomCakeNotification;

public class CustomCakeNotificationSetExtractor implements ResultSetExtractor<CustomCakeNotification>{

	@Override
	public CustomCakeNotification extractData(ResultSet rs) throws SQLException, DataAccessException {
		
		CustomCakeNotification notification = new CustomCakeNotification();
		
		notification.setOrderId(rs.getLong("id"));
		notification.setBuyerId(rs.getLong("userId"));
		notification.setBuyerName(rs.getString("name"));
		notification.setPrice(rs.getDouble("price"));
		notification.setCurrency(new Currency(rs.getLong("currencyId")));
		notification.setImageUrl(rs.getString("imageUrl"));
		notification.setDescription(rs.getString("description"));
		notification.setStatus(rs.getInt("status"));
		notification.setIngredients(null);
		notification.setDeliveryAt(rs.getDate("deliveryAt"));
		notification.setRejectReason(rs.getString("rejectReason"));
		notification.setCreatedAt(rs.getTimestamp("created_at").toInstant());
		notification.setUpdatedAt(rs.getTimestamp("updated_at").toInstant());
		notification.setSeen(rs.getBoolean("seen"));
		
		return notification;
	}

}
