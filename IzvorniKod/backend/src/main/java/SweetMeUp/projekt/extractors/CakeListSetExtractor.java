package SweetMeUp.projekt.extractors;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import SweetMeUp.projekt.entity.Cake;
import SweetMeUp.projekt.entity.CakeList;
import SweetMeUp.projekt.entity.Currency;

public class CakeListSetExtractor implements ResultSetExtractor<CakeList> {

	@Override
	public CakeList extractData(ResultSet rs) throws SQLException, DataAccessException {
		
		CakeList cakeList = new CakeList();
		Cake cake = new Cake();
		
		cake.setId(rs.getInt("id"));
		cake.setName(rs.getString("name"));
		cake.setDescription(rs.getString("description"));
		cake.setImageurl(rs.getString("imageurl"));
		cake.setPrice(rs.getFloat("price"));
		cake.setCurrency(new Currency(rs.getLong("currency")));
		
		cakeList.setCake(cake);
		cakeList.setQuantity(rs.getInt("quantity"));
		
		return cakeList;
	}

}
