package SweetMeUp.projekt.extractors;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import SweetMeUp.projekt.entity.Currency;

public class CurrencySetExtractor implements ResultSetExtractor<Currency>{

	@Override
	public Currency extractData(ResultSet rs) throws SQLException, DataAccessException {
		
		Currency currency = new Currency();
		
		currency.setCurrencyId(rs.getLong("currencyId"));
		currency.setCurrencyName(rs.getString("currencyName"));
		currency.setRatioToEuro(rs.getDouble("ratioToEuro"));
		currency.setSymbol(rs.getString("currencySymbol"));
		
		return currency;
	}
	
}
