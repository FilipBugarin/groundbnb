package SweetMeUp.projekt.extractors;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import SweetMeUp.projekt.entity.Ingredient;

public class IngredientSetExtractor implements ResultSetExtractor<Ingredient>{

	@Override
	public Ingredient extractData(ResultSet rs) throws SQLException, DataAccessException {
		
		Ingredient ingredient = new Ingredient();
		
		ingredient.setId(rs.getLong("id"));
		ingredient.setName(rs.getString("name"));
		ingredient.setAllergen(rs.getString("allergen"));
		
		return ingredient;
	}

}
