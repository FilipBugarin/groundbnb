package SweetMeUp.projekt.extractors;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import SweetMeUp.projekt.entity.Currency;
import SweetMeUp.projekt.entity.Order;

public class OrderSetExtractor implements ResultSetExtractor<Order>{

    @Override
    public Order extractData(ResultSet rs) throws SQLException, DataAccessException {

        Order order = new Order();

        order.setOrderId(rs.getLong("id"));
        order.setBuyerId(rs.getLong("buyerId"));
        order.setCakeList(null);
        order.setPrice(rs.getDouble("price"));
        order.setCurrencyId(new Currency(rs.getLong("currencyId")));
        order.setCreated_at(rs.getString("created_at"));
        order.setUpdated_at(rs.getString("updated_at"));

        return order;
    }
}
