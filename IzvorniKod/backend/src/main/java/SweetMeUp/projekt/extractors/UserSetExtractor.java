package SweetMeUp.projekt.extractors;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import SweetMeUp.projekt.entity.auth.Role;
import SweetMeUp.projekt.entity.auth.User;

public class UserSetExtractor implements ResultSetExtractor<User>{
	
	@Override
	public User extractData(ResultSet rs) throws SQLException, DataAccessException {
				
		User user = new User();
		
		user.setId(rs.getLong("id"));
		user.setName(rs.getString("name"));
		user.setUsername(rs.getString("username"));
		user.setEmail(rs.getString("email"));
		user.setPassword(rs.getString("password"));

		long roleId = rs.getLong("role_id");
		Role userRole = new Role();
		userRole.setId(roleId);
		
		user.setRole(userRole);
		user.setProfileImageUrl(rs.getString("profileImage"));
		
		return user;
	}

}