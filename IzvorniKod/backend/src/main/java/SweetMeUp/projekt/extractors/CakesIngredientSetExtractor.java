package SweetMeUp.projekt.extractors;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import SweetMeUp.projekt.entity.CakesIngredient;

public class CakesIngredientSetExtractor implements ResultSetExtractor<CakesIngredient>{

	@Override
	public CakesIngredient extractData(ResultSet rs) throws SQLException, DataAccessException {
		
		CakesIngredient ingredient = new CakesIngredient();
		
		ingredient.setId(rs.getInt("id"));
		ingredient.setName(rs.getString("name"));
		ingredient.setAllergen(rs.getString("allergen"));
		ingredient.setAmount(rs.getLong("amount"));
		ingredient.setMetric(rs.getString("Metric_name"));
		
		return ingredient;
	}

}
