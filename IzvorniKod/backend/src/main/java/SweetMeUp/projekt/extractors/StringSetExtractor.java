package SweetMeUp.projekt.extractors;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

public class StringSetExtractor implements ResultSetExtractor<String>{

	@Override
	public String extractData(ResultSet rs) throws SQLException, DataAccessException {
		return rs.getString("Metric_name");
	}

}
