package SweetMeUp.projekt.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import SweetMeUp.projekt.extractors.IntegerSetExtractor;

public class IntegerRowMapper implements  RowMapper <Integer> {

    @Override
    public Integer mapRow(ResultSet rs, int rowNum) throws  SQLException {
        IntegerSetExtractor extractor = new IntegerSetExtractor();
        return extractor.extractData(rs);
    }
}
