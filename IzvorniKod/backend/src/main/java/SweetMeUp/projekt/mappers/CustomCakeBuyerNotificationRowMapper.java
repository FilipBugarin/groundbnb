package SweetMeUp.projekt.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import SweetMeUp.projekt.entity.CustomCakeNotification;
import SweetMeUp.projekt.extractors.CustomCakeBuyerNotificationSetExtractor;

public class CustomCakeBuyerNotificationRowMapper implements RowMapper<CustomCakeNotification>{

	@Override
	public CustomCakeNotification mapRow(ResultSet rs, int rowNum) throws SQLException {
		CustomCakeBuyerNotificationSetExtractor extractor = new CustomCakeBuyerNotificationSetExtractor();
		return extractor.extractData(rs);
	}

}
