package SweetMeUp.projekt.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import SweetMeUp.projekt.entity.Currency;
import SweetMeUp.projekt.extractors.CurrencySetExtractor;

public class CurrencyRowMapper implements RowMapper<Currency>{

	@Override
	public Currency mapRow(ResultSet rs, int rowNum) throws SQLException {
		CurrencySetExtractor extractor = new CurrencySetExtractor();
		return extractor.extractData(rs);
	}

}
