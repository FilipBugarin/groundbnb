package SweetMeUp.projekt.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import SweetMeUp.projekt.entity.Cake;
import SweetMeUp.projekt.extractors.CakeSetExtractor;

public class CakeRowMapper implements RowMapper<Cake> {

	@Override
	public Cake mapRow(ResultSet rs, int rowNum) throws SQLException {
		CakeSetExtractor extractor = new CakeSetExtractor();
		return extractor.extractData(rs);
	}

}
