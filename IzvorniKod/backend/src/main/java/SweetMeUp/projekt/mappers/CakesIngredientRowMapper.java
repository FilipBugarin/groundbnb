package SweetMeUp.projekt.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import SweetMeUp.projekt.entity.CakesIngredient;
import SweetMeUp.projekt.extractors.CakesIngredientSetExtractor;

public class CakesIngredientRowMapper implements RowMapper<CakesIngredient>{

	@Override
	public CakesIngredient mapRow(ResultSet rs, int rowNum) throws SQLException {
		CakesIngredientSetExtractor extractor = new CakesIngredientSetExtractor();
		return extractor.extractData(rs);
	}

}
