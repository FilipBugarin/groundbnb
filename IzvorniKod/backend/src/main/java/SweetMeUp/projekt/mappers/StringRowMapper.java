package SweetMeUp.projekt.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import SweetMeUp.projekt.extractors.StringSetExtractor;

public class StringRowMapper implements  RowMapper <String>{

	@Override
	public String mapRow(ResultSet rs, int rowNum) throws SQLException {
		StringSetExtractor extractor = new StringSetExtractor();
		return extractor.extractData(rs);
	}

}
