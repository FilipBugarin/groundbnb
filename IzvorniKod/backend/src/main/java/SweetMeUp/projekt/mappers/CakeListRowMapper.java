package SweetMeUp.projekt.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import SweetMeUp.projekt.entity.CakeList;
import SweetMeUp.projekt.extractors.CakeListSetExtractor;

public class CakeListRowMapper implements RowMapper<CakeList> {

	@Override
	public CakeList mapRow(ResultSet rs, int rowNum) throws SQLException {
		CakeListSetExtractor extractor = new CakeListSetExtractor();
		return extractor.extractData(rs);
	}

}
