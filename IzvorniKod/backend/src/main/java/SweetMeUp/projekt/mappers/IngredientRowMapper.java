package SweetMeUp.projekt.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import SweetMeUp.projekt.entity.Ingredient;
import SweetMeUp.projekt.extractors.IngredientSetExtractor;

public class IngredientRowMapper implements RowMapper<Ingredient>{

	@Override
	public Ingredient mapRow(ResultSet rs, int rowNum) throws SQLException {
		IngredientSetExtractor extractor = new IngredientSetExtractor();
		return extractor.extractData(rs);
	}

}
