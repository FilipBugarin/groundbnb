package SweetMeUp.projekt.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import SweetMeUp.projekt.entity.Order;
import SweetMeUp.projekt.entity.SellersOrderSetExtractor;

public class SellersOrderRowMapper implements RowMapper<Order>{

	@Override
	public Order mapRow(ResultSet rs, int rowNum) throws SQLException {
		SellersOrderSetExtractor extractor = new SellersOrderSetExtractor();
		return extractor.extractData(rs);
	}

}
