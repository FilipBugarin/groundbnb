package SweetMeUp.projekt.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import SweetMeUp.projekt.entity.Seller;
import SweetMeUp.projekt.extractors.SellerSetExtractor;

public class SellerMapper implements RowMapper<Seller> {

	@Override
	public Seller mapRow(ResultSet rs, int rowNum) throws SQLException {
		SellerSetExtractor extractor = new SellerSetExtractor();
		return extractor.extractData(rs);
	}

}
