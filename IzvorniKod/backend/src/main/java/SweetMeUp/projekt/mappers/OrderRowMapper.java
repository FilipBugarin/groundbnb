package SweetMeUp.projekt.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import SweetMeUp.projekt.entity.Order;
import SweetMeUp.projekt.extractors.OrderSetExtractor;

public class OrderRowMapper implements  RowMapper <Order> {

    @Override
    public Order mapRow(ResultSet rs, int rowNum) throws  SQLException {
        OrderSetExtractor extractor = new OrderSetExtractor();
        return extractor.extractData(rs);
    }
}
