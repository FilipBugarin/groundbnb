package SweetMeUp.projekt.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import SweetMeUp.projekt.entity.auth.User;
import SweetMeUp.projekt.extractors.UserSetExtractor;

public class UserMapper implements RowMapper<User>{

	@Override
	public User mapRow(ResultSet rs, int rowNum) throws SQLException {
		UserSetExtractor extractor = new UserSetExtractor();
		return extractor.extractData(rs);
	}

}

