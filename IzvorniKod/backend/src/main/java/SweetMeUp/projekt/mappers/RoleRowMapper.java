package SweetMeUp.projekt.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import SweetMeUp.projekt.entity.auth.Role;
import SweetMeUp.projekt.extractors.RoleSetExtractor;

public class RoleRowMapper implements RowMapper<Role> {

	@Override
	public Role mapRow(ResultSet rs, int rowNum) throws SQLException {
		RoleSetExtractor extractor = new RoleSetExtractor();
		return extractor.extractData(rs);
	}

}
