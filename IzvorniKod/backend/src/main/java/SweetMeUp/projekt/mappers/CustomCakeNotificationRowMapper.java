package SweetMeUp.projekt.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import SweetMeUp.projekt.entity.CustomCakeNotification;
import SweetMeUp.projekt.extractors.CustomCakeNotificationSetExtractor;

public class CustomCakeNotificationRowMapper implements RowMapper<CustomCakeNotification>{

	@Override
	public CustomCakeNotification mapRow(ResultSet rs, int rowNum) throws SQLException {
		CustomCakeNotificationSetExtractor extractor = new CustomCakeNotificationSetExtractor();
		return extractor.extractData(rs);
	}

}
