package SweetMeUp.projekt.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import SweetMeUp.projekt.entity.Metric;
import SweetMeUp.projekt.extractors.MetricResultSetExtractor;

public class MetricRowMapper implements  RowMapper <Metric>{

	@Override
	public Metric mapRow(ResultSet rs, int rowNum) throws SQLException {
		MetricResultSetExtractor extractor = new MetricResultSetExtractor();
		return extractor.extractData(rs);
	}

}
