import React from 'react'

import './Spinner.css'

const Spinner = () => {
  return (
    <div className="spinner-outer-container">
      <div className="spinner">
        <div className="spinner-item"></div>
        <div className="spinner-item"></div>
        <div className="spinner-item"></div>
      </div>
    </div>
  )
}

export default Spinner
