import React from 'react'
import { Link, useNavigate } from 'react-router-dom'
import Dropdown from 'react-bootstrap/Dropdown'

import './Header.css'
import SweetMeUpLogo from './../../assets/images/logo.png'
import { ReactComponent as Cart } from './../../assets/icons/go-to-cart.svg'

import { UserRole } from '../../models/User.model'
import { useAuth } from '../../auth/authContext'
import { Modal } from 'react-bootstrap'

const Header = () => {
  const auth = useAuth()
  const navigate = useNavigate()
  const [openSidebar, setOpenSidebar] = React.useState<boolean>(false)

  // Custom dropdown toggle button
  type CustomToggleProps = {
    children?: React.ReactNode
    onClick?: (event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {}
  }

  const CustomToggle = React.forwardRef(
    (props: CustomToggleProps, ref: React.Ref<HTMLAnchorElement>) => (
      // eslint-disable-next-line
      <a
        href=""
        className="custom-dropdown-toggle-btn"
        ref={ref}
        onClick={e => {
          e.preventDefault()
          if (props.onClick) props.onClick(e)
        }}>
        {props.children}
        <i className="fas fa-angle-down ms-2"></i>
      </a>
    )
  )

  // Custom dropdown menu
  type CustomMenuProps = {
    children?: React.ReactNode
    style?: React.CSSProperties
    className?: string
    labeledBy?: string
  }
  const CustomMenu = React.forwardRef((props: CustomMenuProps, ref: React.Ref<HTMLDivElement>) => (
    <div
      ref={ref}
      style={props.style}
      className={props.className}
      aria-labelledby={props.labeledBy}>
      <ul className="list-unstyled">{props.children}</ul>
    </div>
  ))

  return (
    <div className="header-container">
      <div className="header-left-side">
        <Link className="header-logo-container" to="/">
          <img className="header-sweetMeUp" src={SweetMeUpLogo} alt="SweetMeUp logo"></img>
        </Link>
      </div>
      <div className="header-right-side">
        <div className="header-links-container">
          <Link className="header-word-container" to="/">
            Home
          </Link>
          <Link className="header-word-container" to="webshop">
            Explore
          </Link>
          {auth.user?.accessToken ? (
            auth.user.role === UserRole.ROLE_BUYER ? (
              <Link className="header-word-container" to="customize">
                Customize
              </Link>
            ) : (
              <></>
            )
          ) : (
            <Link className="header-word-container" to="customize">
              Customize
            </Link>
          )}
          {auth.user?.accessToken ? (
            <Dropdown className="header-word-container" align="end">
              <Dropdown.Toggle as={CustomToggle} id="dropdown-basic">
                <i className="fas fa-user"></i> {auth.user.name}
              </Dropdown.Toggle>

              <Dropdown.Menu className="custom-dropdown-item-container" as={CustomMenu}>
                <Dropdown.Item
                  to={`/profile-${auth.user.role === UserRole.ROLE_BUYER ? 'buyer' : 'seller'}`}
                  as={Link}
                  className="custom-dropdown-item">
                  <i className="fas fa-user-circle me-2"></i>
                  My Profile
                </Dropdown.Item>
                <Dropdown.Item to="/notifications" as={Link} className="custom-dropdown-item">
                  <i className="fas fa-envelope me-2"></i>
                  Notifications
                </Dropdown.Item>
                <Dropdown.Item
                  className="custom-dropdown-item"
                  as={Link}
                  to="/"
                  onClick={() => {
                    auth.logout()
                  }}>
                  <i className="fas fa-sign-out-alt me-2"></i>
                  Logout
                </Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          ) : (
            <Link className="header-word-container" to="login">
              Login
            </Link>
          )}
          <Link className="header-image-container" to="checkout">
            <Cart className="header-cart" />
          </Link>
        </div>
        {openSidebar && (
          <Modal show={openSidebar} fullscreen={true}>
            <Modal.Header
              style={{ backgroundColor: '#ebdbca', borderBottom: '1px solid var(--light-black)' }}>
              <Modal.Title
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  width: '100%',
                }}>
                <span
                  style={{
                    fontFamily: '"Quicksand-bold", sans-serif',
                    color: 'var(--dark-brown)',
                    fontSize: '1.5rem',
                  }}>
                  SweetMeUp
                </span>
                <i className="fas fa-times fa-lg" onClick={() => setOpenSidebar(false)}></i>
              </Modal.Title>
            </Modal.Header>
            <Modal.Body style={{ backgroundColor: 'var(--light-brown)' }}>
              <div className="mobile-header-links-container">
                <div className="mobile-header-links-inner-container">
                  <Link
                    onClick={() => setOpenSidebar(false)}
                    className="header-word-container"
                    to="/">
                    Home
                  </Link>
                  <Link
                    onClick={() => setOpenSidebar(false)}
                    className="header-word-container"
                    to="webshop">
                    Explore
                  </Link>
                  <Link
                    onClick={() => setOpenSidebar(false)}
                    className="header-word-container"
                    to="customize">
                    Customize
                  </Link>
                  {auth.user?.accessToken ? (
                    <>
                      <Link
                        onClick={() => setOpenSidebar(false)}
                        className="header-word-container"
                        to={`/profile-${
                          auth.user.role === UserRole.ROLE_BUYER ? 'buyer' : 'seller'
                        }`}>
                        Profile
                      </Link>
                      <Link
                        onClick={() => setOpenSidebar(false)}
                        className="header-word-container"
                        to="/notifications">
                        Notifications
                      </Link>
                      <div
                        className="header-word-container"
                        onClick={() => {
                          auth.logout()
                          navigate('/')
                        }}>
                        Logout
                      </div>
                    </>
                  ) : (
                    <Link
                      onClick={() => setOpenSidebar(false)}
                      className="header-word-container"
                      to="login">
                      Login
                    </Link>
                  )}
                  <Link
                    onClick={() => setOpenSidebar(false)}
                    className="header-image-container header-word-container"
                    style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}
                    to="checkout">
                    <div>Cart</div>
                  </Link>
                </div>
              </div>
            </Modal.Body>
          </Modal>
        )}
        <div className="header-menu-button-container">
          <i className="fas fa-bars fa-lg" onClick={() => setOpenSidebar(true)}></i>
        </div>
      </div>
    </div>
  )
}

export default Header
