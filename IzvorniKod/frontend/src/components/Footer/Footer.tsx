import React from 'react'
import './Footer.css'
import { ReactComponent as FerLogo } from './../../assets/icons/fer-logo.svg'

const Footer = () => {
  return (
    <div className="footer-container">
      <div className="footer-column-left">
        <span className="footer-contact-us-title">Contact us</span>
        <li className="footer-link">(01) 456 -789</li>
        <li className="footer-link">hello.sweetmeup@gmail.com</li>
        <li className="footer-link">XY St blablabla 11111, idk</li>
      </div>

      <div className="footer-column-middle">
        <FerLogo className="footer-LogoFer" />
      </div>

      <div className="footer-column-right">
        <div className="footer-copyright-text">&copy; 2021 SweetMeUp Inc.</div>
      </div>
    </div>
  )
}

export default Footer
