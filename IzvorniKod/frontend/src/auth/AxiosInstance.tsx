import axios from 'axios'

export const AxiosInstance = axios.create({ baseURL: 'https://sweetmeup.herokuapp.com' })

AxiosInstance.interceptors.request.use(async request => {
  const token = sessionStorage.getItem('token')

  if (token && request.headers) {
    request.headers['Authorization'] = 'Bearer ' + token
  }

  // TODO else if token is not present

  return request
})
