import { AxiosResponse } from 'axios'
import { createContext, useContext, useState, ReactNode } from 'react'
import { User } from '../models/User.model'
import { AxiosInstance } from './AxiosInstance'

type AuthContextData = {
  user?: User
  loading: boolean
  signIn: (email: string, password: string) => Promise<AxiosResponse>
  signUp: (
    email: string,
    name: string,
    password: string,
    username: string,
    roleId: number
  ) => Promise<AxiosResponse>
  refreshUser: () => Promise<AxiosResponse | void>
  logout: () => void
}

const AuthContext = createContext<AuthContextData>({} as AuthContextData)

export const AuthProvider = ({ children }: { children: ReactNode }): JSX.Element => {
  const [user, setUser] = useState<User>()
  const [loading, setLoading] = useState<boolean>(false)

  const signIn = (username: string, password: string) => {
    return new Promise<AxiosResponse>((resolve, reject) => {
      setLoading(true)

      AxiosInstance.post('/api/auth/signin', {
        usernameOrEmail: username,
        password,
      })
        .then(res => {
          const { data: user } = res
          setUser(user)
          sessionStorage.setItem('token', user.accessToken)
          resolve(user)
        })
        .catch(err => {
          reject(err)
        })
        .finally(() => setLoading(false))
    })
  }

  const signUp = (
    email: string,
    name: string,
    password: string,
    username: string,
    roleId: number
  ) => {
    return new Promise<AxiosResponse>((resolve, reject) => {
      setLoading(true)

      AxiosInstance.post('/api/auth/signup', {
        name,
        username,
        email,
        password,
        roleId,
      })
        .then(res => {
          resolve(res.data)
        })
        .catch(err => reject(err))
        .finally(() => setLoading(false))
    })
  }

  const refreshUser = () => {
    return new Promise<AxiosResponse | void>((resolve, reject) => {
      const token = sessionStorage.getItem('token')

      if (token) {
        setLoading(true)

        AxiosInstance.get('/api/auth/refresh')
          .then(res => {
            const { data: refreshedUser } = res
            setUser({ ...refreshedUser, accessToken: token })
            resolve(refreshedUser)
          })
          .catch(err => {
            reject(err)
          })
          .finally(() => setLoading(false))
      } else {
        resolve()
      }
    })
  }

  const logout = () => {
    sessionStorage.removeItem('token')
    setUser(undefined)
  }

  return (
    <AuthContext.Provider value={{ user, loading, signUp, signIn, refreshUser, logout }}>
      {children}
    </AuthContext.Provider>
  )
}

export const useAuth = (): AuthContextData => {
  const context = useContext(AuthContext)

  if (!context) {
    throw new Error('useAuth must be used within an AuthProvider')
  }

  return context
}
