import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { RootState } from './store'
import { CakeCart } from '../models/Cake.model'

const initialState: CakeCart[] = []

export const cakeSlice = createSlice({
  name: 'cakes',
  initialState,
  reducers: {
    addToCart: (state, action: PayloadAction<CakeCart>) => {
      if (state.filter(CakeCart => CakeCart.cake.id === action.payload.cake.id).length) {
        state = state.map(cake =>
          cake.cake.id === action.payload.cake.id ? { ...cake, quantity: cake.quantity + 1 } : cake
        )
      } else {
        state.push(action.payload)
      }
      return state.sort((a, b) => (a.cake.id > b.cake.id ? 1 : a.cake.id < b.cake.id ? -1 : 0))
    },
    removeFromCart: (state, action: PayloadAction<CakeCart>) => {
      return state
        .filter(CakeCart => CakeCart.cake.id !== action.payload.cake.id)
        .sort((a, b) => (a.cake.id > b.cake.id ? 1 : a.cake.id < b.cake.id ? -1 : 0))
    },
    removeAll: state => {
      return []
    },
  },
})

export const { addToCart, removeFromCart, removeAll } = cakeSlice.actions
export const selectCakes = (state: RootState) => state.cakes.values
export default cakeSlice.reducer
