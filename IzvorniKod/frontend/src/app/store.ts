import { configureStore } from '@reduxjs/toolkit'
import CakesReducer from './cakeSlice'

export const store = configureStore({
  reducer: {
    cakes: CakesReducer,
  },
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
