import * as React from 'react'
import { useNavigate, useParams } from 'react-router-dom'

import './Cake.css'
import { ReactComponent as AddToCartIcon } from '../../assets/icons/add-to-cart.svg'

import { AxiosInstance } from '../../auth/AxiosInstance'
import { CakeModel } from '../../models/Cake.model'
import { useAppDispatch } from '../../app/hooks'
import { addToCart } from '../../app/cakeSlice'
import { PreviousOrdersModel } from '../../models/PreviousOrders.model'
import CakeList from '../Webshop/components/CakeList/CakeList'
import { useAuth } from '../../auth/authContext'
import { IngredientCakeModel } from '../../models/Ingredient.model'

const Cake = () => {
  const auth = useAuth()
  const navigate = useNavigate()
  const { slug } = useParams()
  const [allCakes, setAllCakes] = React.useState<CakeModel[]>([])
  const [youMayLike, setYouMayLike] = React.useState<CakeModel[]>([])
  const [prevOrdered, setPrevOrdered] = React.useState<CakeModel[]>([])
  const [showCake, setShowCake] = React.useState<CakeModel>()

  const dispatch = useAppDispatch()

  React.useEffect(() => {
    AxiosInstance.get('/cakes')
      .then(res => {
        setAllCakes(res.data)
      })
      .catch(err => console.error(err))

    AxiosInstance.get<PreviousOrdersModel[]>('/profile/buyer')
      .then(res => {
        setPrevOrdered(
          res.data
            .filter(prev => prev.buyerId === auth.user?.id)
            .map(prev =>
              prev.cakeList.map(cakeCart => {
                const { cake } = cakeCart
                return cake
              })
            )
            .reduce((endArray, nowCakeArray) => {
              for (let cake of nowCakeArray) {
                if (endArray.filter(endCake => endCake.id === cake.id).length === 0) {
                  endArray.push(cake)
                }
              }
              return endArray
            }, [])
        )
      })
      .catch(err => console.error(err))
  }, [auth])

  // Get Show cake
  React.useEffect(() => {
    if (slug) {
      setShowCake(allCakes.filter(cake => cake.id === parseInt(slug))[0])
    } else {
      navigate('/webshop')
    }
  }, [slug, allCakes, navigate])

  //   React.useEffect(() => {
  //     const prevOrderedCakes1 = prevOrdered.map(item => item.cakeList)
  //     const prevOrderedCakes2 = prevOrderedCakes1.map(item => item.cake);
  //     var arrSliced = []
  //     var displayNumber
  //     if (prevOrderedCakes.length > 4){
  //        displayNumber = 4
  //     } else {
  //        displayNumber = prevOrderedCakes.length
  //     }

  //     arrSliced = prevOrderedCakes.slice(0, displayNumber)
  //     setDisplayPrevOrder(prevOrderedCakes)
  //   }, [prevOrdered])

  // Configure YouMayLike cakes
  React.useEffect(() => {
    var showCakePairs = []
    var showCakeArray = []
    var map = new Map()

    if (!showCake) {
      return
    }

    for (let cake of allCakes) {
      let result = showCake.ingredients.filter(ingredients =>
        cake.ingredients.some(item => item.name === ingredients.name)
      ) //koliko sličnih sastojaka imaju

      map.set(cake, result.length)
    }
    map.delete(showCake)
    var array = Array.from(map)
    array.sort(function (a, b) {
      return a[1] - b[1]
    })
    var displayNumber
    if (array.length > 4) {
      displayNumber = 4
    } else {
      displayNumber = array.length
    }
    showCakePairs = array.slice(array.length - displayNumber)
    showCakePairs.reverse()
    for (let pair of showCakePairs) {
      showCakeArray.push(pair[0])
    }

    setYouMayLike(showCakeArray)
  }, [showCake, allCakes])

  return (
    <div className="cake-page-container">
      <div className="cake-container-outer">
        <div className="cake-container">
          <div className="cake-page-image-container">
            <img className="cake-page-image" src={showCake?.imageurl} alt="Cake" />
          </div>
          <div className="cake-page-info-container">
            <div className="cake-page-title">{showCake?.name}</div>
            <div className="cake-page-desc">{showCake?.description}</div>
            <div className="cake-page-ingredients">
              Ingredients:&nbsp;
              {showCake?.ingredients.map((item, index) => (index ? ', ' : '') + item.name)}
            </div>
            <div className="cake-page-allergens">
              Allergens:&nbsp;
              {showCake?.ingredients
                .filter(item => item.allergen)
                .reduce<IngredientCakeModel[]>((arr, item) => {
                  return arr.find(i => i.allergen === item.allergen) ? arr : [...arr, item]
                }, [])
                .map((item, index) => (index ? ', ' : '') + item.allergen)}
            </div>
            {showCake ? (
              <div
                className="checkout"
                onClick={() => dispatch(addToCart({ cake: showCake, quantity: 1 }))}>
                <div className="cake-page-price">Cijena: {showCake?.price} kn</div>
                <div className="webshop-cake-cart-btn">
                  <AddToCartIcon className="webshop-cake-cart-icon" />
                </div>
              </div>
            ) : null}
          </div>
        </div>
      </div>
      <div className="suggestion-container">
        <div className="you-may-like-container first-suggestion-container">
          <div className="suggestion-outer-container">
            <div className="title-cake-page">
              <h2>You may also like...</h2>
            </div>
            <div className="suggested-cakes-container">
              {youMayLike.length !== 0 && <CakeList shownCakes={youMayLike} />}
            </div>
          </div>
        </div>
        {prevOrdered.length ? (
          <div className="you-may-like-container">
            <div className="title-cake-page">
              <h2>You previously ordered...</h2>
            </div>
            <div className="suggested-cakes-container">
              <CakeList shownCakes={prevOrdered} />
            </div>
          </div>
        ) : (
          <></>
        )}
      </div>
    </div>
  )
}

export default Cake
