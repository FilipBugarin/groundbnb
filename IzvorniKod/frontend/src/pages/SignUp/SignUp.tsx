import * as React from 'react'
import TextField from '@mui/material/TextField'
import LoadingButton from '@mui/lab/LoadingButton'
import Select from '@mui/material/Select'
import MenuItem from '@mui/material/MenuItem'
import * as Yup from 'yup'
import { useFormik } from 'formik'
import { createTheme } from '@mui/material/styles'
import { ThemeProvider } from '@mui/material/styles'
import { Link, useNavigate } from 'react-router-dom'

import './SignUp.css'

import { useAuth } from '../../auth/authContext'

const LoginSchema = Yup.object().shape({
  email: Yup.string().required('Required').email('Not valid email'),
  username: Yup.string().required('Required'),
  name: Yup.string().required('Required'),
  role: Yup.number().moreThan(0, 'Required'),
  password: Yup.string().required('Required'),
})

const theme = createTheme({
  palette: {
    primary: {
      main: '#e63746',
    },
  },
})

const SignUp = () => {
  const auth = useAuth()
  const navigate = useNavigate()
  const formik = useFormik({
    initialValues: {
      email: '',
      username: '',
      name: '',
      password: '',
      role: 0,
    },
    validationSchema: LoginSchema,
    onSubmit: values => {
      auth
        .signUp(values.email, values.name, values.password, values.username, values.role)
        .then(res => {
          navigate('/login')
        })
        .catch(err => {
          console.error(err)
        })
    },
  })

  return (
    <div className="login-main-container">
      <div className="login-container">
        <div className="login-title">Welcome!</div>
        <form className="login-form" onSubmit={formik.handleSubmit}>
          <div className="signup-double-row">
            <TextField
              id="name"
              name="name"
              label="Name"
              type="name"
              variant="outlined"
              sx={{ marginRight: '1rem' }}
              value={formik.values.name}
              onChange={formik.handleChange}
              error={formik.touched.name && Boolean(formik.errors.name)}
              helperText={formik.touched.name && formik.errors.name}
            />
            <TextField
              id="username"
              name="username"
              label="Username"
              type="username"
              variant="outlined"
              value={formik.values.username}
              onChange={formik.handleChange}
              error={formik.touched.username && Boolean(formik.errors.username)}
              helperText={formik.touched.username && formik.errors.username}
            />
          </div>
          <Select
            id="role"
            name="role"
            label="Role"
            variant="filled"
            sx={{ marginBottom: '1rem' }}
            fullWidth={true}
            value={formik.values.role}
            onChange={formik.handleChange}
            error={formik.touched.role && Boolean(formik.errors.role)}>
            <MenuItem value={0} disabled>
              Select a role
            </MenuItem>
            <MenuItem value={1}>Buyer</MenuItem>
            <MenuItem value={2}>Seller</MenuItem>
          </Select>
          <TextField
            id="email"
            name="email"
            label="Email"
            variant="outlined"
            sx={{ marginBottom: '1rem' }}
            fullWidth={true}
            value={formik.values.email}
            onChange={formik.handleChange}
            error={formik.touched.email && Boolean(formik.errors.email)}
            helperText={formik.touched.email && formik.errors.email}
          />
          <TextField
            id="password"
            name="password"
            label="Password"
            type="password"
            variant="outlined"
            sx={{ marginBottom: '1rem' }}
            fullWidth={true}
            value={formik.values.password}
            onChange={formik.handleChange}
            error={formik.touched.password && Boolean(formik.errors.password)}
            helperText={formik.touched.password && formik.errors.password}
          />
          <ThemeProvider theme={theme}>
            <LoadingButton
              fullWidth={true}
              color="primary"
              variant="contained"
              loading={auth.loading}
              type="submit">
              Login
            </LoadingButton>
          </ThemeProvider>
          <div className="go-to-signup">
            Already have an account?{' '}
            <Link className="go-to-link" to="/login">
              Login
            </Link>
          </div>
        </form>
      </div>
    </div>
  )
}

export default SignUp
