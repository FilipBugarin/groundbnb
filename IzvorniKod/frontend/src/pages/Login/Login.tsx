import * as React from 'react'
import TextField from '@mui/material/TextField'
import LoadingButton from '@mui/lab/LoadingButton'
import * as Yup from 'yup'
import { useFormik } from 'formik'
import { createTheme } from '@mui/material/styles'
import { ThemeProvider } from '@mui/material/styles'
import { Link, useNavigate, useSearchParams } from 'react-router-dom'

import './Login.css'

import { useAuth } from '../../auth/authContext'

const LoginSchema = Yup.object().shape({
  usernameOrEmail: Yup.string().required('Required'),
  password: Yup.string().required('Required'),
})

const theme = createTheme({
  palette: {
    primary: {
      main: '#e63746',
    },
  },
})

const Login = () => {
  const [searchParams] = useSearchParams()
  const auth = useAuth()
  const navigate = useNavigate()

  const formik = useFormik({
    initialValues: {
      usernameOrEmail: '',
      password: '',
    },
    validationSchema: LoginSchema,
    onSubmit: (values, { setErrors }) => {
      auth
        .signIn(values.usernameOrEmail, values.password)
        .then(res => {
          const redirectUrl = searchParams.get('redirectUrl')
          if (redirectUrl) {
            navigate(redirectUrl)
          } else {
            navigate('/')
          }
        })
        .catch(err => {
          if (err.response && err.response.status === 401) {
            setErrors({ usernameOrEmail: 'Wrong username/email!', password: 'Wrong password!' })
          }
        })
    },
  })

  return (
    <div className="login-main-container">
      <div className="login-container">
        <div className="login-title">Welcome back!</div>
        <form className="login-form" onSubmit={formik.handleSubmit}>
          <TextField
            id="usernameOrEmail"
            name="usernameOrEmail"
            label="Email or username"
            variant="outlined"
            sx={{ marginBottom: '1rem' }}
            value={formik.values.usernameOrEmail}
            onChange={formik.handleChange}
            error={formik.touched.usernameOrEmail && Boolean(formik.errors.usernameOrEmail)}
            helperText={formik.touched.usernameOrEmail && formik.errors.usernameOrEmail}
          />
          <TextField
            id="password"
            name="password"
            label="Password"
            type="password"
            variant="outlined"
            sx={{ marginBottom: '1rem' }}
            value={formik.values.password}
            onChange={formik.handleChange}
            error={formik.touched.password && Boolean(formik.errors.password)}
            helperText={formik.touched.password && formik.errors.password}
          />
          <ThemeProvider theme={theme}>
            <LoadingButton
              fullWidth={true}
              color="primary"
              variant="contained"
              loading={auth.loading}
              type="submit">
              Login
            </LoadingButton>
          </ThemeProvider>
          <div className="go-to-signup">
            Don't have an account?{' '}
            <Link className="go-to-link" to="/signUp">
              Sign up
            </Link>
          </div>
        </form>
      </div>
    </div>
  )
}

export default Login
