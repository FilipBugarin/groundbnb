import React from 'react'
//styling
import './HomePage.css'
//other
import { CakeModel } from './../../models/Cake.model'
import { Link } from 'react-router-dom'
import CakeList from '../Webshop/components/CakeList/CakeList'
import { AxiosInstance } from '../../auth/AxiosInstance'
import { useAuth } from '../../auth/authContext'
import { UserRole } from '../../models/User.model'

const HomePage = () => {
  const auth = useAuth()
  const [allCakes, setAllCakes] = React.useState<CakeModel[]>([])
  const [cakesToShow, setCakesToShow] = React.useState<CakeModel[]>([])

  React.useEffect(() => {
    AxiosInstance.get('/cakes')
      .then(res => {
        setAllCakes(res.data)
      })
      .catch(err => console.error(err))
  }, [])

  React.useEffect(() => {
    setCakesToShow(allCakes.slice(0, 3))
  }, [allCakes])

  return (
    <>
      <div className="homepage-container">
        <div className="top-container">
          <div className="brush-stroke-and-h1">
            <div className="headline-Sweet">Sweet me up</div>
            <div className="button-container">
              {auth.user?.role === UserRole.ROLE_SELLER ? (
                <></>
              ) : (
                <Link className="button" to="customize" style={{ textDecoration: 'none' }}>
                  CUSTOMIZE
                </Link>
              )}
              <Link className="button" to="webshop" style={{ textDecoration: 'none' }}>
                WEBSHOP
              </Link>
            </div>
          </div>
        </div>
        <div className="middle-container">
          <div className="middle-left">
            <div className="middle-left-container">
              <img
                src="https://images.unsplash.com/photo-1512223792601-592a9809eed4?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MzB8fGJha2VyJTIwY2FrZXN8ZW58MHx8MHx8&auto=format&fit=crop&w=500&q=60"
                alt="Customize"></img>
            </div>
          </div>
          <div className="middle-right">
            <h1>How it works</h1>
            <br />
            <p>
              Using filters on the left side of screen, you can choose which ingredients you want
              for you custom cake, what kind of cream you want, you can enter text or even upload a
              photo to use on top of the cake. After you go through all the filters, just click
              search and the list of our shops that can make you the cake you want.
            </p>
            <br />
            <h3>
              {auth.user?.role === UserRole.ROLE_SELLER ? (
                <></>
              ) : (
                <Link to="customize" style={{ textDecoration: 'none', color: 'black' }}>
                  Go to costumize
                </Link>
              )}
            </h3>
          </div>
        </div>
        {cakesToShow.length ? (
          <div className="bottom-container">
            <h1>What we offer</h1>
            <div className="bottom-cake-container">
              <CakeList shownCakes={cakesToShow} />
            </div>
          </div>
        ) : (
          <></>
        )}
      </div>
    </>
  )
}

export default HomePage
