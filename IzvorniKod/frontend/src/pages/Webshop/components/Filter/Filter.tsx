import * as React from 'react'
import { Slider } from '@mui/material'

import './Filter.css'

import { ReactComponent as DropdownIcon } from './../../../../assets/icons/filter-drop-down-icon.svg'
import { ReactComponent as CheckIcon } from './../../../../assets/icons/filter-check.svg'
import { ReactComponent as FilterIcon } from './../../../../assets/icons/filter-icon.svg'
import { IngredientModel } from './../../../../models/Ingredient.model'
import { capitalizeString } from './../../../../utils'
import useWindowSize from './../../../../hooks/useWindowSize'

type FilterProps = {
  prices: number[]
  ingredients: IngredientModel[]
  filterCakes: (val: { ingredients: number[]; prices: number[] }) => void
}

const Filter = (props: FilterProps) => {
  const ingredients = props.ingredients
  const filterCakes = props.filterCakes
  const maxPrice = Math.max(...props.prices)
  const windowSize = useWindowSize()

  const [filterOpen, setFilterOpen] = React.useState<boolean>(false)

  const [filter, setFilter] = React.useState<{ ingredients: number[]; prices: number[] }>({
    ingredients: [],
    prices: [0, maxPrice],
  })

  const [filterGroupOpen, setFilterGroupOpen] = React.useState<{
    ingredients: boolean
    prices: boolean
  }>({
    ingredients: false,
    prices: false,
  })

  React.useEffect(() => {
    if (windowSize.width > 944) {
      setFilterOpen(true)
    }
  }, [windowSize])

  React.useEffect(() => {
    filterCakes(filter)
  }, [filter, filterCakes])

  const handleFilterMobile = () => {
    setFilterOpen(!filterOpen)
  }

  const handleSlider = (event: Event, newValue: number | number[], slider: number) => {
    const newPrices = newValue as number[]

    if (slider === 0) {
      setFilter({
        ...filter,
        prices: [Math.min(newPrices[0], filter.prices[1] - 25), newPrices[1]],
      })
    } else {
      setFilter({
        ...filter,
        prices: [newPrices[0], Math.max(newPrices[1], filter.prices[0] + 25)],
      })
    }
  }

  const handleFilterGroupOpening = (drawer: String) => {
    if (drawer === 'ingredients') {
      setFilterGroupOpen({ ...filterGroupOpen, ingredients: !filterGroupOpen.ingredients })
    } else {
      setFilterGroupOpen({ ...filterGroupOpen, prices: !filterGroupOpen.prices })
    }
  }

  const handleChooseFilter = (ingredient: IngredientModel) => {
    filter.ingredients.includes(ingredient.id)
      ? setFilter({
          ...filter,
          ingredients: filter.ingredients.filter(ingredientId => ingredientId !== ingredient.id),
        })
      : setFilter({ ...filter, ingredients: [...filter.ingredients, ingredient.id] })
  }

  return (
    <div className="webshop-filter-holder">
      <div
        className="webshop-filter-title-container"
        onClick={windowSize.width <= 944 ? handleFilterMobile : undefined}>
        <div className="webshop-filter-title">Filter</div>
        <div className="webshop-filter-title-icon">
          <FilterIcon className="webshop-filter-icon" />
        </div>
      </div>
      {filterOpen && (
        <div className="webshop-filter-list-items-container">
          {ingredients.length !== 0 && (
            <div className="webshop-filter-list-item-container">
              <div
                className={`webshop-filter-list-item ${filterGroupOpen.ingredients && 'open'}`}
                onClick={() => handleFilterGroupOpening('ingredients')}>
                <div className="webshop-filter-list-item-title">Ingredients</div>
                <DropdownIcon
                  className={`webshop-filter-list-item-icon ${
                    filterGroupOpen.ingredients && 'open'
                  }`}
                />
              </div>
              <div
                className={`webshop-filter-list-item-subitems-container ${
                  filterGroupOpen.ingredients && 'open'
                }`}>
                {ingredients.map(ingredient => (
                  <div
                    key={ingredient.id}
                    className={`webshop-filter-list-item-subitem ${
                      filter.ingredients.includes(ingredient.id) && 'active'
                    }`}
                    onClick={() => handleChooseFilter(ingredient)}>
                    <CheckIcon className={`webshop-filter-subitem-icon`} />
                    {capitalizeString(ingredient.name)}
                  </div>
                ))}
              </div>
            </div>
          )}
          <div className="webshop-filter-list-item-container">
            <div
              className={`webshop-filter-list-item ${filterGroupOpen.prices && 'open'}`}
              onClick={() => handleFilterGroupOpening('prices')}>
              <div className="webshop-filter-list-item-title">Prices</div>
              <DropdownIcon
                className={`webshop-filter-list-item-icon ${filterGroupOpen.prices && 'open'}`}
              />
            </div>
            <div
              className={`webshop-filter-list-item-subitems-container ${
                filterGroupOpen.prices && 'open'
              }`}>
              <div className="webshop-filter-list-item-prices-text">
                {filter.prices[0]} - {filter.prices[1]} €
              </div>
              <div className="webshop-filter-list-item-prices-slider">
                <Slider
                  value={filter.prices}
                  onChange={handleSlider}
                  disableSwap
                  min={0}
                  step={5}
                  max={maxPrice}
                  sx={{
                    color: 'rgba(255, 31, 31, 1)',
                    height: '0.125rem',
                    '& .MuiSlider-thumb': {
                      width: '0.6rem',
                      height: '0.6rem',
                      transition: '0.3s cubic-bezier(.47,1.64,.41,.8)',
                      '&:before': {
                        boxShadow: '0 2px 12px 0 rgba(0,0,0,0.4)',
                      },
                      '&:hover, &:focus, &.Mui-focusVisible': {
                        boxShadow: '0px 0px 0px 5px rgba(255, 31, 31, 0.3)',
                      },
                    },
                  }}
                />
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  )
}

export default Filter
