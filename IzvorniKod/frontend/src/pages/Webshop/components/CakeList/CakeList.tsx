import * as React from 'react'

import { Link } from 'react-router-dom'

import './CakeList.css'
import { ReactComponent as AddToCartIcon } from './../../../../assets/icons/add-to-cart.svg'

import { CakeModel } from './../../../../models/Cake.model'
import { useAppDispatch } from '../../../../app/hooks'
import { addToCart } from '../../../../app/cakeSlice'

type CakeListPropsModel = {
  shownCakes: CakeModel[]
}

const CakeList = (props: CakeListPropsModel) => {
  const cakes = props.shownCakes
  const dispatch = useAppDispatch()

  return (
    <div className="webshop-cakes-list-holder">
      {cakes.map(cake => (
        <div key={cake.id} className="webshop-cake-container">
          <Link className="webshop-cake-link" to={`/cake/${cake.id}`}>
            <div className="webshop-cake-image-container">
              <img className="webshop-cake-image" src={cake.imageurl} alt="Cake" />
            </div>
          </Link>
          <div className="webshop-cake-info-container">
            <Link className="webshop-cake-link" to={`/cake/${cake.id}`}>
              <div className="webshop-cake-title">{cake.name}</div>
              <div className="webshop-cake-desc">{cake.description}</div>
            </Link>
            <div
              className="webshop-cake-footer"
              onClick={() => dispatch(addToCart({ cake, quantity: 1 }))}>
              <div className="webshop-cake-price">
                {cake.price} {cake.currency.symbol}
              </div>
              <div className="webshop-cake-cart-btn">
                <AddToCartIcon className="webshop-cake-cart-icon" />
              </div>
            </div>
          </div>
        </div>
      ))}
    </div>
  )
}

export default CakeList
