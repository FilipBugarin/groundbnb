import * as React from 'react'

import './Webshop.css'

import { isBetweenOrEqual } from '../../utils'
import CakeList from './components/CakeList/CakeList'
import Filter from './components/Filter/Filter'
import { CakeModel } from './../../models/Cake.model'
import { IngredientModel } from './../../models/Ingredient.model'
import Spinner from '../../components/Spinner/Spinner'
import { AxiosInstance } from '../../auth/AxiosInstance'

const Webshop = () => {
  const [allCakes, setAllCakes] = React.useState<CakeModel[]>([])
  const [shownCakes, setShownCakes] = React.useState<CakeModel[]>([])
  const [ingredients, setIngredients] = React.useState<IngredientModel[]>([])

  React.useEffect(() => {
    AxiosInstance.get('/cakes')
      .then(res => {
        setAllCakes(res.data)
        setShownCakes(res.data)
      })
      .catch(err => console.error(err))

    AxiosInstance.get('/ingredients')
      .then(res => {
        setIngredients(res.data)
      })
      .catch(err => console.error(err))
  }, [])

  const filterCakes = React.useCallback(
    (filter: { ingredients: number[]; prices: number[] }) => {
      setShownCakes(
        allCakes.filter(cake => {
          if (
            filter.ingredients.length === 0 &&
            isBetweenOrEqual(
              cake.price / cake.currency.ratioToEuro,
              filter.prices[0],
              filter.prices[1]
            )
          ) {
            return true
          } else if (
            filter.ingredients.some(e => e in cake.ingredients.map(ingredient => ingredient.id)) &&
            isBetweenOrEqual(
              cake.price / cake.currency.ratioToEuro,
              filter.prices[0],
              filter.prices[1]
            )
          ) {
            return true
          }
          return false
        })
      )
    },
    [allCakes]
  )

  return (
    <>
      {allCakes.length !== 0 ? (
        <div className="webshop-container">
          <div className="webshop-filter-container">
            <Filter
              prices={allCakes.map(cake => cake.price)}
              ingredients={ingredients}
              filterCakes={filterCakes}
            />
          </div>
          <div className="webshop-cakes-list-container">
            {shownCakes.length !== 0 && <CakeList shownCakes={shownCakes} />}
          </div>
        </div>
      ) : (
        <Spinner />
      )}
    </>
  )
}

export default Webshop
