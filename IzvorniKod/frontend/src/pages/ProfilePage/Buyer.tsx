import * as React from 'react'
import { Link } from 'react-router-dom'
//styling
import './Buyer.css'

//auth
import { useAuth } from '../../auth/authContext'
import { AxiosInstance } from '../../auth/AxiosInstance'

//models
import { PreviousOrdersModel2 } from '../../models/PreviousOrders.model'
import { CustomNotificationModel } from '../../models/Notification.model'

//modal
import Modal from 'react-bootstrap/esm/Modal'
import Button from 'react-bootstrap/Button'

//mui
import { Table, TableBody, TableCell, TableHead, TableRow } from '@mui/material'

const Buyer = () => {
  const auth = useAuth()
  const [prevOrders, setPrevOrders] = React.useState<PreviousOrdersModel2[]>([])
  const [openModalUser, setOpenModalUser] = React.useState<boolean>(false)
  const [openModalCustom, setOpenModalCustom] = React.useState<boolean>(false)
  const [modalOrderData, setModalOrderData] = React.useState<CustomNotificationModel>()
  const [customOrders, setCustomOrders] = React.useState<CustomNotificationModel[]>([])

  React.useEffect(() => {
    AxiosInstance.get('/profile/orderHistory').then(res => {
      setPrevOrders(res.data)
      console.log(res.data)
    })
    AxiosInstance.get('/order/customOrderNotificationsBuyer').then(res => {
      setCustomOrders(res.data.filter((order: any) => order.deliveryAt !== null))
    })
  }, [])

  return (
    <div className="buyer-page-container">
      <Modal show={openModalUser} onHide={() => setOpenModalUser(false)} centered>
        <Modal.Header>
          <Modal.Title>Edit your profile information</Modal.Title>
        </Modal.Header>
        <Modal.Body>Form</Modal.Body>
        <Modal.Footer>
          <Button onClick={() => setOpenModalUser(false)} variant="secondary">
            Cancel
          </Button>
          <Button variant="primary">Save changes</Button>
        </Modal.Footer>
      </Modal>
      <Modal
        show={openModalCustom}
        onHide={() => setOpenModalCustom(false)}
        centered
        fullscreen={'md-down'}
        dialogClassName="custom-order-modal-container">
        <Modal.Header className="custom-order-modal-header">
          <div className="mod-order-title">Order no. {modalOrderData?.orderId}</div>
          <div className="notifications-modal-close-icon" onClick={() => setOpenModalCustom(false)}>
            <i className="fas fa-times notification-card-icon"></i>
          </div>
        </Modal.Header>
        <Modal.Body className="custom-order-modal-body">
          <div className="mod-order-content-item">
            <div className="mod-order-content-title">Seller:</div>
            <div className="mod-order-content-text">{modalOrderData?.sellerName}</div>
          </div>
          <div className="mod-order-content-item">
            <div className="mod-order-content-title">Description:</div>
            <div className="mod-order-content-text">
              {modalOrderData?.description
                ? modalOrderData?.description
                : 'There is no description provided in this order.'}
            </div>
          </div>
          <div className="mod-order-content-item">
            <div className="mod-order-content-title">Picture URL:</div>
            <div className="mod-order-content-text">
              {modalOrderData?.imageUrl
                ? modalOrderData?.imageUrl
                : 'There is no image provided in this order.'}
            </div>
          </div>
          <div className="mod-order-content-item">
            <div className="mod-order-content-title">Ingredients:</div>
            <div className="mod-order-content-text">
              <div className="customize-table-container">
                <Table className="customize-table">
                  <TableHead>
                    <TableRow className="customize-table-header">
                      <TableCell
                        style={{
                          padding: '0.5rem',
                          fontFamily: 'Quicksand-semibold',
                          fontSize: '1rem',
                          color: 'var(--light-black)',
                        }}>
                        Ingredient
                      </TableCell>
                      <TableCell
                        style={{
                          padding: '0.5rem',
                          fontFamily: 'Quicksand-semibold',
                          fontSize: '1rem',
                          color: 'var(--light-black)',
                        }}>
                        Allergen
                      </TableCell>
                      <TableCell
                        style={{
                          padding: '0.5rem',
                          fontFamily: 'Quicksand-semibold',
                          fontSize: '1rem',
                          color: 'var(--light-black)',
                        }}>
                        Amount
                      </TableCell>
                      <TableCell
                        style={{
                          padding: '0.5rem',
                          fontFamily: 'Quicksand-semibold',
                          fontSize: '1rem',
                          color: 'var(--light-black)',
                        }}>
                        Metric
                      </TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {modalOrderData?.ingredients.map(ingredient => (
                      <TableRow
                        key={ingredient.id}
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                        <TableCell
                          style={{
                            padding: '0.5rem',
                            fontFamily: 'Quicksand',
                            fontSize: '0.875rem',
                            color: 'var(--light-black)',
                          }}
                          component="th"
                          scope="row">
                          {ingredient.name}
                        </TableCell>
                        <TableCell
                          style={{
                            padding: '0.5rem',
                            fontFamily: 'Quicksand',
                            fontSize: '0.875rem',
                            color: 'var(--light-black)',
                          }}>
                          {ingredient.allergen ? ingredient.allergen : 'none'}
                        </TableCell>
                        <TableCell
                          style={{
                            padding: '0.5rem',
                            fontFamily: 'Quicksand',
                            fontSize: '0.875rem',
                            color: 'var(--light-black)',
                          }}>
                          {ingredient.amount ? ingredient.amount : 'IDGAF'}
                        </TableCell>
                        <TableCell
                          style={{
                            padding: '0.5rem',
                            fontFamily: 'Quicksand',
                            fontSize: '0.875rem',
                            color: 'var(--light-black)',
                          }}>
                          {ingredient.metric ? ingredient.metric : 'IDGAF'}
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </div>
            </div>
            <div className="mod-order-content-item">
              <div className="mod-order-content-title">Will be delivered by:</div>
              <div className="mod-order-content-text">{modalOrderData?.deliveryAt}</div>
            </div>
            <div className="mod-order-content-item">
              <div className="mod-order-content-title">Order status:</div>
              <div className="mod-order-content-text">
                {modalOrderData?.status
                  ? 'Seller has seen the order.'
                  : 'Seller has not seen the order.'}
              </div>
            </div>
            <div className="mod-order-content-item">
              <div className="mod-order-content-title">Price:</div>
              <div className="mod-order-content-text">{modalOrderData?.price}</div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
      <div className="buyer-all-info-container">
        <div className="buyer-profile-picture-container">
          <img className="buyer-profile-picture" src={auth.user?.profileImageUrl} alt=""></img>
        </div>
        <div className="buyer-text-info-container">
          <div className="buyer-title-button-container">
            <div className="buyer-information-title">Profile information</div>
          </div>
          <div className="buyer-info-container">
            <div className="buyer-username">Username:&nbsp;{auth.user?.name}</div>
          </div>
          <div className="buyer-info-container">
            <div className="buyer-email">E-mail:&nbsp;{auth.user?.email}</div>
          </div>
        </div>
      </div>
      <div className="custom-orders-container">
        <div className="custom-orders-title">Custom orders:</div>
        <div className="custom-orders-grid">
          {customOrders.map(order => (
            <div
              onClick={() => {
                setOpenModalCustom(true)
                setModalOrderData(order)
              }}
              key={order.orderId}
              className="single-custom-order">
              <div className="custom-order-number">Order No. {order.orderId}</div>
              <div className="custom-order-deliveryDate">Delivery date: {order.deliveryAt}</div>
              <div className="custom-order-seller">Seller: {order.sellerName}</div>
              <div className="custom-order-price">Price {order.price.toFixed(2)}</div>
              <div className="custom-order-seen">
                {order.status ? 'Seller has seen the order.' : 'Seller has not seen the order.'}
              </div>
            </div>
          ))}
        </div>
      </div>
      <div className="order-history-container">
        <div className="past-orders-title">Order history:</div>
        <div className="past-orders-container">
          {' '}
          {prevOrders.map(order => (
            <div key={order.orderId} className="order-container">
              {order.cakeList.map(cakeCart => (
                <div
                  key={cakeCart.cake.id.toString() + order.orderId}
                  className="order-cake-container">
                  <Link className="order-cake-link" to={`/cake/${cakeCart.cake.id}`}>
                    <div className="order-cake">
                      <div className="cake-cart-quantity">{cakeCart.quantity}&nbsp; x</div>
                      <div className="cake-cart-price">
                        {cakeCart.cake.price}&nbsp;{cakeCart.cake.currency.symbol}
                      </div>
                      <div className="cake-image-container">
                        <img className="cake-image" src={cakeCart.cake.imageurl} alt=""></img>
                      </div>
                      <div className="cake-title">{cakeCart.cake.name}</div>
                    </div>
                  </Link>
                </div>
              ))}
              <div className="order-sum-container">
                <div className="order-sum">
                  Total {order.price.toFixed(2)}{' '}
                  {order.currency.symbol ? order.currency.symbol : '€'}
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  )
}

export default Buyer
