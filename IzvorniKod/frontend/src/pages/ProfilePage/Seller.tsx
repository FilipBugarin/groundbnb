import * as React from 'react'
import axios from 'axios'
import { useNavigate } from 'react-router-dom'

//styling
import './Seller.css'

//auth
import { useAuth } from '../../auth/authContext'
import { AxiosInstance } from '../../auth/AxiosInstance'

//models
import { PreviousOrdersModel2 } from '../../models/PreviousOrders.model'
import { CustomNotificationModel, NormalNotificationModel } from '../../models/Notification.model'
import { CakeModel } from '../../models/Cake.model'
import { CurrencyModel } from '../../models/Currency.model'

//bootstrap
import Modal from 'react-bootstrap/esm/Modal'
import Form from 'react-bootstrap/esm/Form'
import Button from 'react-bootstrap/Button'

//icons
import { FiEdit3 } from 'react-icons/fi'
import { GoDiffAdded } from 'react-icons/go'
import { RiDeleteBinLine } from 'react-icons/ri'
import CloseIcon from '../../assets/icons/delete.svg'

//mui
import { Table, TableBody, TableCell, TableHead, TableRow } from '@mui/material'
import { IngredientModel } from '../../models/Ingredient.model'

export type IngredientModelForm = {
  allergen: string | null
  amount: number | null
  id: number | null
  metric: string | null
  name: string | null
}

export type CakeModelForm = {
  name: string | null
  description: string | null
  imageurl: string | null
  price: number | null
  currencyId: number | null
}

const Seller = () => {
  const auth = useAuth()
  const [openModalCustom, setOpenModalCustom] = React.useState<boolean>(false)
  const [openModalAddCake, setOpenModalAddCake] = React.useState<boolean>(false)
  const [openModalDelete, setOpenModalDelete] = React.useState<boolean>(false)
  const [cakeDeleteId, setCakeDeleteId] = React.useState<number>(0)

  const [modalOrderData, setModalOrderData] = React.useState<CustomNotificationModel>()
  const [sellerCakes, setSellerCakes] = React.useState<CakeModel[]>([])
  const [customOrders, setCustomOrders] = React.useState<CustomNotificationModel[]>([])
  const [openModalUser, setOpenModalUser] = React.useState<boolean>(false)
  const [addCake, setAddCake] = React.useState<CakeModelForm>({
    name: null,
    description: null,
    imageurl: null,
    price: 0,
    currencyId: 1,
  })

  //Get custom orders and cakes

  React.useEffect(() => {
    AxiosInstance.get('/order/customOrderNotifications').then(res => {
      setCustomOrders(res.data)
      console.log(res.data)
    })
    AxiosInstance.get('/profile/seller').then(res => {
      setSellerCakes(res.data)
    })
  }, [])

  //post mapping for adding cakes
  const handleAddOrder = () => {
    AxiosInstance.post('/cakes/add', addCake)
      .then(res => {
        if (res.status === 200) {
          setOpenModalAddCake(false)
          setAddCake({
            name: null,
            description: null,
            imageurl: null,
            price: 0,
            currencyId: 1,
          })
        }
      })
      .catch(err => console.error(err))
  }

  //function for mapping delete cake
  const handleDeleteOrder = () => {
    AxiosInstance.delete('/cakes/delete?cakeId=' + cakeDeleteId)
      .then(res => {
        if (res.status === 200) {
          setOpenModalDelete(false)
        }
      })
      .catch(err => console.error(err))
  }

  //function for closing addModal
  const handleCloseModal = () => {
    setAddCake({
      name: null,
      description: null,
      imageurl: null,
      price: 0,
      currencyId: 1,
    })
    setOpenModalAddCake(false)
  }

  return (
    <div className="seller-page-container">
      {/* user modal */}
      <Modal show={openModalUser} onHide={() => setOpenModalUser(false)} centered>
        <Modal.Header>
          <Modal.Title>Edit your profile information</Modal.Title>
        </Modal.Header>
        <Modal.Body>Form</Modal.Body>
        <Modal.Footer>
          <Button onClick={() => setOpenModalUser(false)} variant="secondary">
            Cancel
          </Button>
          <Button variant="primary">Save changes</Button>
        </Modal.Footer>
      </Modal>
      {/* delete cake modal */}
      <Modal show={openModalDelete} onHide={() => setOpenModalDelete(false)} centered>
        <Modal.Header>
          <Modal.Title>Are you sure you want to delete your cake?</Modal.Title>
        </Modal.Header>
        <Modal.Footer>
          <Button onClick={() => setOpenModalDelete(false)} variant="secondary">
            Cancel
          </Button>
          <Button onClick={handleDeleteOrder} variant="primary">
            Delete cake
          </Button>
        </Modal.Footer>
      </Modal>
      {/* show custom cake modal */}
      <Modal
        show={openModalCustom}
        onHide={() => setOpenModalCustom(false)}
        centered
        fullscreen={'md-down'}
        dialogClassName="custom-order-modal-container">
        <Modal.Header className="custom-order-modal-header">
          <div className="mod-order-title">Order no. {modalOrderData?.orderId}</div>
          <div className="notifications-modal-close-icon" onClick={() => setOpenModalCustom(false)}>
            <i className="fas fa-times notification-card-icon"></i>
          </div>
        </Modal.Header>
        <Modal.Body className="custom-order-modal-body">
          <div className="mod-order-content-item">
            <div className="mod-order-content-title">Buyer:</div>
            <div className="mod-order-content-text">{modalOrderData?.buyerName}</div>
          </div>
          <div className="mod-order-content-item">
            <div className="mod-order-content-title">Description:</div>
            <div className="mod-order-content-text">
              {modalOrderData?.description
                ? modalOrderData?.description
                : 'There is no description provided in this order.'}
            </div>
          </div>
          <div className="mod-order-content-item">
            <div className="mod-order-content-title">Picture URL:</div>
            <div className="mod-order-content-text">
              {modalOrderData?.imageUrl
                ? modalOrderData?.imageUrl
                : 'There is no image provided in this order.'}
            </div>
          </div>
          <div className="mod-order-content-item">
            <div className="mod-order-content-title">Ingredients:</div>
            <div className="mod-order-content-text">
              <div className="customize-table-container">
                <Table className="customize-table">
                  <TableHead>
                    <TableRow className="customize-table-header">
                      <TableCell
                        style={{
                          padding: '0.5rem',
                          fontFamily: 'Quicksand-semibold',
                          fontSize: '1rem',
                          color: 'var(--light-black)',
                        }}>
                        Ingredient
                      </TableCell>
                      <TableCell
                        style={{
                          padding: '0.5rem',
                          fontFamily: 'Quicksand-semibold',
                          fontSize: '1rem',
                          color: 'var(--light-black)',
                        }}>
                        Allergen
                      </TableCell>
                      <TableCell
                        style={{
                          padding: '0.5rem',
                          fontFamily: 'Quicksand-semibold',
                          fontSize: '1rem',
                          color: 'var(--light-black)',
                        }}>
                        Amount
                      </TableCell>
                      <TableCell
                        style={{
                          padding: '0.5rem',
                          fontFamily: 'Quicksand-semibold',
                          fontSize: '1rem',
                          color: 'var(--light-black)',
                        }}>
                        Metric
                      </TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {modalOrderData?.ingredients.map(ingredient => (
                      <TableRow
                        key={ingredient.id}
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                        <TableCell
                          style={{
                            padding: '0.5rem',
                            fontFamily: 'Quicksand',
                            fontSize: '0.875rem',
                            color: 'var(--light-black)',
                          }}
                          component="th"
                          scope="row">
                          {ingredient.name}
                        </TableCell>
                        <TableCell
                          style={{
                            padding: '0.5rem',
                            fontFamily: 'Quicksand',
                            fontSize: '0.875rem',
                            color: 'var(--light-black)',
                          }}>
                          {ingredient.allergen ? ingredient.allergen : 'none'}
                        </TableCell>
                        <TableCell
                          style={{
                            padding: '0.5rem',
                            fontFamily: 'Quicksand',
                            fontSize: '0.875rem',
                            color: 'var(--light-black)',
                          }}>
                          {ingredient.amount ? ingredient.amount : 'IDGAF'}
                        </TableCell>
                        <TableCell
                          style={{
                            padding: '0.5rem',
                            fontFamily: 'Quicksand',
                            fontSize: '0.875rem',
                            color: 'var(--light-black)',
                          }}>
                          {ingredient.metric ? ingredient.metric : 'IDGAF'}
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </div>
            </div>
            <div className="mod-order-content-item">
              <div className="mod-order-content-title">Has to be delivered by:</div>
              <div className="mod-order-content-text">{modalOrderData?.deliveryAt}</div>
            </div>
            <div className="mod-order-content-item">
              <div className="mod-order-content-title">Price:</div>
              <div className="mod-order-content-text">{modalOrderData?.price}</div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
      {/* add cake modal */}
      <Modal
        show={openModalAddCake}
        onHide={handleCloseModal}
        centered
        fullscreen={'md-down'}
        dialogClassName="edit-cake-modal-container">
        <Modal.Header className="edit-cake-modal-header">
          <div className="mod-order-title">Add a cake</div>
          <div className="notifications-modal-close-icon" onClick={handleCloseModal}>
            <i className="fas fa-times notification-card-icon"></i>
          </div>
        </Modal.Header>
        <Modal.Body className="edit-cake-modal-body">
          <div className="mod-order-content-item">
            <div className="mod-order-content-title">Cake name:</div>
            <div className="mod-order-content-text">
              <Form.Control
                type="text"
                onChange={e => setAddCake({ ...addCake, name: e.target.value })}
                value={addCake?.name ? addCake?.name : ''}
                placeholder="Cake name:"
                id="CakeName"
              />
            </div>
          </div>
          <div className="mod-order-content-item">
            <div className="mod-order-content-title">Description:</div>
            <div className="mod-order-content-text">
              <Form.Control
                type="textarea"
                onChange={e => setAddCake({ ...addCake, description: e.target.value })}
                value={addCake?.description ? addCake?.description : ''}
                placeholder="Cake description:"
                id="CakeDescription"
              />
            </div>
          </div>
          <div className="mod-order-content-item">
            <div className="mod-order-content-title">Picture URL:</div>
            <div className="mod-order-content-text">
              <Form.Control
                type="text"
                onChange={e => setAddCake({ ...addCake, imageurl: e.target.value })}
                value={addCake?.imageurl ? addCake?.imageurl : ''}
                placeholder="Picture of the cake:"
                id="CakeImageUrl"
              />
            </div>
          </div>
          <div className="mod-order-content-item">
            <div className="mod-order-content-item">
              <div className="mod-order-content-title">Price:</div>
              <div className="mod-order-content-text">
                <Form.Control
                  type="text"
                  onChange={e => setAddCake({ ...addCake, price: parseFloat(e.target.value) })}
                  value={addCake?.price ? addCake?.price : ''}
                  placeholder="Price of the cake:"
                  id="CakePrice"
                />
              </div>
            </div>
            <div className="mod-order-content-item">
              <div className="mod-order-content-title">Currency:</div>
              <div className="mod-order-content-text">
                <Form.Select
                  onChange={e =>
                    setAddCake({ ...addCake, currencyId: parseFloat(e.target.value) })
                  }>
                  <option value="1">Euro</option>
                  <option value="2">HRK</option>
                  <option value="3">American Dollar</option>
                </Form.Select>
              </div>
            </div>
            <div className="customize-modal-buttons-container-seller">
              <div className="customize-modal-cancel-button-container">
                <button
                  className="customize-modal-cancel-button"
                  type="button"
                  onClick={handleCloseModal}>
                  Cancel
                </button>
              </div>
              <div className="customize-modal-add-button-container">
                <button
                  className="customize-modal-add-button"
                  onClick={handleAddOrder}
                  type="button">
                  Add
                </button>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>

      {/* main page container */}
      <div className="seller-all-info-container">
        <div className="seller-profile-picture-container">
          <img className="seller-profile-picture" src={auth.user?.profileImageUrl} alt=""></img>
        </div>
        <div className="seller-text-info-container">
          <div className="seller-title-button-container">
            <div className="seller-information-title">Profile information</div>
          </div>
          <div className="seller-info-container">
            <div className="buyer-username">Username:&nbsp;{auth.user?.name}</div>
          </div>
          <div className="seller-info-container">
            <div className="buyer-email">E-mail:&nbsp;{auth.user?.email}</div>
          </div>
        </div>
      </div>
      <div className="custom-orders-container">
        <div className="custom-orders-title">Custom orders:</div>
        <div className="custom-orders-grid">
          {customOrders.map(order => (
            <div
              onClick={() => {
                setOpenModalCustom(true)
                setModalOrderData(order)
              }}
              key={order.orderId}
              className="single-custom-order-seller">
              <div className="custom-order-number">Order No. {order.orderId}</div>
              <div className="custom-order-deliveryDate-seller">
                Delivery date: {order.deliveryAt}
              </div>
              <div className="custom-order-seller">Buyer: {order.buyerName}</div>
              <div className="custom-order-price">Price {order.price}</div>
            </div>
          ))}
        </div>
      </div>
      <div className="seller-cakes-container">
        <div className="seller-cake-title-container">
          <div className="seller-cakes-title">Your cakes:</div>
          <div className="seller-cakes-add-button">
            <GoDiffAdded className="cake-add-icon" onClick={() => setOpenModalAddCake(true)} />
          </div>
        </div>
        <div className="seller-cakes-grid">
          {sellerCakes.map(cake => (
            <div key={cake.id} className="seller-one-cake-container">
              <div className="seller-cake-title-image-container">
                <div className="seller-cake-image-container">
                  <img className="seller-cake-image" src={cake.imageurl} alt=""></img>
                </div>
                <div className="seller-cake-title">{cake.name}</div>
              </div>
              <div className="seller-cake-price">Price {cake.price}</div>
              <div className="seller-cake-buttons">
                <div
                  className="seller-delete-cake"
                  onClick={() => {
                    setOpenModalDelete(true)
                    setCakeDeleteId(cake.id)
                  }}>
                  <RiDeleteBinLine className="cake-delete-icon" />
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  )
}

export default Seller
