import React from 'react'

import './Notifications.css'

import { useAuth } from '../../auth/authContext'
import { AxiosInstance } from '../../auth/AxiosInstance'
import {
  CustomNotificationModel,
  NormalNotificationModel,
  NotificationTypeEnum,
  OrderStatusEnum,
} from '../../models/Notification.model'
import { UserRole } from '../../models/User.model'
import Spinner from '../../components/Spinner/Spinner'
import PendingNotificationCard from './components/PendingNotificationCard/PendingNotificationCard'
import AcceptedNotificationCard from './components/AcceptedNotificationCard/AcceptedNotificationCard'
import RejectedNotificationCard from './components/RejectedNotificationCard/RejectedNotificationCard'
import NormalNotificationCard from './components/NormalNotificationCard/NormalNotificationCard'
import NotificationModal from './components/NotificationModal/NotificationModal'
import NormalNotificationModal from './components/NormalNotificationModal/NormalNotificationModal'

export type NotificationCardType = {
  orderId: number
  name: string
  type: NotificationTypeEnum
  status: OrderStatusEnum
  createdAt: Date
  updatedAt: Date
}

const NotificationsSeller = () => {
  const auth = useAuth()
  const [dataLoading, setDataLoading] = React.useState<boolean>(true)
  const [allNotifications, setAllNotifications] = React.useState<
    (CustomNotificationModel | NormalNotificationModel)[]
  >([])
  const [cardNotifications, setCardNotifications] = React.useState<NotificationCardType[]>([])
  const [selectedOrder, setSelectedOrder] = React.useState<
    CustomNotificationModel | NormalNotificationModel | null
  >(null)
  const [selectedNotificationCard, setSelectedNotificationCard] =
    React.useState<NotificationCardType | null>(null)
  const [openModal, setOpenModal] = React.useState<boolean>(false)
  const [openNormalModal, setOpenNormalModal] = React.useState<boolean>(false)

  // Get all custom notifications
  React.useEffect(() => {
    AxiosInstance.get<CustomNotificationModel[]>(
      `order/customOrderNotifications${auth.user?.role === UserRole.ROLE_BUYER ? 'Buyer' : ''}`
    )
      .then(async res => {
        let notifications: NormalNotificationModel[] = []
        if (auth.user?.role === UserRole.ROLE_SELLER) {
          notifications = (await AxiosInstance.get('/order/getNormalNotifications')).data
        }

        setAllNotifications([
          ...notifications.filter(n => !n.seen),
          ...res.data.filter(order =>
            auth.user?.role === UserRole.ROLE_SELLER
              ? order.status === OrderStatusEnum.PENDING ||
                (order.status === OrderStatusEnum.BUYER_REJECTED && !order.seen) ||
                (order.status === OrderStatusEnum.BUYER_ACCEPTED && !order.seen)
              : order.status === OrderStatusEnum.SELLER_ACCEPTED ||
                (order.status === OrderStatusEnum.SELLER_REJECTED && !order.seen)
          ),
        ])

        setCardNotifications(
          [
            ...notifications
              .filter(n => !n.seen)
              .map<NotificationCardType>(d => ({
                orderId: d.orderId,
                name: d.buyerName,
                type: NotificationTypeEnum.NORMAL,
                status: OrderStatusEnum.SELLER_ACCEPTED,
                createdAt: d.createdAt,
                updatedAt: d.updatedAt,
              })),
            ...res.data
              .filter(order =>
                auth.user?.role === UserRole.ROLE_SELLER
                  ? order.status === OrderStatusEnum.PENDING ||
                    (order.status === OrderStatusEnum.BUYER_REJECTED && !order.seen) ||
                    (order.status === OrderStatusEnum.BUYER_ACCEPTED && !order.seen)
                  : order.status === OrderStatusEnum.SELLER_ACCEPTED ||
                    (order.status === OrderStatusEnum.SELLER_REJECTED && !order.seen)
              )
              .map<NotificationCardType>(notification => ({
                orderId: notification.orderId,
                name:
                  auth.user?.role === UserRole.ROLE_BUYER
                    ? notification.sellerName
                    : notification.buyerName,
                type: NotificationTypeEnum.CUSTOM,
                status: notification.status,
                createdAt: notification.createdAt,
                updatedAt: notification.updatedAt,
              })),
          ].sort((a, b) => (a.updatedAt < b.updatedAt ? 1 : a.updatedAt > b.updatedAt ? -1 : 0))
        )
      })
      .catch(err => console.error(err))
      .finally(() => setTimeout(() => setDataLoading(false), 500))
  }, [auth.user])

  // Handle closing modal
  React.useEffect(() => {
    if (selectedOrder && selectedNotificationCard && !openModal && !openNormalModal) {
      if (selectedNotificationCard.type === NotificationTypeEnum.NORMAL) {
        setAllNotifications(a => a.filter(n => n.orderId !== selectedOrder.orderId))
        setCardNotifications(a => a.filter(n => n.orderId !== selectedOrder.orderId))
      } else if (
        auth.user?.role === UserRole.ROLE_SELLER
          ? (selectedNotificationCard?.status === OrderStatusEnum.PENDING &&
              ((selectedOrder as CustomNotificationModel).status ===
                OrderStatusEnum.SELLER_ACCEPTED ||
                (selectedOrder as CustomNotificationModel).status ===
                  OrderStatusEnum.SELLER_REJECTED)) ||
            selectedNotificationCard?.status === OrderStatusEnum.BUYER_REJECTED ||
            selectedNotificationCard?.status === OrderStatusEnum.BUYER_ACCEPTED
          : (selectedNotificationCard?.status === OrderStatusEnum.SELLER_ACCEPTED &&
              ((selectedOrder as CustomNotificationModel).status ===
                OrderStatusEnum.BUYER_REJECTED ||
                (selectedOrder as CustomNotificationModel).status ===
                  OrderStatusEnum.BUYER_ACCEPTED)) ||
            selectedNotificationCard?.status === OrderStatusEnum.SELLER_REJECTED
      ) {
        setAllNotifications(a => a.filter(n => n.orderId !== selectedOrder.orderId))
        setCardNotifications(a => a.filter(n => n.orderId !== selectedOrder.orderId))
      }
      setSelectedOrder(null)
      setSelectedNotificationCard(null)
    }
  }, [auth.user, selectedOrder, selectedNotificationCard, openModal, openNormalModal])

  // Handle selecting one of pending notifications and opening a modal with more info
  const handleNotificationClick = (notificationId: number) => {
    const order = allNotifications.find(notification => notification.orderId === notificationId)
    const notification = cardNotifications.find(
      notification => notification.orderId === notificationId
    )
    setSelectedOrder(order ? order : null)
    setSelectedNotificationCard(notification ? (notification as NotificationCardType) : null)

    if (notification?.type === NotificationTypeEnum.NORMAL) {
      setOpenNormalModal(true)
    } else {
      setOpenModal(true)
    }
  }

  return (
    <div className="notifications-outer-container">
      {!dataLoading ? (
        <>
          {selectedNotificationCard?.type === NotificationTypeEnum.CUSTOM ? (
            <NotificationModal
              openModal={openModal}
              order={selectedOrder as CustomNotificationModel}
              setOpenModal={setOpenModal}
              setOrder={
                setSelectedOrder as React.Dispatch<
                  React.SetStateAction<CustomNotificationModel | null>
                >
              }
            />
          ) : (
            <NormalNotificationModal
              openModal={openNormalModal}
              order={selectedOrder as NormalNotificationModel}
              setOpenModal={setOpenNormalModal}
            />
          )}
          <div className="notifications-container">
            <div className="notifications-title">So, what's going on?</div>
            {cardNotifications.length ? (
              <div className="notifications-list-container">
                {cardNotifications.map(notification => {
                  if (notification.type === NotificationTypeEnum.CUSTOM) {
                    switch (notification.status) {
                      case OrderStatusEnum.BUYER_ACCEPTED:
                        return (
                          <AcceptedNotificationCard
                            key={notification.orderId}
                            notification={notification}
                            handleNotificationClick={handleNotificationClick}
                          />
                        )
                      case OrderStatusEnum.SELLER_REJECTED:
                        return (
                          <RejectedNotificationCard
                            key={notification.orderId}
                            notification={notification}
                            handleNotificationClick={handleNotificationClick}
                          />
                        )
                      case OrderStatusEnum.BUYER_REJECTED:
                        return (
                          <RejectedNotificationCard
                            key={notification.orderId}
                            notification={notification}
                            handleNotificationClick={handleNotificationClick}
                          />
                        )
                      default:
                        return (
                          <PendingNotificationCard
                            key={notification.orderId}
                            notification={notification}
                            handleNotificationClick={handleNotificationClick}
                          />
                        )
                    }
                  } else {
                    return (
                      <NormalNotificationCard
                        key={notification.orderId}
                        notification={notification}
                        handleNotificationClick={handleNotificationClick}
                      />
                    )
                  }
                })}
              </div>
            ) : (
              <div className="notifications-empty-container">
                <div className="notifications-empty-text">
                  No new notifications, You can relax now!
                </div>
                <div className="notifications-empty-icon">
                  <i className="fas fa-glass-cheers fa-3x"></i>
                </div>
              </div>
            )}
          </div>
        </>
      ) : (
        <Spinner />
      )}
    </div>
  )
}

export default NotificationsSeller
