import React from 'react'

import Modal from 'react-bootstrap/esm/Modal'
import { NormalNotificationModel } from '../../../../models/Notification.model'
import { AxiosInstance } from '../../../../auth/AxiosInstance'

type NotificationModalProps = {
  order: NormalNotificationModel | null
  openModal: boolean
  setOpenModal: React.Dispatch<React.SetStateAction<boolean>>
}

const NotificationModal = (props: NotificationModalProps) => {
  const { order, openModal, setOpenModal } = props

  const handleCloseModal = () => {
    AxiosInstance.put('/order/seen', {
      orderId: order?.orderId,
      seen: true,
    })
    setOpenModal(false)
  }

  return (
    <Modal
      show={openModal}
      fullscreen={'md-down'}
      onHide={handleCloseModal}
      dialogClassName="notifications-modal-outer-container"
      centered>
      <Modal.Header className="notifications-modal-header">
        <div className="notifications-modal-title">Order No. {order?.orderId}</div>
        <div className="notifications-modal-close-icon" onClick={handleCloseModal}>
          <i className="fas fa-times notification-card-icon"></i>
        </div>
      </Modal.Header>
      <Modal.Body className="notifications-modal-body">
        <div className="notifications-modal-helper-text">
          Here is your order No. {order?.orderId} made by <span>{order?.buyerName}</span>. More
          detailed order info is located on your profile page.
        </div>
        <div className="notifications-modal-content-item">
          <div className="notifications-modal-content-title">Buyer:</div>
          <div className="notifications-modal-content-text">{order?.buyerName}</div>
        </div>
        <div className="notifications-modal-content-item">
          <div className="notifications-modal-content-title">Cakes:</div>
          {order?.cakeList.map(cake => (
            <div className="notifications-modal-content-text">
              – {cake.quantity} x {cake.cake.name}
            </div>
          ))}
        </div>
        <div className="notifications-modal-content-item">
          <div className="notifications-modal-content-title">Price:</div>
          <div className="notifications-modal-content-text">
            {order?.price.toFixed(2)} {order?.currency.symbol}
          </div>
          <div className="notifications-modal-buttons-container">
            <div className="notifications-modal-lower-button-container">
              <div className="notifications-modal-postpone-button-container">
                <button
                  className="notifications-modal-postpone-button"
                  type="button"
                  onClick={handleCloseModal}>
                  Dismiss
                </button>
              </div>
            </div>
          </div>
        </div>
      </Modal.Body>
    </Modal>
  )
}

export default NotificationModal
