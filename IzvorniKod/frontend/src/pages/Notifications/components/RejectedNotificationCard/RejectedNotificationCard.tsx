import React from 'react'
import moment from 'moment'

import './RejectedNotificationCard.css'

import { NotificationCardType } from '../../Notifications'

interface INotificationCardProps {
  notification: NotificationCardType
  handleNotificationClick: (notificationId: number) => void
}

const RejectedNotificationCard = (props: INotificationCardProps) => {
  const { notification, handleNotificationClick } = props

  const handleClick = () => {
    handleNotificationClick(notification.orderId)
  }

  return (
    <div className="rejected-notification-card-container" onClick={handleClick}>
      <div className="rejected-notification-card-icon-container">
        <i className={`fas fa-palette rejected-notification-card-icon`}></i>
      </div>
      <div className="rejected-notification-card-content">
        <div className="rejected-notification-card-title">
          Order No. {notification.orderId} rejected!
        </div>
        <div className="rejected-notification-card-buyer">Rejected by: {notification.name}</div>
      </div>
      <div className="rejected-notification-card-date-container">
        <div className="rejected-notification-card-date">
          <i className="fas fa-calendar-week me-2"></i>
          {moment.duration(moment().diff(notification.updatedAt)).humanize()} ago
        </div>
      </div>
    </div>
  )
}

export default RejectedNotificationCard
