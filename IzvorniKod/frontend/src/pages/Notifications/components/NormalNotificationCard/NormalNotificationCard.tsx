import React from 'react'
import moment from 'moment'

import './NormalNotificationCard.css'

import { NotificationCardType } from '../../Notifications'

interface INotificationCardProps {
  notification: NotificationCardType
  handleNotificationClick: (notificationId: number) => void
}

const NormalNotificationCard = (props: INotificationCardProps) => {
  const { notification, handleNotificationClick } = props

  const handleClick = () => {
    handleNotificationClick(notification.orderId)
  }

  return (
    <div className="normal-notification-card-container" onClick={handleClick}>
      <div className="normal-notification-card-icon-container">
        <i className={`fas fa-bell normal-notification-card-icon`}></i>
      </div>
      <div className="normal-notification-card-content">
        <div className="normal-notification-card-title">Order No. {notification.orderId}</div>
        <div className="normal-notification-card-buyer">Made by: {notification.name}</div>
      </div>
      <div className="normal-notification-card-date-container">
        <div className="normal-notification-card-date">
          <i className="fas fa-calendar-week me-2"></i>
          {moment.duration(moment().diff(notification.updatedAt)).humanize()} ago
        </div>
      </div>
    </div>
  )
}

export default NormalNotificationCard
