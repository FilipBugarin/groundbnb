import React from 'react'
import moment from 'moment'

import './PendingNotificationCard.css'

import { NotificationCardType } from '../../Notifications'
import { useAuth } from '../../../../auth/authContext'
import { UserRole } from '../../../../models/User.model'

interface INotificationCardProps {
  notification: NotificationCardType
  handleNotificationClick: (notificationId: number) => void
}

const PendingNotificationCard = (props: INotificationCardProps) => {
  const auth = useAuth()
  const { notification, handleNotificationClick } = props

  const handleClick = () => {
    handleNotificationClick(notification.orderId)
  }

  return (
    <div className="pending-notification-card-container" onClick={handleClick}>
      <div className="pending-notification-card-icon-container">
        <i className={`fas fa-palette pending-notification-card-icon`}></i>
      </div>
      <div className="pending-notification-card-content">
        <div className="pending-notification-card-title">Order No. {notification.orderId}</div>
        <div className="pending-notification-card-buyer">
          {auth.user?.role === UserRole.ROLE_SELLER ? `Ordered by:` : `Sent by:`}{' '}
          {notification.name}
        </div>
      </div>
      <div className="pending-notification-card-date-container">
        <div className="pending-notification-card-date">
          <i className="fas fa-calendar-week me-2"></i>
          {moment.duration(moment().diff(notification.updatedAt)).humanize()} ago
        </div>
      </div>
    </div>
  )
}

export default PendingNotificationCard
