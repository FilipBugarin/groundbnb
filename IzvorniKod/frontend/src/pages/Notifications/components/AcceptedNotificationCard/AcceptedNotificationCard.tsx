import React from 'react'
import moment from 'moment'

import './AcceptedNotificationCard.css'

import { NotificationCardType } from '../../Notifications'

interface INotificationCardProps {
  notification: NotificationCardType
  handleNotificationClick: (notificationId: number) => void
}

const AcceptedNotificationCard = (props: INotificationCardProps) => {
  const { notification, handleNotificationClick } = props

  const handleClick = () => {
    handleNotificationClick(notification.orderId)
  }

  return (
    <div className="accepted-notification-card-container" onClick={handleClick}>
      <div className="accepted-notification-card-icon-container">
        <i className={`fas fa-palette accepted-notification-card-icon`}></i>
      </div>
      <div className="accepted-notification-card-content">
        <div className="accepted-notification-card-title">
          Order No. {notification.orderId} accepted!
        </div>
        <div className="accepted-notification-card-buyer">Accepted by: {notification.name}</div>
      </div>
      <div className="accepted-notification-card-date-container">
        <div className="accepted-notification-card-date">
          <i className="fas fa-calendar-week me-2"></i>
          {moment.duration(moment().diff(notification.updatedAt)).humanize()} ago
        </div>
      </div>
    </div>
  )
}

export default AcceptedNotificationCard
