import React from 'react'

import { Table, TableBody, TableCell, TableHead, TableRow } from '@mui/material'
import Modal from 'react-bootstrap/esm/Modal'
import Form from 'react-bootstrap/esm/Form'
import moment from 'moment'
import { CustomNotificationModel, OrderStatusEnum } from '../../../../models/Notification.model'
import { CurrencyModel } from '../../../../models/Currency.model'
import { AxiosInstance } from '../../../../auth/AxiosInstance'
import { useAuth } from '../../../../auth/authContext'
import { UserRole } from '../../../../models/User.model'

type NotificationModalProps = {
  order: CustomNotificationModel | null
  setOrder: React.Dispatch<React.SetStateAction<CustomNotificationModel | null>>
  openModal: boolean
  setOpenModal: React.Dispatch<React.SetStateAction<boolean>>
}

const NotificationModal = (props: NotificationModalProps) => {
  const auth = useAuth()
  const { order, setOrder, openModal, setOpenModal } = props
  const [currencyOptions, setCurrencyOptions] = React.useState<CurrencyModel[]>([])
  const [priceError, setPriceError] = React.useState<boolean>(false)
  const [orderRejected, setOrderRejected] = React.useState<boolean>(false)
  const [rejectionReason, setRejectionReason] = React.useState<string>('')

  // Get all currencies
  React.useEffect(() => {
    AxiosInstance.get<CurrencyModel[]>('/cakes/currencies')
      .then(res => {
        setCurrencyOptions(res.data)
      })
      .catch(err => console.error(err))
  }, [])

  // Handle accepting order
  const handleAcceptOrder = () => {
    if (order?.price && order.currency) {
      if (auth.user?.role === UserRole.ROLE_SELLER) {
        AxiosInstance.post('/order/acceptOrRejectCustomNotification', {
          orderId: order.orderId,
          status: OrderStatusEnum.SELLER_ACCEPTED,
          price: order.price,
          currencyId: order.currency.currencyId,
        })
          .then(res => {
            setOrder({ ...order, status: OrderStatusEnum.SELLER_ACCEPTED })
            setOpenModal(false)
            setRejectionReason('')
            setOrderRejected(false)
          })
          .catch(err => console.error(err))
      } else {
        AxiosInstance.post('/order/acceptOrRejectCustomBuyerNotification', {
          orderId: order.orderId,
          status: OrderStatusEnum.BUYER_ACCEPTED,
        })
          .then(res => {
            setOrder({ ...order, status: OrderStatusEnum.BUYER_ACCEPTED })
            setOpenModal(false)
            setOrderRejected(false)
          })
          .catch(err => console.error(err))
      }
    } else {
      setPriceError(true)
    }
  }

  // Handle reject order
  const handleRejectOrder = () => {
    if (order) {
      if (auth.user?.role === UserRole.ROLE_SELLER) {
        AxiosInstance.post('/order/acceptOrRejectCustomNotification', {
          orderId: order.orderId,
          status: OrderStatusEnum.SELLER_REJECTED,
          rejectReason: rejectionReason,
        })
          .then(res => {
            setOrder({ ...order, status: OrderStatusEnum.SELLER_REJECTED })
            setOpenModal(false)
            setRejectionReason('')
            setOrderRejected(false)
          })
          .catch(err => console.error(err))
      } else {
        AxiosInstance.post('/order/acceptOrRejectCustomBuyerNotification', {
          orderId: order.orderId,
          status: OrderStatusEnum.BUYER_REJECTED,
        })
          .then(res => {
            setOrder({ ...order, status: OrderStatusEnum.BUYER_REJECTED })
            setOpenModal(false)
            setOrderRejected(false)
          })
          .catch(err => console.error(err))
      }
    }
  }

  const handleCloseModal = () => {
    if (
      order?.status === OrderStatusEnum.SELLER_REJECTED ||
      order?.status === OrderStatusEnum.BUYER_REJECTED ||
      order?.status === OrderStatusEnum.BUYER_ACCEPTED
    ) {
      AxiosInstance.put('/order/seen', {
        orderId: order.orderId,
        seen: true,
      })
    }
    setOpenModal(false)
  }

  return (
    <Modal
      show={openModal}
      fullscreen={'md-down'}
      onHide={handleCloseModal}
      dialogClassName="notifications-modal-outer-container"
      centered>
      <Modal.Header className="notifications-modal-header">
        <div className="notifications-modal-title">Order No. {order?.orderId}</div>
        <div className="notifications-modal-close-icon" onClick={handleCloseModal}>
          <i className="fas fa-times notification-card-icon"></i>
        </div>
      </Modal.Header>
      {!orderRejected ? (
        <Modal.Body className="notifications-modal-body">
          <div className="notifications-modal-helper-text">
            {order?.status === OrderStatusEnum.PENDING ? (
              <>
                Here is the order made by <span>{order?.buyerName}</span>.
              </>
            ) : order?.status === OrderStatusEnum.BUYER_REJECTED ? (
              <>
                This order has been rejected by <span>{order?.buyerName}</span>.
              </>
            ) : order?.status === OrderStatusEnum.BUYER_ACCEPTED ? (
              <>
                This order has been accepted by <span>{order?.buyerName}</span>!
              </>
            ) : order?.status === OrderStatusEnum.SELLER_ACCEPTED ? (
              <>
                Here is your full order with the price specified by <span>{order?.sellerName}</span>{' '}
                at the bottom.
              </>
            ) : (
              <>
                This order has been rejected by <span>{order?.sellerName}</span>.
              </>
            )}
          </div>
          <div className="notifications-modal-content-item">
            <div className="notifications-modal-content-title">Description:</div>
            <div className="notifications-modal-content-text">
              {order?.description
                ? order.description
                : 'There is no description provided in this order.'}
            </div>
          </div>
          <div className="notifications-modal-content-item">
            <div className="notifications-modal-content-title">Picture Url:</div>
            <div className="notifications-modal-content-text" style={{ wordBreak: 'break-all' }}>
              {order?.imageUrl ? order.imageUrl : 'There is no picture needed on this cake.'}
            </div>
          </div>
          <div className="notifications-modal-content-item">
            <div className="notifications-modal-content-title">Ingredients wanted:</div>
            <div className="notifications-modal-content-text">
              <div className="customize-table-container">
                <Table className="customize-table">
                  <TableHead>
                    <TableRow className="customize-table-header">
                      <TableCell
                        style={{
                          padding: '0.5rem',
                          fontFamily: 'Quicksand-semibold',
                          fontSize: '1rem',
                          color: 'var(--light-black)',
                        }}>
                        Ingredient
                      </TableCell>
                      <TableCell
                        style={{
                          padding: '0.5rem',
                          fontFamily: 'Quicksand-semibold',
                          fontSize: '1rem',
                          color: 'var(--light-black)',
                        }}>
                        Allergen
                      </TableCell>
                      <TableCell
                        style={{
                          padding: '0.5rem',
                          fontFamily: 'Quicksand-semibold',
                          fontSize: '1rem',
                          color: 'var(--light-black)',
                        }}>
                        Amount
                      </TableCell>
                      <TableCell
                        style={{
                          padding: '0.5rem',
                          fontFamily: 'Quicksand-semibold',
                          fontSize: '1rem',
                          color: 'var(--light-black)',
                        }}>
                        Metric
                      </TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {order?.ingredients.map(ingredient => (
                      <TableRow
                        key={ingredient.id}
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                        <TableCell
                          style={{
                            padding: '0.5rem',
                            fontFamily: 'Quicksand',
                            fontSize: '0.875rem',
                            color: 'var(--light-black)',
                          }}
                          component="th"
                          scope="row">
                          {ingredient.name}
                        </TableCell>
                        <TableCell
                          style={{
                            padding: '0.5rem',
                            fontFamily: 'Quicksand',
                            fontSize: '0.875rem',
                            color: 'var(--light-black)',
                          }}>
                          {ingredient.allergen ? ingredient.allergen : 'none'}
                        </TableCell>
                        <TableCell
                          style={{
                            padding: '0.5rem',
                            fontFamily: 'Quicksand',
                            fontSize: '0.875rem',
                            color: 'var(--light-black)',
                          }}>
                          {ingredient.amount ? ingredient.amount : 'IDGAF'}
                        </TableCell>
                        <TableCell
                          style={{
                            padding: '0.5rem',
                            fontFamily: 'Quicksand',
                            fontSize: '0.875rem',
                            color: 'var(--light-black)',
                          }}>
                          {ingredient.metric ? ingredient.metric : 'IDGAF'}
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </div>
            </div>
          </div>
          <div className="notifications-modal-content-item">
            <div className="notifications-modal-content-title">
              The date on which{' '}
              {auth.user?.role === UserRole.ROLE_SELLER ? order?.buyerName : 'you'} want
              {auth.user?.role === UserRole.ROLE_SELLER ? 's' : ''} it done:
            </div>
            <div className="notifications-modal-content-text">
              {moment(order?.deliveryAt).locale('hr').calendar({ sameElse: 'DD/MM/YYYY' })}
            </div>
          </div>
          <div className="notifications-modal-content-item-input">
            <div className="notifications-modal-content-title">
              {order?.status === OrderStatusEnum.PENDING ? (
                <>If you want to accept it, please specify your price here:</>
              ) : order?.status === OrderStatusEnum.SELLER_ACCEPTED ? (
                <>Price specified by {order?.sellerName}:</>
              ) : order?.status === OrderStatusEnum.SELLER_REJECTED ? (
                <>Rejection reason:</>
              ) : (
                <>Price:</>
              )}
            </div>
            {auth.user?.role === UserRole.ROLE_SELLER &&
            order?.status === OrderStatusEnum.PENDING ? (
              <>
                <div className="notifications-modal-content-forms">
                  <Form.Control
                    as="input"
                    type="number"
                    placeholder="Price:"
                    className="notifications-modal-price-input"
                    value={order?.price ? order.price : ''}
                    onChange={e => {
                      if (order) {
                        setPriceError(false)
                        setOrder({
                          ...order,
                          price: parseFloat(parseFloat(e.target.value).toFixed(3)),
                        })
                      }
                    }}
                  />
                  <Form.Select
                    className="notifications-modal-currency-input"
                    value={order?.currency?.currencyId ? order.currency.currencyId : ''}
                    onChange={e => {
                      if (order) {
                        setPriceError(false)
                        const curr = currencyOptions.find(
                          curr => curr.currencyId.toString() === e.target.value
                        )

                        if (curr)
                          setOrder({
                            ...order,
                            currency: curr,
                          })
                      }
                    }}>
                    <option value="" style={{ color: 'var(--light-black)' }} disabled>
                      Currency
                    </option>
                    {currencyOptions.map(currency => (
                      <option key={currency.currencyId} value={currency.currencyId}>
                        {currency.currencyName} ({currency.symbol})
                      </option>
                    ))}
                  </Form.Select>
                </div>
                <div className="notifications-modal-price-error">
                  {priceError ? 'This is required for accepting this order!' : ' '}
                </div>
              </>
            ) : order?.status === OrderStatusEnum.SELLER_REJECTED ? (
              <div className="notifications-modal-content-text">
                {order.rejectReason ? order.rejectReason : 'No reason provided.'}
              </div>
            ) : (
              <div className="notifications-modal-content-text">
                {order?.price.toFixed(2)} {order?.currency.symbol}
              </div>
            )}
            <div className="notifications-modal-buttons-container">
              {order?.status === OrderStatusEnum.PENDING ||
              order?.status === OrderStatusEnum.SELLER_ACCEPTED ? (
                <>
                  <div className="notifications-modal-upper-button-container">
                    <div className="notifications-modal-reject-button-container">
                      <button
                        className="notifications-modal-reject-button"
                        type="button"
                        onClick={() =>
                          auth.user?.role === UserRole.ROLE_SELLER
                            ? setOrderRejected(true)
                            : handleRejectOrder()
                        }>
                        Reject
                      </button>
                    </div>
                    <div className="notifications-modal-accept-button-container">
                      <button
                        className="notifications-modal-accept-button"
                        type="button"
                        onClick={handleAcceptOrder}>
                        Accept
                      </button>
                    </div>
                  </div>
                  <div className="notifications-modal-lower-button-container">
                    <div className="notifications-modal-postpone-button-container">
                      <button
                        className="notifications-modal-postpone-button"
                        type="button"
                        onClick={handleCloseModal}>
                        I don't know yet
                      </button>
                    </div>
                  </div>
                </>
              ) : (
                <div className="notifications-modal-lower-button-container">
                  <div className="notifications-modal-postpone-button-container">
                    <button
                      className="notifications-modal-postpone-button"
                      type="button"
                      onClick={handleCloseModal}>
                      Dismiss
                    </button>
                  </div>
                </div>
              )}
            </div>
          </div>
        </Modal.Body>
      ) : (
        <Modal.Body className="notifications-modal-rejected-body">
          <div className="notifications-modal-go-back-container">
            <div className="notifications-modal-go-back" onClick={() => setOrderRejected(false)}>
              <i className="fas fa-arrow-left"></i> Go back
            </div>
          </div>
          <div className="notifications-modal-reject-description-container">
            <div className="notifications-modal-content-title">
              Here you can describe your rejection reason
            </div>
            <Form.Control
              className="notifications-modal-reject-description"
              as="textarea"
              placeholder="Rejection reason..."
              rows={4}
              value={rejectionReason}
              onChange={e => setRejectionReason(e.target.value)}
            />
          </div>
          <div className="notifications-modal-final-reject-button-container">
            <button
              className="notifications-modal-final-reject-button"
              type="button"
              onClick={handleRejectOrder}>
              Reject
            </button>
          </div>
        </Modal.Body>
      )}
    </Modal>
  )
}

export default NotificationModal
