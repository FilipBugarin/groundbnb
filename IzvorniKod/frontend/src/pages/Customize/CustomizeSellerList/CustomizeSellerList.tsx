import React from 'react'

import './CustomizeSellerList.css'
import { ReactComponent as SelectIcon } from '../../../assets/icons/plus-icon.svg'

import { CustomizeSeller } from '../Customize'

interface ICustomizeSellerListProps {
  sellers: CustomizeSeller[]
  selectSeller: (seller: CustomizeSeller) => void
}

const CustomizeSellerList = (props: ICustomizeSellerListProps) => {
  const { sellers, selectSeller } = props

  const handleSelect = (seller: CustomizeSeller) => {
    selectSeller(seller)
  }

  return (
    <div
      className="customize-seller-list-main-container"
      style={{ display: sellers.length ? 'flex' : 'none' }}>
      <div className="customize-seller-list-container">
        <div className="customize-seller-title">Available sellers:</div>
        <div className="customize-seller-list">
          {sellers.map(seller => (
            <div key={seller.id} className="customize-seller-list-item-container">
              <div className="customize-seller-list-item-image-container">
                <img
                  className="customize-seller-list-item-image"
                  src={seller.profileImageUrl}
                  alt="Seller"
                />
              </div>
              <div className="customize-seller-list-item-description-container">
                <div className="customize-seller-list-item-title">{seller.name}</div>
                <div className="customize-seller-list-item-prices-container">
                  Average prices:
                  <div className="customize-seller-list-item-prices">
                    {seller.averagePrices.map(avg => (
                      <span key={avg.currency.currencyId}>
                        {avg.avg}
                        {avg.currency.symbol}
                      </span>
                    ))}
                  </div>
                </div>
              </div>
              <div className="customize-seller-list-select-button-container">
                <div
                  className="customize-seller-list-item-select-icon-container"
                  onClick={() => handleSelect(seller)}>
                  <SelectIcon className="customize-seller-list-item-select-icon" />
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  )
}

export default CustomizeSellerList
