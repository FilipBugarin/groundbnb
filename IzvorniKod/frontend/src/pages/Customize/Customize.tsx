import React from 'react'
import { Modal, Table, TableBody, TableCell, TableHead, TableRow, TextField } from '@mui/material'
import { MobileDatePicker } from '@mui/lab'

import './Customize.css'
import CloseIcon from '../../assets/icons/delete.svg'

import { useAuth } from '../../auth/authContext'
import CustomizeForm from './CustomizeForm/CustomizeForm'
import CustomizeSellerList from './CustomizeSellerList/CustomizeSellerList'
import { AxiosInstance } from '../../auth/AxiosInstance'
import { CurrencyModel } from '../../models/Currency.model'
import { IngredientModel } from '../../models/Ingredient.model'
import { useNavigate } from 'react-router-dom'

export type CustomIngredientType = {
  ingredientId: number
  amount: number
  metricId: number
}

export type CustomizeAveragePrice = {
  avg: number
  currency: CurrencyModel
}

export type CustomizeSeller = {
  id: number
  name: string
  profileImageUrl: string
  averagePrices: CustomizeAveragePrice[]
}

export type CustomizeFormType = {
  sellerId: number
  customImageUrl: string | null
  description: string | null
  ingredients: CustomIngredientType[]
  deliveryAt: Date
}

const Customize = () => {
  const auth = useAuth()
  const navigate = useNavigate()
  const [modalOpen, setModalOpen] = React.useState<boolean>(false)
  const [form, setForm] = React.useState<CustomizeFormType>({
    sellerId: 0,
    customImageUrl: null,
    description: null,
    ingredients: [],
    deliveryAt: new Date(),
  })
  const [sellers, setSellers] = React.useState<CustomizeSeller[]>([])
  const [ingredients, setIngredients] = React.useState<IngredientModel[]>([])

  // Get all ingredients and metrics
  React.useEffect(() => {
    AxiosInstance.get('/ingredients')
      .then(res => setIngredients(res.data))
      .catch(err => console.error(err))
  }, [])

  const handleCustomizeSearch = () => {
    AxiosInstance.get('/profile/sellers')
      .then(res => setSellers(res.data))
      .catch(err => console.error(err))
  }

  const handleSelectSeller = (seller: CustomizeSeller) => {
    setForm({ ...form, sellerId: seller.id })
    setModalOpen(true)
  }

  const handleSubmitOrder = () => {
    AxiosInstance.post('/order/sendCustomCakeRequest', {
      ...form,
      deliveryAt: form.deliveryAt.toISOString().split('T')[0],
    })
      .then(res => {
        navigate('/')
      })
      .catch(err => console.error(err))
  }

  return (
    <div className="customize-main-container">
      <Modal open={modalOpen} onClose={() => setModalOpen(false)}>
        <div className="customize-modal-container">
          <div className="customize-modal-header-container">
            <div className="customize-modal-title">Your order</div>
            <div className="customize-modal-close-button-container">
              <img
                className="customize-modal-close-button"
                src={CloseIcon}
                alt="Close"
                onClick={() => setModalOpen(false)}></img>
            </div>
          </div>
          <div className="customize-modal-info-container">
            <div className="customize-modal-info-title">Picture url: </div>
            <div className="customize-modal-info-text">{form.customImageUrl}</div>
          </div>
          <div className="customize-modal-info-container">
            <div className="customize-modal-info-title">Description: </div>
            <div className="customize-modal-info-text">{form.description}</div>
          </div>
          <div className="customize-modal-info-container">
            <div className="customize-modal-info-title">Ingredients: </div>
            <div className="customize-table-container">
              <Table className="customize-table">
                <TableHead>
                  <TableRow className="customize-table-header">
                    <TableCell
                      style={{
                        padding: '0.5rem',
                        fontFamily: 'Quicksand-semibold',
                        fontSize: '1rem',
                        color: 'var(--light-black)',
                      }}>
                      Ingredient
                    </TableCell>
                    <TableCell
                      style={{
                        padding: '0.5rem',
                        fontFamily: 'Quicksand-semibold',
                        fontSize: '1rem',
                        color: 'var(--light-black)',
                      }}>
                      Allergen
                    </TableCell>
                    <TableCell
                      style={{
                        padding: '0.5rem',
                        fontFamily: 'Quicksand-semibold',
                        fontSize: '1rem',
                        color: 'var(--light-black)',
                      }}>
                      Amount
                    </TableCell>
                    <TableCell
                      style={{
                        padding: '0.5rem',
                        fontFamily: 'Quicksand-semibold',
                        fontSize: '1rem',
                        color: 'var(--light-black)',
                      }}>
                      Metric
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {form.ingredients.map(ingredient => (
                    <TableRow
                      key={ingredient.ingredientId}
                      sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                      <TableCell
                        style={{
                          padding: '0.5rem',
                          fontFamily: 'Quicksand',
                          fontSize: '0.875rem',
                          color: 'var(--light-black)',
                        }}
                        component="th"
                        scope="row">
                        {ingredients.filter(ing => ing.id === ingredient.ingredientId)[0].name}
                      </TableCell>
                      <TableCell
                        style={{
                          padding: '0.5rem',
                          fontFamily: 'Quicksand',
                          fontSize: '0.875rem',
                          color: 'var(--light-black)',
                        }}>
                        {ingredients.filter(ing => ing.id === ingredient.ingredientId)[0].allergen
                          ? ingredients.filter(ing => ing.id === ingredient.ingredientId)[0]
                              .allergen
                          : 'none'}
                      </TableCell>
                      <TableCell
                        style={{
                          padding: '0.5rem',
                          fontFamily: 'Quicksand',
                          fontSize: '0.875rem',
                          color: 'var(--light-black)',
                        }}>
                        {ingredient.amount ? ingredient.amount : 'IDGAF'}
                      </TableCell>
                      <TableCell
                        style={{
                          padding: '0.5rem',
                          fontFamily: 'Quicksand',
                          fontSize: '0.875rem',
                          color: 'var(--light-black)',
                        }}>
                        {ingredient.metricId
                          ? ingredients
                              .filter(ing => ing.id === ingredient.ingredientId)[0]
                              .metrics.filter(metric => metric.metricId === ingredient.metricId)[0]
                              .metricName
                          : 'IDGAF'}
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </div>
          </div>
          <div className="customize-modal-info-container">
            <div className="customize-modal-info-title">Selected seller: </div>
            <div className="customize-modal-info-text">
              {sellers.filter(seller => seller.id === form.sellerId).length
                ? sellers.filter(seller => seller.id === form.sellerId)[0].name
                : ''}
            </div>
          </div>
          <div className="customize-modal-info-date-container">
            <div className="customize-modal-info-title">Select date: </div>
            <MobileDatePicker
              className="customize-modal-info-date"
              label="Date mobile"
              inputFormat="DD/MM/yyyy"
              value={form.deliveryAt}
              onChange={e => {
                setForm({ ...form, deliveryAt: e ? e : new Date() })
              }}
              renderInput={params => <TextField {...params} />}
            />
          </div>
          <div className="customize-modal-submit-button-container">
            <button
              className="customize-modal-submit-button"
              type="button"
              onClick={handleSubmitOrder}>
              Submit order
            </button>
          </div>
        </div>
      </Modal>
      <CustomizeForm
        ingredients={ingredients}
        form={form}
        setForm={setForm}
        loading={auth.loading}
        handleCustomizeSearch={handleCustomizeSearch}
      />
      <CustomizeSellerList sellers={sellers} selectSeller={handleSelectSeller} />
    </div>
  )
}

export default Customize
