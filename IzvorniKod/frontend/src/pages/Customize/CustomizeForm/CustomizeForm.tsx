import React, { Dispatch, SetStateAction } from 'react'
import { ThemeProvider, createTheme } from '@mui/material/styles'
import { Modal, Table, TableBody, TableCell, TableHead, TableRow } from '@mui/material'
import { LoadingButton } from '@mui/lab'

import './CustomizeForm.css'
import CloseIcon from '../../../assets/icons/delete.svg'

import { IngredientModel } from '../../../models/Ingredient.model'
import { CustomIngredientType, CustomizeFormType } from '../Customize'
import Form from 'react-bootstrap/Form'

const theme = createTheme({
  palette: {
    primary: {
      main: '#e63746',
    },
  },
})

interface ICustomizeForm {
  loading: boolean
  ingredients: IngredientModel[]
  form: CustomizeFormType
  setForm: Dispatch<SetStateAction<CustomizeFormType>>
  handleCustomizeSearch: () => void
}

const CustomizeForm = (props: ICustomizeForm) => {
  const { loading, form, setForm, handleCustomizeSearch, ingredients } = props
  const [modalOpen, setModalOpen] = React.useState<boolean>(false)
  const [selectedIngredientError, setSelectedIngredientError] = React.useState<boolean>(false)
  const [selectedMetricError, setSelectedMetricError] = React.useState<boolean>(false)

  const [ingredientForm, setIngredientForm] = React.useState<CustomIngredientType>({
    amount: 0,
    ingredientId: 0,
    metricId: 0,
  })

  // Reset error
  React.useEffect(() => {
    setSelectedIngredientError(false)
    setSelectedMetricError(false)
  }, [ingredientForm.ingredientId, ingredientForm.metricId])

  // Handle modal submit for ingredient
  const handleModalSubmit = (e: any) => {
    e.preventDefault()

    if (ingredientForm.ingredientId) {
      if (ingredientForm.amount && !ingredientForm.metricId) {
        setSelectedMetricError(true)
      } else {
        if (
          ingredients.filter(ing => ing.id === ingredientForm.ingredientId)[0].metrics.length === 1
        ) {
          const metric = ingredients.filter(ing => ing.id === ingredientForm.ingredientId)[0]
            .metrics[0]
          setForm({
            ...form,
            ingredients: [
              ...form.ingredients,
              {
                amount: ingredientForm.amount,
                ingredientId: ingredientForm.ingredientId,
                metricId: metric.metricId,
              },
            ],
          })
        } else {
          setForm({ ...form, ingredients: [...form.ingredients, ingredientForm] })
        }
        setIngredientForm({
          amount: 0,
          ingredientId: 0,
          metricId: 0,
        })
        setModalOpen(false)
      }
    } else {
      setSelectedIngredientError(true)
    }
  }

  // Handle modal closing
  const handleCloseModal = () => {
    setIngredientForm({
      amount: 0,
      ingredientId: 0,
      metricId: 0,
    })
    setModalOpen(false)
  }

  // Handle submit
  const handleSubmit = (e: any) => {
    e.preventDefault()

    if (form.ingredients.length) {
      handleCustomizeSearch()
    } else {
      setSelectedIngredientError(true)
    }
  }

  return (
    <div className="customize-container">
      <Modal open={modalOpen} onClose={() => setModalOpen(false)}>
        <div className="customize-modal-container">
          <div className="customize-modal-header-container">
            <div className="customize-modal-title">Select ingredient info</div>
            <div className="customize-modal-close-button-container">
              <img
                className="customize-modal-close-button"
                src={CloseIcon}
                alt="Close"
                onClick={handleCloseModal}></img>
            </div>
          </div>
          <form className="customize-modal-form" onSubmit={handleModalSubmit}>
            <div className="customize-modal-select-ingredient-input">
              <div className="customize-form-form-title">Ingredient</div>
              <Form.Select
                id="selectedIngredient"
                value={ingredientForm.ingredientId ? ingredientForm.ingredientId : ''}
                onChange={e => {
                  setIngredientForm({
                    ...ingredientForm,
                    ingredientId: parseInt(e.target.value.toString()),
                  })
                }}>
                <option value="" disabled>
                  Select ingredient
                </option>
                {ingredients
                  .filter(
                    ingredient =>
                      !form.ingredients.map(ing => ing.ingredientId).includes(ingredient.id)
                  )
                  .map(ingredient => (
                    <option key={ingredient.id} value={ingredient.id}>
                      {ingredient.name}
                    </option>
                  ))}
              </Form.Select>
              {selectedIngredientError ? (
                <div className="customize-form-error">This is required</div>
              ) : (
                <></>
              )}
            </div>
            <div className="customize-modal-amount-container">
              <div className="customize-form-form-title">Amount</div>
              <Form.Control
                disabled={!ingredientForm.ingredientId}
                as="input"
                type="number"
                onChange={e =>
                  setIngredientForm({
                    ...ingredientForm,
                    amount: parseFloat(parseFloat(e.target.value).toFixed(3)),
                    metricId:
                      ingredients.find(ing => ing.id === ingredientForm.ingredientId)!.metrics
                        .length === 1
                        ? ingredients.find(ing => ing.id === ingredientForm.ingredientId)!
                            .metrics[0].metricId
                        : ingredientForm.metricId,
                  })
                }
                value={ingredientForm.amount ? ingredientForm.amount : ''}
                placeholder="Amount:"
                id="ingredientAmount"
              />
            </div>
            <div className="customize-modal-select-metric-input">
              <div className="customize-form-form-title">Metric</div>
              <Form.Select
                disabled={!ingredientForm.ingredientId}
                id="selectedMetric"
                value={
                  ingredientForm.metricId
                    ? ingredientForm.metricId
                    : ingredientForm.ingredientId
                    ? ingredients.find(ing => ing.id === ingredientForm.ingredientId)!.metrics
                        .length === 1
                      ? ingredients.find(ing => ing.id === ingredientForm.ingredientId)!.metrics[0]
                          .metricId
                      : ''
                    : ''
                }
                onChange={e => {
                  setIngredientForm({
                    ...ingredientForm,
                    metricId: parseInt(e.target.value.toString()),
                  })
                }}>
                <option disabled value="">
                  Select metric option
                </option>
                {ingredientForm.ingredientId &&
                  ingredients
                    .filter(ing => ing.id === ingredientForm.ingredientId)[0]
                    .metrics.map(metric => (
                      <option key={metric.metricId} value={metric.metricId}>
                        {metric.metricName}
                      </option>
                    ))}
              </Form.Select>
              {selectedMetricError ? (
                <div className="customize-form-error">This is required</div>
              ) : (
                <></>
              )}
            </div>
            <div className="customize-modal-buttons-container">
              <div className="customize-modal-add-button-container">
                <button className="customize-modal-add-button" type="submit">
                  Add
                </button>
              </div>
              <div className="customize-modal-cancel-button-container">
                <button
                  className="customize-modal-cancel-button"
                  type="button"
                  onClick={handleCloseModal}>
                  Cancel
                </button>
              </div>
            </div>
          </form>
        </div>
      </Modal>
      <div className="customize-form">
        <div className="customize-form-container">
          <div className="customize-form-title">Customize</div>
          <form className="customize-form-form" onSubmit={handleSubmit}>
            <div className="customize-form-picture-container">
              <div className="customize-form-form-title">Picture URL</div>
              <Form.Control
                type="text"
                onChange={e => setForm({ ...form, customImageUrl: e.target.value })}
                value={form.customImageUrl ? form.customImageUrl : ''}
                placeholder="URL:"
                id="pictureURL"
              />
            </div>
            <div className="customize-form-desc-container">
              <div className="customize-form-form-title">Description</div>
              <Form.Control
                as="textarea"
                rows={3}
                onChange={e => setForm({ ...form, description: e.target.value })}
                value={form.description ? form.description : ''}
                placeholder="Description:"
                id="description"
              />
            </div>
            <div className="customize-form-ingredient-list-container">
              <div className="customize-form-ingredient-list-container-header">
                <div className="customize-form-ingredient-list-title">Ingredient list</div>
                <div className="customize-form-ingredient-list-add-button-container">
                  <button
                    className="customize-form-ingredient-list-add-button"
                    type="button"
                    onClick={() => setModalOpen(true)}>
                    Add ingredient
                  </button>
                </div>
              </div>
              {form.ingredients.length ? (
                <div className="customize-table-container">
                  <Table className="customize-table">
                    <TableHead>
                      <TableRow className="customize-table-header">
                        <TableCell
                          style={{
                            padding: '0.5rem',
                            fontFamily: 'Quicksand-semibold',
                            fontSize: '1rem',
                            color: 'var(--light-black)',
                          }}>
                          Ingredient
                        </TableCell>
                        <TableCell
                          style={{
                            padding: '0.5rem',
                            fontFamily: 'Quicksand-semibold',
                            fontSize: '1rem',
                            color: 'var(--light-black)',
                          }}>
                          Allergen
                        </TableCell>
                        <TableCell
                          style={{
                            padding: '0.5rem',
                            fontFamily: 'Quicksand-semibold',
                            fontSize: '1rem',
                            color: 'var(--light-black)',
                          }}>
                          Amount
                        </TableCell>
                        <TableCell
                          style={{
                            padding: '0.5rem',
                            fontFamily: 'Quicksand-semibold',
                            fontSize: '1rem',
                            color: 'var(--light-black)',
                          }}>
                          Metric
                        </TableCell>
                        <TableCell
                          style={{
                            padding: '0.5rem',
                            fontFamily: 'Quicksand-semibold',
                            fontSize: '1rem',
                            color: 'var(--light-black)',
                          }}></TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {form.ingredients.map(ingredient => (
                        <TableRow
                          key={ingredient.ingredientId}
                          sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                          <TableCell
                            style={{
                              padding: '0.5rem',
                              fontFamily: 'Quicksand',
                              fontSize: '0.875rem',
                              color: 'var(--light-black)',
                            }}
                            component="th"
                            scope="row">
                            {ingredients.filter(ing => ing.id === ingredient.ingredientId)[0].name}
                          </TableCell>
                          <TableCell
                            style={{
                              padding: '0.5rem',
                              fontFamily: 'Quicksand',
                              fontSize: '0.875rem',
                              color: 'var(--light-black)',
                            }}>
                            {ingredients.filter(ing => ing.id === ingredient.ingredientId)[0]
                              .allergen
                              ? ingredients.filter(ing => ing.id === ingredient.ingredientId)[0]
                                  .allergen
                              : 'none'}
                          </TableCell>
                          <TableCell
                            style={{
                              padding: '0.5rem',
                              fontFamily: 'Quicksand',
                              fontSize: '0.875rem',
                              color: 'var(--light-black)',
                            }}>
                            {ingredient.amount ? ingredient.amount : 'IDGAF'}
                          </TableCell>
                          <TableCell
                            style={{
                              padding: '0.5rem',
                              fontFamily: 'Quicksand',
                              fontSize: '0.875rem',
                              color: 'var(--light-black)',
                            }}>
                            {ingredient.metricId
                              ? ingredients
                                  .filter(ing => ing.id === ingredient.ingredientId)[0]
                                  .metrics.filter(
                                    metric => metric.metricId === ingredient.metricId
                                  )[0].metricName
                              : 'IDGAF'}
                          </TableCell>
                          <TableCell
                            style={{
                              padding: '0.5rem',
                            }}>
                            <img
                              className="customize-table-delete-button"
                              src={CloseIcon}
                              alt="Delete"
                              onClick={() =>
                                setForm({
                                  ...form,
                                  ingredients: form.ingredients.filter(
                                    ing => ing.ingredientId !== ingredient.ingredientId
                                  ),
                                })
                              }></img>
                          </TableCell>
                        </TableRow>
                      ))}
                    </TableBody>
                  </Table>
                </div>
              ) : (
                <></>
              )}
            </div>
            {selectedIngredientError ? (
              <div className="customize-form-error">At least 1 ingredient required!</div>
            ) : (
              <></>
            )}
            <div className="customize-form-submit-button-container">
              <ThemeProvider theme={theme}>
                <LoadingButton
                  loading={loading}
                  fullWidth={true}
                  color="primary"
                  variant="contained"
                  type="submit">
                  Search
                </LoadingButton>
              </ThemeProvider>
            </div>
          </form>
        </div>
      </div>
      <div className="customize-image-hero">
        <div className="customize-image-container">
          <img
            className="customize-image"
            src={
              'https://images.unsplash.com/photo-1586985289688-ca3cf47d3e6e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80'
            }
            alt=""
          />
        </div>
      </div>
    </div>
  )
}

export default CustomizeForm
