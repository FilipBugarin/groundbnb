import * as React from 'react'

import './CartItem.css'

import { CakeCart } from '../../../models/Cake.model'
import { ReactComponent as DeleteIcon } from '../../../assets/icons/delete.svg'
import { removeFromCart, addToCart } from '../../../app/cakeSlice'
import { useAppDispatch } from '../../../app/hooks'

type CartItemProps = {
  cake: CakeCart
}

const CartItem = ({ cake }: CartItemProps) => {
  const dispatch = useAppDispatch()

  return (
    <div className="cart-item">
      <div className="cart-item-image-container">
        <img className="cart-item-image" src={cake.cake.imageurl} alt="Cake" />
      </div>
      <div className="cart-item-main">
        <div className="cart-item-header">
          <div className="cart-item-title">{cake.cake.name}</div>
          <div className="cart-item-delete-button" onClick={() => dispatch(removeFromCart(cake))}>
            <DeleteIcon className="cart-item-delete-icon" />
          </div>
        </div>
        <div className="cart-item-footer">
          <div className="cart-item-quantity">
            Quantity:{' '}
            <span>
              <i
                className="fas fa-minus-circle me-2 fa-md change-quantity-icon"
                onClick={() => {
                  dispatch(removeFromCart(cake))
                  if (cake.quantity - 1) {
                    dispatch(addToCart({ ...cake, quantity: cake.quantity - 1 }))
                  }
                }}></i>
              {cake.quantity}
              <i
                className="fas fa-plus-circle ms-2 fa-md change-quantity-icon"
                onClick={() => {
                  dispatch(removeFromCart(cake))
                  dispatch(addToCart({ ...cake, quantity: cake.quantity + 1 }))
                }}></i>
            </span>
          </div>
          <div className="cart-item-price">
            {cake.cake.price} {cake.cake.currency.symbol}
          </div>
        </div>
      </div>
    </div>
  )
}

export default CartItem
