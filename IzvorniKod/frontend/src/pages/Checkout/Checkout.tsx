import * as React from 'react'

import './Checkout.css'

import CartItem from './components/CartItem'
import { ReactComponent as SadEmoji } from '../../assets/icons/sad-emoji.svg'
import { useAppSelector, useAppDispatch } from '../../app/hooks'
import { removeAll } from '../../app/cakeSlice'
import { useAuth } from '../../auth/authContext'
import { AxiosInstance } from '../../auth/AxiosInstance'
import { useNavigate } from 'react-router-dom'

type CheckoutCakeType = {
  cakeId: number
  quantity: number
}

const CheckoutPage = () => {
  const auth = useAuth()
  const navigate = useNavigate()
  const cart = useAppSelector(state => state.cakes)
  const dispatch = useAppDispatch()

  const handleCheckout = () => {
    if (auth.user) {
      const cakeList = cart.map<CheckoutCakeType>(cake => ({
        cakeId: cake.cake.id,
        quantity: cake.quantity,
      }))
      const price = cart.reduce<number>((sum, cake) => {
        return sum + (cake.cake.price / cake.cake.currency.ratioToEuro) * cake.quantity
      }, 0)

      AxiosInstance.post('/order/checkout', {
        cakeList,
        price,
        currencyId: 1,
      }).then(res => {
        dispatch(removeAll())
        navigate('/')
      })
    } else {
      navigate({ pathname: '/login', search: '?redirectUrl=/checkout' })
    }
  }

  return (
    <>
      {cart.length > 0 ? (
        <div className="checkout-container">
          <div className="checkout-main-container">
            <div className="checkout-title">Your cart:</div>
            <div className="checkout-cart-items">
              {cart.map(cake => (
                <CartItem key={cake.cake.id} cake={cake} />
              ))}
            </div>
            <div className="checkout-total-container">
              <div className="checkout-total-text">Total</div>
              <div className="checkout-total-amount">
                {cart
                  .reduce((sum, cake2) => {
                    return (
                      sum + (cake2.cake.price / cake2.cake.currency.ratioToEuro) * cake2.quantity
                    )
                  }, 0)
                  .toFixed(2)}{' '}
                €
              </div>
            </div>
            <button type="button" className="checkout-button" onClick={handleCheckout}>
              Checkout
            </button>
          </div>
        </div>
      ) : (
        <div className="cart-nothing-container">
          <div className="cart-emoji-sad-container">
            <SadEmoji className="cart-emoji-sad" />
          </div>
          <div className="cart-no-items-container">Oops, you don't have anything in your cart!</div>
        </div>
      )}
    </>
  )
}

export default CheckoutPage
