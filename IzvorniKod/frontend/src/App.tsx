import React from 'react'
import { BrowserRouter as Router, Navigate, Route, Routes } from 'react-router-dom'
import DateAdapter from '@mui/lab/AdapterMoment'
import { LocalizationProvider } from '@mui/lab'

import './App.css'

import { useAuth } from './auth/authContext'
import ScrollToTop from './components/ScrollToTop/ScrollToTop'
import Footer from './components/Footer/Footer'
import Header from './components/Header/Header'
import Webshop from './pages/Webshop/Webshop'
import HomePage from './pages/HomePage/HomePage'
import CheckoutPage from './pages/Checkout/Checkout'
import Login from './pages/Login/Login'
import SignUp from './pages/SignUp/SignUp'
import Cake from './pages/Cake/Cake'
import Customize from './pages/Customize/Customize'
import Notifications from './pages/Notifications/Notifications'
import { UserRole } from './models/User.model'
import Buyer from './pages/ProfilePage/Buyer'
import Seller from './pages/ProfilePage/Seller'

function App() {
  const auth = useAuth()
  const [loadingRefresh, setLoadingRefresh] = React.useState<boolean>(true)

  React.useEffect(() => {
    if (!auth.user) {
      if (!auth.loading) {
        auth.refreshUser().finally(() => {
          setLoadingRefresh(false)
        })
      }
    } else {
      setLoadingRefresh(false)
    }
  }, [auth])

  return (
    <LocalizationProvider dateAdapter={DateAdapter}>
      <div className="app">
        <Router>
          <ScrollToTop />
          <Header />
          <main>
            {!loadingRefresh ? (
              <Routes>
                <Route path="/" element={<HomePage />} />
                <Route path="webshop" element={<Webshop />} />
                <Route path="checkout" element={<CheckoutPage />} />
                <Route
                  path="customize"
                  element={
                    auth.user?.accessToken ? (
                      auth.user.role === UserRole.ROLE_BUYER ? (
                        <Customize />
                      ) : (
                        <Navigate to="/" />
                      )
                    ) : (
                      <Navigate to={{ pathname: '/login', search: '?redirectUrl=/customize' }} />
                    )
                  }
                />
                {auth.user?.accessToken ? (
                  <>
                    <Route path="profile-buyer" element={<Buyer />} />
                    <Route path="profile-seller" element={<Seller />} />
                    <Route path="notifications" element={<Notifications />} />
                    {/* <Route path="notifications-buyer" element={} /> */}
                  </>
                ) : (
                  <>
                    <Route path="login" element={<Login />} />
                    <Route path="signUp" element={<SignUp />} />
                  </>
                )}
                <Route path="cake/:slug" element={<Cake />} />
                <Route path="*" element={<Navigate to="/" />} />
              </Routes>
            ) : (
              <></>
            )}
          </main>
          <Footer />
        </Router>
      </div>
    </LocalizationProvider>
  )
}

export default App
