export const capitalizeString = (str: String) => {
  return str.charAt(0).toUpperCase() + str.slice(1)
}

export const isBetweenOrEqual = (val: number, first: number, second: number) => {
  return val >= first && val <= second
}

export const getWindowSize = () => {
  const { innerWidth: width, innerHeight: height } = window
  return {
    width,
    height,
  }
}
