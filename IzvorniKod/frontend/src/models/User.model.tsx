export enum UserRole {
  ROLE_BUYER = 'ROLE_BUYER',
  ROLE_SELLER = 'ROLE_SELLER',
}

export type User = {
  id: number
  name: string
  email: string
  profileImageUrl: string
  role: UserRole
  accessToken: string
}
