import { CakeCart } from './Cake.model'
import { CurrencyModel } from './Currency.model'
import { IngredientCakeModel } from './Ingredient.model'

export type CustomNotificationModel = {
  orderId: number
  buyerId: number
  buyerName: string
  sellerId: number
  sellerName: string
  imageUrl: string
  description: string
  ingredients: IngredientCakeModel[]
  status: number
  rejectReason: string
  price: number
  currency: CurrencyModel
  seen: boolean
  deliveryAt: Date
  createdAt: Date
  updatedAt: Date
}

export type NormalNotificationModel = {
  orderId: number
  buyerId: number
  buyerName: string
  cakeList: CakeCart[]
  price: number
  currency: CurrencyModel
  seen: boolean
  createdAt: Date
  updatedAt: Date
}

export enum NotificationTypeEnum {
  CUSTOM = 'custom',
  NORMAL = 'normal',
}

export enum OrderStatusEnum {
  PENDING = 0,
  SELLER_ACCEPTED = 1,
  SELLER_REJECTED = -1,
  BUYER_ACCEPTED = 2,
  BUYER_REJECTED = -2,
}
