import { CakeCart, CakeCart2 } from "./Cake.model"
import { CurrencyModel } from "./Currency.model" 

export type PreviousOrdersModel = {
   buyerId: number
   cakeList: CakeCart[]
   currency: CurrencyModel
   orderId: number
   price: number
   curreny: CurrencyModel
   
}

export type PreviousOrdersModel2 = {
   buyerId: number
   cakeList: CakeCart2[]
   currency: CurrencyModel
   orderId: number
   price: number
   curreny: CurrencyModel
   
}