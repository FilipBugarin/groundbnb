export type IngredientModel = {
  id: number
  name: string
  allergen: string
  metrics: MetricModel[]
}

export type IngredientCakeModel = {
  id: number
  name: string
  allergen: string
  amount: number
  metric: string
}

export type MetricModel = {
  metricId: number
  metricName: string
}
