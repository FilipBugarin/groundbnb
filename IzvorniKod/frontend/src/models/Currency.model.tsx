export type CurrencyModel = {
  currencyId: number
  currencyName: string
  ratioToEuro: number
  symbol: string
}
