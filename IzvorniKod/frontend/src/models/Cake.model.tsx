import { IngredientCakeModel } from './Ingredient.model'
import { CurrencyModel } from './Currency.model'

export type CakeModel = {
  id: number
  name: string
  description: string
  imageurl: string
  price: number
  currency: CurrencyModel
  ingredients: IngredientCakeModel[]
}

export type CakeCart = {
  cake: CakeModel
  quantity: number
}

export type CakeModel2 = {
  cake: {
    id: number
    name: string
    description: string
    imageurl: string
    price: number
    ingredients: IngredientCakeModel[]
    currency: CurrencyModel
  }
}

export type CakeCart2 = CakeModel2 & { quantity: number } 