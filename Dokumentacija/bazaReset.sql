drop table if exists CakesIngredients cascade;
drop table if exists Ingredient cascade;
drop table if exists Cake cascade;
drop table if exists users cascade;
drop table if exists roles cascade;
drop table if exists user_roles cascade;

CREATE TABLE Cake
(
  Id INT GENERATED ALWAYS AS identity not NULL,
  Name VARCHAR NOT NULL,
  Description VARCHAR NOT NULL,
  ImageUrl VARCHAR,
  Price FLOAT NOT NULL,
  Currency VARCHAR not null,
  PRIMARY KEY (Id)
);

CREATE TABLE Ingredient
(
  Id INT GENERATED ALWAYS AS identity not NULL,
  Name VARCHAR NOT NULL,
  Allergen VARCHAR,
  PRIMARY KEY (Id)
);

CREATE TABLE CakesIngredients
(
  CakeId INT NOT NULL,
  IngredientId INT NOT NULL,
  Amount FLOAT NOT NULL,
  Metric VARCHAR,
  PRIMARY KEY (CakeId, IngredientId),
  FOREIGN KEY (CakeId) REFERENCES Cake(Id),
  FOREIGN KEY (IngredientId) REFERENCES Ingredient(Id)
);

CREATE TABLE roles (
  id INT GENERATED ALWAYS AS identity not NULL,
  name varchar(60) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE (name)
);

CREATE TABLE users (
  id INT GENERATED ALWAYS AS identity not NULL,
  name varchar(40) NOT NULL,
  username varchar(15) NOT NULL,
  email varchar(40) NOT NULL,
  password varchar(100) NOT NULL,
  role_id INT NOT NULL,
  created_at timestamp DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamp DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  CONSTRAINT fk_user_roles_role_id FOREIGN KEY (role_id) REFERENCES roles (id),
  UNIQUE (username),
  UNIQUE (email)
);

INSERT INTO roles(name) VALUES('ROLE_BUYER');
INSERT INTO roles(name) VALUES('ROLE_SELLER');

insert into users(name,username,email,"password",role_id) values('buyer','BuyerUser','buyer@gmail.com','$2a$10$Fnz4MgWbmj27gHiIEzEt0e1e/tUZ7ij4MfKzqWLECGGD1XcKtxAGW',1);
insert into users(name,username,email,"password",role_id) values('seller','SellerUser','seller@gmail.com','$2a$10$DRUkXz6107JJMVcYvXx7POBQGcJrQ5KU5knThjXTW3Cn4FMIf4/9y',2);


INSERT INTO public.ingredient(
	name, allergen)
	VALUES ( 'eggs', 'egg'), 
		   ( 'sugar', NULL),
		   ( 'oil', 'sunflower'),
		   ( 'milk', 'milk'),
		   ( 'cocoa', 'cocoa'),
		   ( 'flour', 'wheat'),
		   ( 'baking powder', 'sodium'),
		   ( 'chocolate', 'cocoa'),
		   ( 'whip cream', 'gelatine'),
		   ( 'margarine', 'milk'),
		   ( 'walnuts', 'walnuts'),
		   ( 'chestnut puree', 'chestnuts'),
		   ( 'rum', 'alcohol'),
		   ( 'cookies', NULL),
		   ( 'butter', 'milk'),
		   ( 'cream cheese', 'milk'),
		   ( 'lemon', 'lemon'),
		   ( 'sour cream', NULL),
		   ('egg whites','egg'),
		   ('vanilla sugar', 'vanilla'),
		   ('coconut flour','coconut'),
		   ('yolk','egg'),
		   ('maraschino', 'alcohol');

		  
INSERT INTO public.cake(name, description, imageurl, price, currency) VALUES 
	( 'Chocolate cake', 'Simple and tasty beautiful chocolate cake','https://podravkaiovariations.azureedge.net/881a63fa-6421-11eb-97f3-0242ac12001c/v/5a667e06-64be-11eb-aef3-0242ac130010/1600x3200-5a668d88-64be-11eb-ac4d-0242ac130010.webp', 
	 '250','HRK'),
	( 'Chestnut cake', 'Easy and without baking', 'https://podravkaiovariations.azureedge.net/d15fd556-63de-11eb-8777-0242ac120046/v/f2b1f6a6-64bc-11eb-b6c2-0242ac130010/1600x1200-f2b21938-64bc-11eb-9498-0242ac130010.webp',
	 '300','EUR'),
	( 'Cheesecake', 'Best cheesecake in the whole world', 'https://domacica.com.hr/wp-content/uploads/2014/04/domacica-cheesecake-100x100.jpg',
	 '220','USD'),
	( 'Rafaelo cake', 'Tropical vacation on your tongue', 'https://podravkaiovariations.azureedge.net/3b32de4e-631f-11eb-a1f4-0242ac120055/v/f2b1f6a6-64bc-11eb-b6c2-0242ac130010/1600x1200-f2b21938-64bc-11eb-9498-0242ac130010.webp',
	 '350','HRK');
 
INSERT INTO public.CakesIngredients(CakeId, IngredientId, Amount, Metric) VALUES 
	   ('1', '1', '3', 'whole'),
	   ('1','2', '13', 'tbsp'),
	   ('1', '3', '3', 'tbsp'),
	   ('1','4', '3', 'tbsp'),
	   ('1','5', '3', 'tbsp'),
	   ('1','6','3','tbsp'),
	   ('1','7','1','tbsp'),
	   ('1','8','300','g'),
	   ('1','9','500', 'mL'),
	   ('1', '10', '125', 'g'),
	   ('2','10','320', 'g'),
	   ('2','8','100', 'g'),
	   ('2','2','100','g'),
	   ('2','11','50','g'),
	   ('2','12','250','g'),
	   ('2','13','3','tbsp'),
	   ('2','14','150','g'),
	   ('3', '14','200','g'),
	   ('3','2','5','tbsp'),
	   ('3', '15','120','g'),
	   ('3','16','500','g'),
	   ('3','17', '1', 'whole'),
	   ('3', '1','2','whole'),
	   ('3','18','360','g'),
	   ('4','19','7','whole'),
	   ('4','2','250','g'),
	   ('4','20','50','g'),
	   ('4','7','50','g'),
	   ('4','6','70','g'),
	   ('4','21','150','g'),
	   ('4','22','7','whole'),
	   ('4','4','700','mL'),
	   ('4','15','100','g'),
	   ('4','14','200','g'),
	   ('4','23','1','tbsp');
	  
	  
